<?php

/*
    System to upload from bitbucket server
    Created and developed by radoslav55
    All rights reserved
*/

define('USERNAME','radoslav55');
define('PASSWORD','12_xrc$st');

//$data = json_decode('{"repository": {"website": "", "fork": false, "name": "Trackky", "scm": "git", "owner": "radoslav55", "absolute_url": "/radoslav55/trackky/", "slug": "trackky", "is_private": true}, "truncated": false, "commits": [{"node": "67bbc0b743d8", "files": [{"type": "added", "file": "public/save/action.php"}, {"type": "modified", "file": "public/save/index.php"}], "raw_author": "Radoslav Stefanov <radoslav.stefanov2000@gmail.com>", "utctimestamp": "2015-10-09 19:08:12+00:00", "author": "radoslav55", "timestamp": "2015-10-09 21:08:12", "raw_node": "67bbc0b743d8c30f6660fa48cebcfa0a1892ceed", "parents": ["6c356190d785"], "branch": "master", "message": "Test commit\n", "revision": null, "size": -1}], "canon_url": "https:/bitbucket.org", "user": "radoslav55"}');

$json = stripslashes($_POST['payload']);
$data = json_decode($json);
$urlAbsolute = $data->repository->absolute_url; // Absolute repository url adress 


foreach($data->commits as $commit){
	$node = $commit->node;
    $branch = $commit->branch;

	if($branch == '/radoslav55/trackky/'){
        foreach($commit->files as $file){
            if($file->type == 'modified' || $file->type == 'added'){
                $url = "https://api.bitbucket.org/1.0/repositories".$urlAbsolute."raw/".$node."/".$file->file;
                writeLog('Url -> ',$url);
                $script = download($url,$file->file);
                file_put_contents("../../" . $file->file, $script);
                writeLog('File modified -> ',$file->file);
            }
            else{
                unlink($file->file);
                writeLog('File deleted -> ',$file->file);
            }
        }
    }
    else{
        writeLog('Branch error -> ','Actual branch : ' . $branch);
    }

}

function download($url, $destination) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, USERNAME . ":" . PASSWORD);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);

    ob_start();
    curl_exec($ch);
    $result = ob_get_clean();
    return $result;
    
}

function writeLog($title,$text){
    $file = "../../log.txt";
    $txt = file_get_contents($file);
    ob_start();
    var_dump($text);
    $result = ob_get_clean();
    $txt .= date('Y-m-d H:i:s') . ' - ' . $title . $result . "\n";
    file_put_contents($file, $txt);
    fclose($myfile);

}

//Test haha