function Dom(element){

    var self = this;

    /*
     * Holds the new element
     * If new element is not defined, the system will search for a existing one
     */ 
    this.element = false;

    if(element)
        this.element = document.createElement(element);
    

    this.content = '';

    this.attr = function(key, value){

        self.element.setAttribute(key,value);
        return self;
    }

    this.append = function(html){

        self.content += html;
        self.element.innerHTML = self.content;
        return self;
    }

    this.html = function(){

        return this.element.outerHTML;
    }

    this.appendTo = function(element){
        $(element).append(self.html());
        return self;
    }

    /*
     * Functions for existing dom element
     */

    this.appendTRow = function(cols){
        
        for(var i in cols){
            self.append(new Dom('td').append(cols[i]).html());
        }

        return self;
    }
}