$(function(){

    $('#sendContactsEmail').click(function(e){
        e.preventDefault();
        $.ajax({
            'url' : '/sendmail',
            'method' : 'post',
            'dataType' : 'json',
            'async' : false,
            'headers' : { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
            'data' : {
                'email' : $('#contactsEmail').val(),
                'title' : $('#contactsTitle').val(),
                'message' : $('#contactsMessage').val()
            }
        }).done(function(data){

            if(data.status == 'success'){
                
                $('#sendContactsEmail').closest('form').find("input[type=text], textarea").val("");
                alert('Съобщението е изпратено успешно');
            }
            else{
                
            }
        }).fail(function(data){
            console.log(data);
            var fields = {'email' : '#contactsEmail','message' : '#contactsMessage','title' : '#contactsTitle' }
            var errors = data.responseJSON;
            for(var i in errors){
                for(var j in fields){
                    if(i == j){
                        $(fields[j]).css('border','1px solid red');
                        $(fields[j]).after('<span class="glyphicon glyphicon-remove form-control-feedback"></span><div class="text-danger" style="padding:5px">' + errors[i] + '</div>');
                    }
                }
            }
        })
    })

})