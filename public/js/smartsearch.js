function SmartSearch(){

	var self = this;

	this.searchFor;
	this.cache = {};

	this.objects = {
		'nodes' : {
			'url' : '/panel/nodes',
			'searchIn' : ['name']
		}
	}

	this.box;
	this.onTypeCallback = function(){};
	this.onSearchCallback = function(){};
	this.onCleanCallback = function(){};

	this.timeout;

	this.inside = function(searchFor){

		if(searchFor){
			self.searchFor = searchFor;
			return self;
		}
		return self.searchFor;
	}

	this.search = function(string, callback){

		/*if(!self.hasCache()){
			self.loadCache(callback);
		}*/
		self.getResults(string,callback);
	}
	/**
	 * handleField method
	 * Sets the default input that the user will search in
	 * All the smart events from the object will be called from this field
	 * 
	 * @param string dom - The element id or class
	 * @return SmartSearch
	 */
	this.handleField = function(dom){

		self.box = $(dom);
		self._beginListener();
	}

	this.onClean = function(callback){
		self.onCleanCallback = callback;
	}

	this.onSearch = function(callback){
		self.onSearchCallback = callback;
	}
	this.onType = function(callback){
		self.onTypeCallback = callback;
	}

	this.hasCache = function(){

		return !$.isEmptyObject(self.cache);
	}

	this.getResults = function(search,callback){


		$.ajax({
			'url' : self.objects[self.searchFor].url + '/find',
			'method' : 'get',
			'headers' : { 'X-CSRF-Token' : $('meta[name=csrf_token]').attr('content') },
    		'dataType' : 'json',
    		'data' : {
    			'string' : search
    		},
    		'async' : true
		})
		.success(function(output){

			if(output.status == 'success'){
				callback(output.data);
			}
			else{
				console.log(output);
				
			}
		})
	}

	this._beginListener = function(){

		self.box.keyup(function(e){

			if(self.timeout){
				clearTimeout(self.timeout);
			}

			self.timeout = setTimeout(function(){
				self.onSearchCallback(string);
			},500);

			var string = $(this).val();
			var stringLength = string.length;

			if(stringLength == 0){
				self.onCleanCallback();
			}
			else if(stringLength % 3 == 0 || e.keyCode == 13){
				self.onSearchCallback(string);
			}
			else{
				self.onTypeCallback(string);
			}

		})
	}
}