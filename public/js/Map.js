function Map(){

    var self = this;

    // dom element to spawn map here
    this.dom = document.getElementById('googleMap');

    // current user location TODO : get approximately location from php
    this.position = {
        'lat': 42.59217410588496,
        'lng': 25.544529855251312
    };

    // destination lat and log
    this.destination;

    // current map settings
    this.settings = {
        center: this.position,
        zoom: 7,
        disableDefaultUI: true,
        zoomControl:true,
        disableDoubleClickZoom: true,
        scrollwheel: true,
        'style' : [
            {
            featureType: "all",
            stylers: [
              { visibility: "off" }
            ]
            },
            {
            featureType: "road",
            stylers: [
              { visibility: "on" }
            ]
            }
        ]
    };

    // value showing if the user is selecting position or destination
    this.choosing = 'to';

    this.startPin;
    this.endPin;

    this.startPointIsLocation = false;
    this.endPointIsLocation = false;

    this.polylines = [];
    this.stops = [];

    this.absoluteTime = new Date().getTime();
    this.intervalTime = 0;

    this.textRoute = [];

    this.blockedControls = false;

    // The animation speed variable
    this.initialSpeed = animationSettingSpeed;
    this.speed = this.initialSpeed;

    // Indicates if the animation is paused or not
    this.animation = true;


    /*
     * run method
     * runs the map, the service for the directions 
     * executes the locate and sets the events
     */
    this.run = function(){

        this.map =  new google.maps.Map(this.dom,this.settings);
        this.service = new google.maps.DirectionsService();
        self.infoWindow = new google.maps.InfoWindow();
        self.infoWindow.setContent('<div id="infoWIndowContent">' + $('.info-window-scheme')[0].outerHTML +  '</div>');
        
        self.startPin = new google.maps.Marker({
            map: self.map,
            draggable : true,
            label: 'Н',
        });

        self.endPin = new google.maps.Marker({
            map: self.map,
            draggable : true,
            color : '#00FF00',
            label: 'К'
        });

        if(debug){
            this.locate();
            this.setEvents();
        }

        //self.infoWindow.open(self.map);
        //this.drawAll();
    }

    /*
     * changeChoosing method
     * used to change the value of the marker when the user clicks the map
     * if the value is from, the user's click event will set the from position
     * if the value is to, the user's click event will set the destination
     */
    this.changeChoosing = function(choosing){

        if(choosing == 'from'){

            self.choosing = 'from';

            $('.toSelect').removeClass('active');
            $('.fromSelect').addClass('active');

        }
        else if(choosing == 'to'){

            self.choosing = 'to';

            $('.fromSelect').removeClass('active');
            $('.toSelect').addClass('active');
        }
    }

    /*
     * locate method
     * used to determinate the current user position
     * if the user doesn't have the required hardware, it will show a message
     */
    this.locate = function(){

        self.position = {
            lat: 42.59217410588496,
            lng: 25.544529855251312
        };

        //self.startPin.setPosition(self.position);
        //self.map.setCenter(self.position);
        
        // Try HTML5 geolocation.
        if (navigator.geolocation.getCurrentPosition) {
        //if(false){
            navigator.geolocation.getCurrentPosition(function(position) {

                self._closestRoad(function(coordinates){

                    self.setStartByLocation(coordinates);

                },{
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                });

                self.map.setZoom(16);

                
            }, function() {
                System.alert('err_gps_off');
            });
        } else {
            System.alert('err_gps_off');
        }

        
    }

    /*
     * setEvents method
     * it initializes the user's events ON THE MAP
     */
    this.setEvents = function(){

        google.maps.event.addListener(this.map,'click',function(e){
            
            if(!self.blockedControls){
                self._closestRoad(function(coordinates){

                    if(self.choosing == 'to'){

                        self.setEnd(coordinates);

                    }
                    else if(self.choosing == 'from'){

                        self.setStart(coordinates);
                    }

                },{
                    'lat' : e.latLng.lat(),
                    'lng' : e.latLng.lng()
                });
            }

        },false);

        self.startPin.addListener('dragend',function(e) {
                
            if(!self.blockedControls){
                self._closestRoad(function(coordinates){

                    self.setStart(coordinates);

                },{
                    'lat' : e.latLng.lat(),
                    'lng' : e.latLng.lng()
                });
            }

        });

        self.endPin.addListener('dragend',function(e) {
            
            if(!self.blockedControls){
                self._closestRoad(function(coordinates){
                    
                    self.setEnd(coordinates);
                },{
                    'lat' : e.latLng.lat(),
                    'lng' : e.latLng.lng()
                });
            }

        });

        $('#calculate').click(function(){
            self.calculateRoute();
        })
    }

    this.calculateRoute = function(){
        if(
            self.destination &&
            self.position.lat.toString().length &&
            self.position.lng.toString().length && 
            self.destination.lat.toString().length &&
            self.destination.lng.toString().length
        ){
            System.alert('loading');
            System.ajax('/routes/calculate',{
                from : this.position,
                to : this.destination,
                //date : $('#leavingTime').val() + ' ' + $('#leavingDate').val()
            },function(data){
                System.remove();

                //self.absoluteTime = data.initl * 1000;
                if(data.status == 'success'){
                    self.map.setZoom(17);
                    $('.animation-control-buttons').slideDown('slow');
                    $('.block-controls').show();
                    self.blockedControls = true;
                    self.animation = true;
                    $('.start-animation').attr('class','stop-animation');

                    map.startPin.setDraggable(false);
                    map.endPin.setDraggable(false);

                    self.infoWindow.open(self.map);

                    self.clearPolylines();
                    self.cleanNodes();

                    self.pathData = data;

                    self.pathInfo = data.data;

                    self.currentNode = 0;
                    self.drawPathWithAnimation(data);

                }
                else{
                    alert('Нещо се обърка');
                }
            });
        }
        else{
            
        }
    }

    this.terminate = function(){
        $('.general-statistic-fullbox').fadeIn('medium');
        $('#status-walking-time').html(System.timeToText(self.pathData.walkingTime));
        $('#status-traveling-time').html(System.timeToText(self.pathData.travelingTime));
        $('#status-waiting-time').html(System.timeToText(self.pathData.waitingTime));
        $('#status-all-time').html(System.timeToText(self.pathData.totalTime));

        $('.animation-control-buttons-right').show();

        $('#statistics-marchrute').html('');

        for(var i in self.textRoute){
            $('#statistics-marchrute').append('<div class="row">' + self.textRoute[i] + '</div>');
        }
        resize();

    }

    this.setInfoBoxIcon = function(status){
        $('.action-image').css({
            'background-image' : 'url(' + statsIcons[status] + ')'
        });
    }

    this.setInfoBoxText = function(data){

        if(data['route']){
            $('.action-title').text(data.text + ' №' + data.route);
        }
        else{
            $('.action-title').text(data.text);
        }
    }

    this.writeTime = function(){
        
        var date = new Date(self.absoluteTime);

        var hours = date.getHours();
        var minutes = "0" + date.getMinutes();
        var seconds = "0" + date.getSeconds();

        //console.log(hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2));

        $('.total-time').text(hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2));
    }

    this.waitInStop = function(time, data, callback){

        var initialTime = self.absoluteTime;

        var currentTime = 0;
        self.setInfoBoxIcon('waiting');
        self.setInfoBoxText({'text' : 'Чакане'});

        if(time != 0)
            self.textRoute.push(System.timeToText(time) + ' чакане, после');

        var step = time / 200 * self.speed / self.initialSpeed;

        function animate(){
            if(self.animation){
                if(currentTime < time){

                    currentTime += step;
                    self.absoluteTime += step * 1000;
                    if(self.absoluteTime > (initialTime + time * 1000)){

                        self.absoluteTime = initialTime + time * 1000;
                    }
                    self.writeTime();
                    requestAnimationFrame(animate);
                }
                else{
                    self.absoluteTime = initialTime + time * 1000;
                    self.setInfoBoxIcon(data['iconType']);
                    self.setInfoBoxText(data['json']);
                    callback();
                }
            }
            else{
                requestAnimationFrame(animate);
            }
        }
        animate();
    }

    this.addNode = function(latLng,name){

        var nodeIndex = self.stops.length;

        var newNode = new MarkerWithLabel({
            position: new google.maps.LatLng(latLng),
            draggable: false,
            map: self.map,
            labelContent: name,
            labelAnchor: new google.maps.Point(22, 0),
            labelClass: "labels", // the CSS class for the label
            labelStyle: {opacity: 1},
        });

        self.textRoute.push($(name).html());

        self.stops[nodeIndex] = newNode;

        google.maps.event.addListener(self.stops[nodeIndex], 'click', function(){
            //alert(self.stops[nodeIndex].get('labelContent'));
        });
    }

    this.cleanNodes = function(){
        for(var i in self.stops){
            self.stops[i].setMap(null);
        }
    }

    this.drawPathWithAnimation = function(data){

        var latLng;

        self.writeTime();

        self.setInfoBoxIcon(self.pathInfo[self.currentNode].type);

        if(data.path[parseInt(self.currentNode) + 1].type != 'to'){

            if(data.path[self.currentNode].type == 'from'){

                latLng = self.position;
                self.textRoute.push('Начало');
            }
            else{

                latLng = {
                    'lat' : parseFloat(data.path[self.currentNode].lat),
                    'lng' : parseFloat(data.path[self.currentNode].lng)
                };

                self.addNode(latLng,data.path[self.currentNode].name);

            }

            var nextLatLng = {
                'lat' : parseFloat(data.path[parseInt(self.currentNode) + 1].lat),
                'lng' : parseFloat(data.path[parseInt(self.currentNode) + 1].lng)
            };
            
            /*var centerCircle = new google.maps.Circle({
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 1,
                map: self.map,
                center: latLng,
                radius: 5
            });
            */

            var totalSeconds = self.pathInfo[self.currentNode].timestamp;


            self.intervalTime = self.pathInfo[self.currentNode].timestamp;

            if(self.pathInfo[self.currentNode].type == 'walk'){

                self.textRoute.push(System.timeToText(self.intervalTime) + ' пеша до');


                self.setInfoBoxText({'text' : 'Ходене'});

                self.drawTravelModePath(latLng,nextLatLng,[false],function(){
                    self.currentNode++;
        
                    self.drawPathWithAnimation(data);
                });
            }
            else{



                var dataJson = {
                    'text' : vehicles[self.pathInfo[self.currentNode].type],
                    'route' :  self.pathInfo[self.currentNode].routeNumber,
                }

                self.setInfoBoxText(dataJson);


                self.waitInStop(self.pathInfo[self.currentNode].waitingTime,{
                    'iconType' : self.pathInfo[self.currentNode].type,
                    'json' : dataJson
                },function(){
                    self.drawLine(latLng,nextLatLng,'#000000',self.pathInfo[self.currentNode].travelingTime,function(){
                        self.currentNode++;
            
                        self.drawPathWithAnimation(data);
                    });
                });
                //console.log(dataJson);
                self.textRoute.push(System.timeToText(self.intervalTime) + ' с ' + dataJson.text + ' №' + dataJson.route + ' до');

                
            }

            /*self.drawLine(latLng,nextLatLng,self.pathInfo[self.currentNode].type == 'walk' ? '#FF0000' : '#000000',

                self.pathInfo[self.currentNode].type == 'walk' ? 'route' : false,function(){
                    self.currentNode++;
        
                    self.drawPathWithAnimation(data);
                });
            */

        }
        else{

            self.setInfoBoxText({'text' : 'Ходене'});

            if(data.path[self.currentNode].type == 'from'){
                latLng = self.position;
                self.textRoute.push('Начало');
            }
            else{
                latLng = {
                    'lat' : parseFloat(data.path[self.currentNode].lat),
                    'lng' : parseFloat(data.path[self.currentNode].lng)
                };
                self.addNode(latLng,data.path[self.currentNode].name);
            }

            self.intervalTime = self.pathInfo[self.currentNode].timestamp;

            var nextLatLng = self.destination;

            self.textRoute.push(System.timeToText(self.intervalTime) + ' пеша до');

            self.textRoute.push('Край');

            self.drawTravelModePath(latLng,nextLatLng,[false],function(){
                self.terminate();
            });

        }

    }

    /*
     * convertDirectionsResult method
     * used to convert the google output to json with lat1,lng1,lat2,lng2 and the percentage of each part of the path
     * it also returns the path distance
     */
    this.convertDirectionsResult = function(result, callback){

        var path = [];
        var distance = 0;

        for(var i in result.routes[0].overview_path){

            if(result.routes[0].overview_path[parseInt(i) + 1]){

                var lat1 = result.routes[0].overview_path[i].lat();
                var lng1 = result.routes[0].overview_path[i].lng();

                var lat2 = result.routes[0].overview_path[parseInt(i) + 1].lat();
                var lng2 = result.routes[0].overview_path[parseInt(i) + 1].lng();

                var a = lat1 - lat2;
                var b = lng1 - lng2;

                var c = Math.sqrt( a*a + b*b );

                path.push([
                    [lat1,lng1],
                    [lat2,lng2],
                    c
                ]);

                distance += c;

            }

        }

        for(var i in path){
            path[i][2] = path[i][2] * 100 / distance;
        }
        if(JSON.stringify(path) == '[]'){
            callback(false);
        }
        else{
            callback(path);
        }
        

        
        

    }

    this.drawTravelModePath = function(from,to,status,callback){

        if(status[0]){

            if(status[2]){
                self.drawLine({
                    'lat' : status[2][status[1]][0][0],
                    'lng' : status[2][status[1]][0][1]
                },
                {
                    'lat' : status[2][status[1]][1][0],
                    'lng' : status[2][status[1]][1][1]
                },'#FF0000',status[2][status[1]][2] * self.intervalTime * 0.01,function(){
                    if(status[2][parseInt(status[1] + 1)]){
                        status[1]++;
                        self.drawTravelModePath(from,to,status,callback);
                    }
                    else{
                        callback();
                    }
                });
            }
            else{
                callback();
            }
            
                
            
            
        }
        else{

            self.service.route({
                origin: from,
                destination: to,
                travelMode: google.maps.DirectionsTravelMode.WALKING
            }, function(result, stat){

                if (stat == google.maps.DirectionsStatus.OK) {

                    self.pathArray = [];
                    self.totalDistance = 0;

                    self.convertDirectionsResult(result,function(path){
                        self.drawTravelModePath(from,to,[
                            true,
                            0,
                            path,
                        ],callback);
                    })

                }
            });
        }
        
    }

    this.drawLine = function(from, to, color, time, callback){

        var initialTime = self.absoluteTime;

        var x = parseFloat(from['lat']);
        var y = parseFloat(from['lng']);

        var destinationX = parseFloat(to['lat']);
        var destinationY = parseFloat(to['lng']);

        var radians = Math.atan2(destinationY - y, destinationX - x);

        var angleDeg = radians * 180 / Math.PI;

        var speed = self.speed;

        var step = 0;

        var stepXbase = Math.cos(radians);
        var stepYbase = Math.sin(radians);

        var stepX = stepXbase * speed;
        var stepY = stepYbase * speed;

        if(Math.abs(destinationX - x) > Math.abs(destinationY - y)){
            step = stepX / ((destinationX - x) / 100);
        }
        else{
            step = stepY / ((destinationY - y) / 100);
        }

        var percentage = 0;

        function animation(){

            stepX = stepXbase * self.speed;
            stepY = stepYbase * self.speed;

            if(self.animation){
                 if(x != from['lat']){
                    self.polylines.slice(-1)[0].setMap(null);
                    self.polylines.slice(-1)[0] = {};
                }
                

                self.polylines.push(new google.maps.Polyline({
                    path: [{
                        'lat' : parseFloat(from['lat']),
                        'lng' : parseFloat(from['lng'])
                    },{
                        'lat' : parseFloat(x),
                        'lng' : parseFloat(y)
                    }],
                    geodesic: true,
                    strokeColor: color,
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                }));

                self.polylines.slice(-1)[0].setMap(self.map);

                self.map.setCenter({
                    'lat' : parseFloat(x),
                    'lng' : parseFloat(y)
                });
                self.infoWindow.setPosition({
                    'lat' : parseFloat(x),
                    'lng' : parseFloat(y)
                });

                var willContiunue = true;

                if(stepX < 0){
                    if(destinationX < parseFloat(x)){

                        x += stepX;

                        if(x < destinationX){
                            x = destinationX;
                        }

                        willContiunue = true;
                    }
                    else{
                        willContiunue = false;
                    }
                }
                else{
                    if(destinationX > parseFloat(x)){

                        x += stepX;

                        if(x > destinationX){
                            x = destinationX;
                        }

                        willContiunue = true;
                    }
                    else{
                        willContiunue = false;
                    }
                }

                if(stepY < 0){
                    if(destinationY < parseFloat(y)){

                        y += stepY;

                        if(y < destinationY){
                            y = destinationY;
                        }

                        willContiunue = true;
                    }
                    else{
                        //willContiunue = false;
                    }
                }
                else{
                    if(destinationY > parseFloat(y)){

                        y += stepY;

                        if(y > destinationY){
                            y = destinationY;
                        }


                        willContiunue = true;
                    }
                    else{
                        //willContiunue = false;
                    }
                }

                if(willContiunue){

                    requestAnimationFrame(animation);
                    self.absoluteTime += time * step * 10;
                    if(self.absoluteTime < time){
                        self.absoluteTime = initialTime + time * 1000;
                    }
                    self.writeTime();
                }
                else{

                    self.absoluteTime = initialTime + time * 1000;
                    //self.absoluteTime = new Date.getTime() + time * 1000;
                    self.writeTime();
                    callback();
                }
                
                //if(destinationY > parseInt(y)){
                    
                //}
            }
            else{
                requestAnimationFrame(animation);
            }
        }

        animation();

        
        /*self.polylines.push(new google.maps.Polyline({
            path: [{
                'lat' : parseFloat(from['lat']),
                'lng' : parseFloat(from['lng'])
            },{
                'lat' : parseFloat(to['lat']),
                'lng' : parseFloat(to['lng'])
            }],
            geodesic: true,
            strokeColor: color,
            strokeOpacity: 1.0,
            strokeWeight: 2
        }));
        self.polylines.slice(-1)[0].setMap(self.map);
        */

        
    }

    this.clearPolylines = function(){
        for(var i in self.polylines){
            self.polylines[i].setMap(null);
        }
    }

    this.setStart = function(latLng){

        self._setStart(latLng);

        $('#from').removeClass('special');

        System.getAddress(self.position,function(address){
            
            $('#from').val(address);
            self.startPointIsLocation = false;
        });

    }

    this.setStartByLocation = function(latLng){

        self._setStart(latLng);
        self.map.setCenter(self.position);

        $('#from').addClass('special');

        $('#from').val('Текущо местоположение');
        self.changeChoosing('to');
        self.startPointIsLocation = true;

    }

    this._setStart = function(latLng){

        self.position = latLng;
        self.map.setZoom(16);
        self.startPin.setPosition(self.position);

    }

    this.setEnd = function(latLng){

        self.destination = latLng;
        self.endPin.setPosition(self.destination);
        self.map.setZoom(16);

        System.getAddress(self.destination,function(address){
            
            $('#to').val(address);
            self.endPointIsLocation = false;
        });
        
    }

    this.synchTextField = function(field){

        var latLng;
        var isLocation = false;

        if(field == 'from'){

            latLng = self.position;
            isLocation = self.startPointIsLocation;
        }
        else{

            latLng = self.destination;
            isLocation = self.endPointIsLocation;
        }

        if(isLocation){

            $('#' + field).attr('class','special');
            $('#' + field).val('Текущо местоположение');
        }
        else{

            System.getAddress(latLng,function(address){
                
                $('#' + field).removeClass('special');
                $('#' + field).val(address);
            });
        }

    }

    this.reverseFields = function(){

        if(self.position && self.destination){
            self.position = [self.destination, self.destination = self.position][0];
            //self.setStart([self.destination, self.setEnd(self.position)][0]);

            self.startPin.setPosition(self.position);

            self.endPin.setPosition(self.destination);

            self.startPointIsLocation = [self.endPointIsLocation, self.endPointIsLocation = self.startPointIsLocation][0];

            self.synchTextField('from');
            self.synchTextField('to');
        }
    }

    this.autocomplete = function(element){
        var autocomplete = new google.maps.places.Autocomplete(document.getElementById(element));
        autocomplete.bindTo('bounds', map.map);

        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();

            var latLng = {
                'lat' : place.geometry.location.lat(),
                'lng' : place.geometry.location.lng()
            };
            
            if(element == 'from'){
                self.setStart(latLng);
                self.map.setCenter(latLng);
            }
            else{
                self.setEnd(latLng);
                self.map.setCenter(latLng);
            }
        });
    }

    this._closestRoad = function(callback,latLng){
        self.service.route({
            origin: latLng,
            destination: latLng,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        }, function(result, status){
            if (status == google.maps.DirectionsStatus.OK) {
                for (var i = 0, len = result.routes[0].overview_path.length;
                    i < len; i++) {
                    callback({
                        'lat' : result.routes[0].overview_path[i].lat(),
                        'lng' : result.routes[0].overview_path[i].lng(),
                    });
                }
            }
        });
    }
  
}

var System = {
    ajax : function(r_url,r_parameters,r_function){
        $.ajax({
            url: r_url,
            type: 'get',
            data: r_parameters,
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
            success: r_function
        });
    },
    remove : function(callback){
        $('.alert-blackbox').fadeOut('slow',function(){
            $('.alert-blackbox').remove();
            if(callback)
                callback();
        });
    },
    alert : function(code){
        var values = {
            'err_gps_off' : {
                'title' : '<h2>Моля, включете си GPS-a</h2>',
                'buttons' : '<input type="button" class="message-box-button message-box-button-openedGps" value="Вече си го включих"><br>' + 
                        '<input type="button" class="message-box-button message-box-button-noGps" value="Не ми трябва GPS">'
            },
            'loading' : {
                'title' : '<h2>Изчисляване на маршрута</h2><h4>Процесът може да отнеме повече време</h4>',
                'buttons' : '<div class="loading-icon"></div>'
            }
        }
        $('body').append('<div class="black-background alert-blackbox">' + 
            '<div class="message-box">' + 
                '<div class="message-box-centerer">' +
                    '<div class="message-text">' + 
                        values[code].title +
                    '</div>' + 
                    '<div class="message-buttons">' + 
                        values[code].buttons +
                    '</div>' + 
                '</div>' + 
            '</div></div>');

        $('body').on('click','.message-box-button-noGps',function(){
            System.remove();

        })
        $('body').on('click','.message-box-button-openedGps',function(){
            System.remove(function(){
                map.locate();
            })

        })
    },
    timeToText : function(timestamp){

        var minutes = timestamp / 60;
        var hours = 0;
        var string = "";

        if(minutes < 1){
            timestamp = timestamp | 0;
            string = timestamp + ' секунди';
        }

        else{
            while(minutes > 59){
                hours += 1;
                minutes -= 59;
            }

            

            if(hours){
                string += hours + ' часа и ';
            }

            string += parseInt(minutes) + ' минути';
        }

        return string;
    },
    getAddress : function(latlng,callback){
        var geocoder = new google.maps.Geocoder;
        geocoder.geocode({'location': latlng}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    callback(results[0]['formatted_address']);
                    
                } else {
                    window.alert('No results found');
                }
            } else {
                if(status == 'OVER_QUERY_LIMIT'){
                    alert('Не може да изпълните толкова заявки за това време. Моля, изчакайте няколко секунди');
                }
            }
        });
    }
}

function drawCircle(map, color, json){
    new google.maps.Circle({
        strokeColor: color,
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: color,
        fillOpacity: 0.35,
        map: map,
        center: {
            'lat' : parseFloat(json['lat']),
            'lng' : parseFloat(json['lng'])
        },
        radius: 10
    });
}


function echo(text,from,to){
    new MapLabel({
        text: text,
        position: new google.maps.LatLng(
            parseFloat(parseFloat((parseFloat(from['lat']) + parseFloat(to['lat'])) / 2)),
            parseFloat(parseFloat((parseFloat(from['lng']) + parseFloat(to['lng'])) / 2))
        ),
        map: self.map,
        fontSize: 20,
        align: 'right'
    });
}