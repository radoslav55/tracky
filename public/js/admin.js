$(document).ready(function(){
                
    $('.main').height(window.innerHeight - $('nav').height());

    var initialLeft = 0;

    $('input[type=file]').change(function() { 

        var element = $(this);


        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                //element.closest('div').find('.image_preview_div').html('<img class="image-preview">');
                //element.closest('div').find('.image-preview').css('padding-top','10px');
                //$('.image-preview').attr('height',100);
                element.closest('.image-upload').find('.image-preview').attr('src', e.target.result);
            };

            reader.readAsDataURL(this.files[0]);
        }
    });
    
    if($(window).width() < 768){
        $('.go-back').html('<span class="glyphicon glyphicon-chevron-left"></span>');
        $('.add-new').html('<span class="glyphicon glyphicon-plus"></span>');
        $('.previous-node').html('<span class="glyphicon glyphicon-chevron-left"></span>');
        $('.next-node').html('<span class="glyphicon glyphicon-chevron-right"></span>');
        $('.glyphicon-label').html(null);

        $('.btn-success').html('<span class="glyphicon glyphicon-pencil"></span>');
        $('.btn-danger').html('<span class="glyphicon glyphicon-remove"></span>')
    }

    $('#menu-collapse').click(function(){
        $('.admin').toggle();
    });

    $('.are-you-sure').on('click',function(e){
        var prompt = confirm($(this).attr('question'));
        if(prompt){
            return true;
        }
        else{
            e.preventDefault();
            e.stopImmediatePropagation();
        }
    })


    $('.incognitoSelect .clickableArea').click(function(){

        var element = $(this).parent('.incognitoSelect')
        .find('.options')
        .slideToggle('fast');

    })
    
});