$(function(){

    var search = new SmartSearch();
    search.handleField('#search-field');

    search.onType(function(val){

    	$('table').waiting();
    	

    });
    search.onSearch(function(val){
    	search.inside('nodes').search(val,function(nodes){

    		$('table').waiting();
            $('.main-list').css('display','none');
            $('table').stopWaiting();
            $('.searchTBody').remove();

            var table = new Dom('tbody').attr('class','searchTBody');
            console.log(nodes);
            for(var i in nodes){
                console.log(nodes[i].town);
                table.append(new Dom('tr').appendTRow([
                    nodes[i].id,
                    nodes[i].name,
                    nodes[i].town.name,
                    new Dom('a')
                    .attr('href','/panel/nodes/manage?nodeId=' + nodes[i].id)
                    .attr('class','btn btn-success btn-xs')
                    .append('<span class="glyphicon glyphicon-pencil"></span> Редактирай')
                    .html(),
                    new Dom('a')
                    .attr('href','/panel/nodes/delete/' + nodes[i].id)
                    .attr('class','btn btn-danger btn-xs')
                    .append('<span class="glyphicon glyphicon-remove"></span> Изтрий')
                    .html()

                ]).html());
            }
            
            table.appendTo('table');
            
        });
    })	
    search.onClean(function(){
    	$('.searchTBody').remove();
    	$('.main-list').css('display','table-row-group');
    	$('table').stopWaiting();
    })
    /*search.smartEvent('#search-field',function(val){
        
        if(val){
            search.inside('nodes').search(val,function(nodes){
                $('.main-list').css('display','none');
                
                var table = new Dom('tbody').attr('class','searchTBody');
                console.log(nodes);
                for(var i in nodes){
                    console.log(nodes[i].town);
                    table.append(new Dom('tr').appendTRow([
                        nodes[i].id,
                        nodes[i].name,
                        nodes[i].town.name,
                        new Dom('a')
                        .attr('href','/panel/nodes/manage?nodeId=' + nodes[i].id)
                        .attr('class','btn btn-success btn-xs')
                        .append('<span class="glyphicon glyphicon-pencil"></span> Редактирай')
                        .html(),
                        new Dom('a')
                        .attr('href','/panel/nodes/delete/' + nodes[i].id)
                        .attr('class','btn btn-danger btn-xs')
                        .append('<span class="glyphicon glyphicon-remove"></span> Изтрий')
                        .html()

                    ]).html());
                }
                
                table.appendTo('table');
                
            });
        }
        else{
            $('.main-list').css('display','table-row-group');
        }
    });
	*/
    /*$('#search-field').keyup(function(e){

        $('.searchTBody').remove();

        else{
            $('.main-list').css('display','table-row-group');
        }
    })*/
    
})