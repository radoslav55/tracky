function System(){



	this.message = function(html){
		$('body').append('<div style="position:fixed;top:0;left:0;background-color:rgba(0,0,0,0.8); z-index:999;width:100%;height:100%;" class="dialog">' +
	        '<div style="position:absolute;top:5%;right:7%;color:red;z-index:999999;padding:10px;"><span class="glyphicon glyphicon-remove" id="closeMap" style="cursor:pointer;"></span></div>' + 
	        '<div style="position:relative;width:86%;height:86%;margin:0px auto;top:5%;background-color:white;padding:2%;">' + 
	        html +
	    '</div></div>');

	    $('#closeMap').on('click',function(){
		    $('.dialog').remove();
		})

	}

}

(function( $ ){
	$.fn.waiting = function(html) {
	
		if(!this.find('.gray-background').length){
			var dom = new Dom('div')
			.attr('class','gray-background');

			if(html){
				dom.append(
					new Dom('div').
					attr('class','absolute-centerer')
					.append(
						new Dom('div')
						.attr('class','white-tooltip')
						.append(html)
						.html()
					)
					.html()
				)
			}

			this.css('position','relative');
			dom.appendTo(this);

			this.find('.gray-background').css('height',this.height());
		}
	}; 

	$.fn.stopWaiting = function(){
		//this.find('.gray-background').fadeOut('slow');
		this.find('.gray-background').remove();
	}
})( jQuery );


var system = new System();

