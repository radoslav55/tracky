function Panel(){

    var self = this;

    this.canBeHidden = false;
    this.intialLeft;
    this.mousedownAction = false;


    this.append = function(element){

        var html = new Dom('div')
        .attr('class','map-right-panel')
        .append(
            new Dom('div')
            .attr('class','panel-arrow')
            .append(
                new Dom('span')
                .attr('class','glyphicon glyphicon-chevron-left')
                .html()
            )
            .html()
        )
        .append(
            new Dom('div')
            .attr('style','text-align:center')
            .append(
                new Dom('button')
                .attr('type','button')
                .attr('class','btn btn-default control-fullscreen')
                .append('На цял екран [F]')
                .html()
            )
            .append(new Dom('br').html())
            .append(
                new Dom('button')
                .attr('type','button')
                .attr('class','btn btn-default control-removeNode')
                .append('Изтрий връзка [ - ]')
                .html()
            )
            .append(new Dom('br').html())
            .append(
                new Dom('button')
                .attr('type','button')
                .attr('class','btn btn-default control-removeLastEdge')
                .append('Изтрий последната връзка [Ctrl + z]')
                .html()
            )
            .append(new Dom('br').html())
            .append(
                new Dom('button')
                .attr('type','button')
                .attr('class','btn btn-default control-removeAll')
                .append('Изтрий всичко [Ctrl + x]')
                .html()
            )
            .html()
        )
        .html();

        $(element).append(html);
        self.fit();
        self.setEvents();
    }

    this.fit = function(){


        $('.map-right-panel').each(function(){
            self.intialLeft = parseInt($(this).width());

            $(this).css('left',$('#mapContainer').width());
            $(this).children('.panel-arrow').css('left', - 30);
        })

        $('.map-right-panel .panel-arrow').mouseover(function(){

            self.pullLeft();

        });

        $('.map-right-panel').mouseleave(function(){
            
            if(self.canBeHidden){

               self.pullRight(500);
            }
            
        });
    }

    this.setEvents = function(){

        $('body').on('click','.control-removeLastEdge',function(){
            edges.path.pop();
            edges.points.pop();
            nodes.updateBeginAndEnd(edges.points);
        })

        $('body').on('click','.control-removeAll',function(){
            edges.path.clear();
            edges.points = [];
            nodes.updateBeginAndEnd(edges.points);
        })

        $('body').on('click','.control-fullscreen',function(){
            self.toggleFullscreen();
        })

        $('body').on('click','.control-removeNode',function(){
            edges.toggleRemoving();
        });

        $(document).on('keydown', function ( e ) {

            if ( $('input:focus').length == 0 ) {

                if(e.ctrlKey && (String.fromCharCode(e.which) == 'z' || String.fromCharCode(e.which) == 'Z')){
                    edges.path.pop();
                    edges.points.pop();
                    nodes.updateBeginAndEnd(edges.points);
                }
                if(e.ctrlKey && (String.fromCharCode(e.which) == 'x' || String.fromCharCode(e.which) == 'X')){
                    edges.path.clear();
                    edges.points = [];
                    nodes.updateBeginAndEnd(edges.points);
                }
                if((String.fromCharCode(e.which) == 'f' || String.fromCharCode(e.which) == 'F')){
                    self.toggleFullscreen();
                }
                if((String.fromCharCode(e.which) == 'm')){
                    edges.toggleRemoving();
                }
            }
        });


    }

    this.pullRight = function(speed){
        $('.map-right-panel').animate({
            'left' : $('#mapContainer').width()
        },speed);
        self.canBeHidden = false;
    }

    this.pullLeft = function(){

        $('.map-right-panel').animate({
            'left' : $('#mapContainer').width() - self.intialLeft - (parseInt($('.map-right-panel').css('padding-left'))) * 2
        },200);

        setTimeout(function(){
            self.canBeHidden = true;
        },500);
    }

    this.toggleFullscreen = function(){
        if($('.control-fullscreen').hasClass('opened')){

            $('.control-fullscreen').removeClass('opened');
            $('.control-fullscreen').html('На цял екран [F]');

            $('#map')
            .prependTo('.mapDirectory')
            .css({
                'position' : 'relative',
                'height' : '300px',
                'width' : '100%',
                'left' : 0,
                'top' : 0
            });

        }
        else{


            $('.control-fullscreen').html('Изход от цял екран [F]');
            $('.control-fullscreen').addClass('opened');

            $('#map')
            .appendTo('body')
            .attr('class','fullscreenMap')
            .css({
                'position' : 'absolute',
                'top' : '5%',
                'left' : '5%',
                'width' : '90%',
                'height' : '90%',
                'z-index' : '99999999999'
            });

        }
        self.pullRight(1);
        google.maps.event.trigger(map.map, "resize");
    }
}

/*


<div class="map-right-panel">
    <div class="panel-arrow">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </div>
    <div style="text-align:center">
        <button type="button" class="btn btn-default control-fullscreen">
            На цял екран
        </button><br>
        <button type="button" class="btn btn-default control-removeLastEdge">
            Изтрий последната връзка [Ctrl + z]
        </button><br>
        <button type="button" class="btn btn-default control-removeAll">
            Изтрий всичко [Ctrl + x]
        </button>
    </div>
</div>
 */