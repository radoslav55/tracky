function Edges(index){

	var self = this;

    this.lastClickLength = 0;
    this.cache = [];

    this.path;
    this.poly;

    this.points = [];

    this.newNodeMoving = false;

    this.removeOnClick = false;
    
    this.run = function(){
    	this.init();
    }

    this.init = function(){
	    this.poly = new google.maps.Polyline({ map: map.map });
	    this.path = new google.maps.MVCArray();
	    this.poly.setPath(self.path);

        this.setEvents();
	};

    this.draw = function(data){

        self.cache.unshift(self.path);

        self.points.push(parseInt(data));
		self.path.push(nodes.markers[data].getPosition());

		lastClickLength = self.path.getLength();

        nodes.updateBeginAndEnd(self.points);

    };

    this.remove = function(id){

        for(var i in self.points){
            nodes.markers[self.points[i]].setLabel('');
        }

        self.path.removeAt(self.getNodePosition(id));
        self.points.splice(self.getNodePosition(id),1);


        lastClickLength = self.path.getLength();
        self.removeOnClick = false;

        nodes.updateBeginAndEnd(self.points);

    }

    this.submit = function(){   
	    
	    $('#mapRoutes').val(JSON.stringify(self.points));
	    $('form').submit();
		
    };

    this.startNodeDragging = function(id){

        self.moving = id;
    }

    this.stopNodeDragging = function(){
        
        if(self.moving){

            var x1 = self.path.getArray()[self.getNodePosition(self.moving)].lat();
            var y1 = self.path.getArray()[self.getNodePosition(self.moving)].lng();

            var closest = nodes.getClosest(x1,y1);
            var minValue = closest.distance;
            var minId = closest.id;

            if(minValue != 0 || minId == self.moving){

                self.path.setAt(self.getNodePosition(self.moving),nodes.markers[self.moving].getPosition());
            }
            else{
                if(!self.getNodePosition(minId)){
                    self.points[self.getNodePosition(self.moving)] = parseInt(minId);
                    nodes.updateBeginAndEnd(self.points);
                }
                else{
                    self.path.setAt(self.getNodePosition(self.moving),nodes.markers[self.moving].getPosition());
                    map.alert('Тази спирка вече се използва в маршрута', 'danger');
                }
            }
            
            self.moving = false;
        }

        else if(self.newNodeMoving){

            var x1 = self.path.getArray()[self.newNodeMoving].lat();
            var y1 = self.path.getArray()[self.newNodeMoving].lng();

            var closest = nodes.getClosest(x1,y1);
            var minValue = closest.distance;
            var minId = closest.id;


            if(minValue != 0 || minId == self.moving){

                self.path.removeAt(self.newNodeMoving);
            }
            else{
                if(!self.getNodePosition(minId)){
                    self.points.splice(self.newNodeMoving, 0, parseInt(minId));
                    nodes.updateBeginAndEnd(self.points);
                }
                else{
                    self.path.removeAt(self.newNodeMoving);
                    map.alert('Тази спирка вече се използва в маршрута','danger');
                }
            }

            self.newNodeMoving = false;
            map.map.setOptions({draggable: true});
            
        }
    }

    this.getNodePosition = function(id){
        for(var i in self.points){
            if(self.points[i] == id){
                return i;
            }
        }

        return false;
    }

    this.drawRoute = function(array){

    	for(var i in array){
			//var latlng = new google.maps.LatLng(stops[array[i]]['lat'],stops[array[i]]['lng']);
            self.draw(array[i]);
			nodes.selectedNodes[array[i]] = true;
		}
        map.map.setCenter(nodes.markers[array[0]].getPosition());
        map.map.setZoom(17);
        //map.map.setCenter(new google.maps.LatLng(self.getNodePosition(array[0])));
    }

    this.drawRouteByPoints = function(array){
    	for(var i in array){
			//var latlng = new google.maps.LatLng(array[i][0],array[i][1]);
			self.draw(array[i]);
			nodes.selectedNodes[array[i]] = true;
		}
    }

    this.isNodeUsed = function(id){

        return self.points.indexOf(id) < 0 ? false : true;
    }

    this.toggleRemoving = function(){

        if(self.removeOnClick){
            self.removeOnClick = false;
            for(var i in self.points){
                nodes.markers[self.points[i]].setLabel('');
            }
        }
        else{
            self.removeOnClick = true;
            for(var i in self.points){
                nodes.markers[self.points[i]].setLabel('-');
            }
        }
    }

    this.setEvents = function(){
        google.maps.event.addListener(map.map,'mousemove',function(evt){

            if(self.moving || self.newNodeMoving){

                if(self.moving){
                    var key = self.getNodePosition(self.moving);
                }
                else{
                    var key = self.newNodeMoving;
                }
            
                var nearNode = false;

                for(var i in nodes.markers){
                    var x1 = evt.latLng.lat();
                    var y1 = evt.latLng.lng();
                    var x0 = nodes.markers[i].getPosition().lat();
                    var y0 = nodes.markers[i].getPosition().lng();
                    if(Math.sqrt((x1-x0)*(x1-x0) + (y1-y0)*(y1-y0)) < 0.0003){
                        self.path.setAt(key,new google.maps.LatLng({
                            'lat' : x0,
                            'lng' : y0
                        }));
                        nearNode = true;
                    }
                }

                if(!nearNode){
                    self.path.setAt(key,evt.latLng);
                }

            }
        })  

        google.maps.event.addListener(self.poly, 'mousedown', function(evt) {
            
            if(!self.newNodeMoving){
                var position = {
                    'x' : evt.latLng.lng(),
                    'y' : evt.latLng.lat()
                }

                var newPosition = 0;

                var path = self.path.getArray();

                for(var i = 1; i < path.length; i++){
                    var before = {
                        'x' : path[i - 1].lng(),
                        'y' : path[i - 1].lat()
                    };

                    var after = {
                        'x' : path[i].lng(),
                        'y' : path[i].lat()
                    };
                    if(isBetween(before, after , position, 0.00002)){
                        newPosition = i;
                    }
                }
                if(newPosition != 0){
                    self.path.insertAt(newPosition,new google.maps.LatLng({
                        'lat' : position.y + 0.0001,
                        'lng' : position.x + 0.0001
                    }));
                    self.newNodeMoving = newPosition;
                }
                map.map.setOptions({draggable: false});
            }
        });
    }

    self.run();

}