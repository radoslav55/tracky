function Map(index){

	var self = this;
    this.domElement = document.getElementById(index);
    this.dom = $('#' + index);
    this.domInput = $('#mapRoutes');

    this.isAlertAppended = false;
    this.currentCenter;
    
    this.properties = {
        zoom: 15,
        center: new google.maps.LatLng(42.9065721,23.784499699999998),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        zoomControl:true,
        disableDoubleClickZoom: true,
        scrollwheel: true,
        draggableCursor: "crosshair",
        
    };

    this.run = function(location, zoom, callback){

        map.properties.center = new google.maps.LatLng(location.lat,location.lng),
        map.properties.zoom = zoom;

    	this.init();
    	this.setEvents();

    	callback();
    }

    this.init = function(){
    	this.map = new google.maps.Map(this.domElement,this.properties);
	    this.service = new google.maps.DirectionsService();
        self.currentCenter = self.map.getCenter();
        google.maps.event.addListenerOnce(self.map, 'idle', function(){

            if(nodes.editable && currentNode != 0){

                nodes.clear();

                var latLng = {
                    'lat' : parseFloat(currentNode.lat),
                    'lng' : parseFloat(currentNode.lng),
                };

                nodes.drawNode(latLng,currentNode.name, currentNode.id, true);

                nodes.actualName = currentNode.name;
                nodes.actualId = currentNode.id;
                
                nodes.openInput(latLng);
                self.map.setCenter(new google.maps.LatLng(latLng));
            }

            self.loadVisibleNodes();
        });

	};
    this.setEvents = function(){
 	    //google.maps.event.addListener(this.map,'click',this.draw,false);
        
        $(window).unload(function(){

            $.ajax({
                type: 'GET',
                async: false,
                url: '/panel/settings/set',
                data: {
                    'last_admin_map_location' : {
                        'lat' : map.map.getCenter().lat,
                        'lng' : map.map.getCenter().lng
                    },
                    'last_admin_map_zoom' : map.map.getZoom()
                },
                success : function(data){
                    console.log(data);
                }
            });
        });

        google.maps.event.addListener(self.map,'center_changed',function(){
            var bounds = self.map.getBounds();

            if(bounds){
                var ne = bounds.getNorthEast();
                var sw = bounds.getSouthWest();

                if(
                    ne.lat() < sw.lat() ||
                    self.currentCenter.lat() > ne.lat() || 
                    self.currentCenter.lng() < sw.lng() ||
                    self.currentCenter.lng() > ne.lng()
                ){
                    
                    self.loadVisibleNodes();
                }
            }
            
        })

        google.maps.event.addListener(self.map,'zoom_changed',function(){
            self.loadVisibleNodes();
        })

        var search = new SmartSearch();
        search.handleField('#searchNode');

        search.onSearch(function(val){

            search.inside('nodes').search(val,function(output){
                console.log(output);

                $('.search-node-results').html('');
                for(var i in output){
                    if(i > 8) break;
                    $('.search-node-results').append('<li lat="' + output[i].lat + '" lng="' + output[i].lng + '">' + output[i].name + '</li>');
                }
                $('.search-node-results').slideDown('slow');
                
            });
        });

        search.onClean(function(){
            $('.search-node-results').slideUp('slow');
        });

       /* $('#searchNode').keydown(function(e){

            if(e.keyCode == 13){
                e.preventDefault();
                $.get('/panel/nodes/search',{
                    'string' : $(this).val()
                },function(json){
                    
                });
            }
        })*/

        $('body').on('click','.search-node-results li',function(){
            map.map.setCenter({
                'lat' : parseFloat($(this).attr('lat')),
                'lng' : parseFloat($(this).attr('lng'))
            })
            map.map.setZoom(17);
            self.loadVisibleNodes();
            $('.search-node-results').slideUp('medium');
            $('#searchNode').val('');
        })


    };

    /**
     * loadVisibleNodes method
     * Makes an AJAX request to recive the nodes that are visible by the user
     * The idea is to keep the map clean from nodes that are not visible by the user
     */
    this.loadVisibleNodes = function(){

        /*var bounds = self.map.getBounds();
        if(bounds){
            var ne = bounds.getNorthEast();
            var sw = bounds.getSouthWest();

            $.get('/panel/nodes/export',{
                'fromLat' : ne.lat() + ((ne.lat() - sw.lat()) / 2), // Extends the area by 50% for a better experience
                'fromLng' : ne.lng() + ((ne.lng() - sw.lng()) / 2),
                'toLat' : sw.lat() - ((ne.lat() - sw.lat()) / 2),
                'toLng' : sw.lng() - ((ne.lng() - sw.lng()) / 2)
            },function(json){
                
                var data = JSON.parse(json);
                console.log(data);
                if(data.status == 'warning'){
                    self.alert('Не са показвани всички възможни спирки','warning');
                }

                if(Object.keys(nodes.markers).length){
                    nodes.setVisible(data.nodes,false);
                }

                nodes.drawNodes(data.nodes,false);
            })
        }
        

        self.currentCenter = self.map.getCenter();*/

    }

    this.alert = function(text, type){
        if(!self.isAlertAppended){
            self.dom.parent('#map')
            .children('.map-alert')
            .children('.map-alert-middle')
            .html('<div class="' + type + '">' + text + '</div>');

            self.dom.parent('#map')
            .children('.map-alert')
            .fadeIn('slow');

            self.isAlertAppended = true;

            setTimeout(function(){
                self.dom.parent('#map')
                .children('.map-alert')
                .fadeOut('slow');

                self.isAlertAppended = false;
            },4500);
        }
    }

}


var isBetween = function(a,b,c, tolerance){

    //test if the point c is inside a pre-defined distance (tolerance) from the line
    var distance = Math.abs((c.y - b.y)*a.x - (c.x - b.x)*a.y + c.x*b.y - c.y*b.x) / Math.sqrt(Math.pow((c.y-b.y),2) + Math.pow((c.x-b.x),2));
    if (distance > tolerance){ return false; }

    //test if the point c is between a and b
    var dotproduct = (c.x - a.x) * (b.x - a.x) + (c.y - a.y)*(b.y - a.y)
    if(dotproduct < 0){ return false; }

    var squaredlengthba = (b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y - a.y);
    if(dotproduct > squaredlengthba){ return false; }

    return true;
};