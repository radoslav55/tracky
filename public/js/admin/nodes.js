function Nodes(index){

	var self = this;

    self.actualName = null;
    self.actualId = 0;

    self.markers = {};
    self.circles = {};

    self.editable = false;

    self.selectedNodes = [];

    self.blockedNodes = {};

    self.dragging = 0;

    /**
     * makeEditable method
     * Activates the funcionality for editing the nodes
     * If the nodes aren't editable, the edges ARE
     * 
     */
    this.makeEditable = function(){

    	self.editable = true;

        // Allows creating new nodes when clicking somewhere in the map
 	    google.maps.event.addListener(map.map,'click',function(){

            self.actualName = null;
            self.actualId = 0;

            self.openInput({
                'lat' : evt.latLng.lat(),
                'lng' : evt.latLng.lng()
            });
        },false);
    };

    /**
     * openInput method
     * appends an info window at the given lat and long
     * 
     * @param  float[] - The coordinates of the info window
     *  - lat
     *  - lng
     *  
     */
    this.openInput = function(latLng){

        self.clear();

    	self.infoWindow = new google.maps.InfoWindow();
    	var location = new google.maps.LatLng(latLng);

    	self.infoWindow.setContent('<div id="add_node_box">' + $('#addInput')[0].outerHTML + '</div>');

        //Set Position of InfoWindow.
        self.infoWindow.setPosition(location);

        //Open InfoWindow.
        self.infoWindow.open(map.map);

        // Close button event
        google.maps.event.addListener(self.infoWindow,'closeclick',function(){
        	self.clear();
		});

        // When the infoWindow is loaded
		google.maps.event.addListener(self.infoWindow, 'domready', function() {

            // Checks if we are editing some specific node or we are creating a new one
			if(self.markers[self.actualId]){
				self.changeMode('edit');
			}
			else{
				self.changeMode('add');
			}

            // Fills the name field with the current name (Only valid when editing)
			$('#add_node_box').find('#addNodeForm [name=name]').val(self.actualName);


			$('#submitNode').on('click',function(){

                var result = {};
                var obs = $('#add_node_box').find('#addNodeForm :input').serializeArray();

                $.each( obs, function() {

                    result[this.name] = this.value;
                });

                result['lat'] = latLng['lat'];
                result['lng'] = latLng['lng'];
                result['id'] = self.actualId;

				self.saveChangesToNode(result);

			});

			$('#removeNode').on('click',function(){

				$.get(deleteURL + '/' + self.actualId,{},function(data){

					if(data['type'] == 'success'){

						self.removeNode(self.actualId);
					}
					else{

						map.alert(data['body'],data['type']);
					}
				});
			})
		});
    }

    /**
     * blockNodes method
     * Makes the given nodes unhiddable by the system
     * They will be there even the user doesn't see them
     * 
     * @param  int[] nodes - The id's of the nodes to block

     */
    this.blockNodes = function(nodes){
        for(var i in nodes){
            self.blockedNodes[nodes[i]] = true;
        }
    }

    /**
     * saveChangesToNode method
     * Saves the current information for a node to the server
     * 
     * @return {[type]} [description]
     */
    this.saveChangesToNode = function(result){

        map.dom.waiting('Изчисляване на връзките между спирките за пешеходци...<br>Процесът може да отнеме няколко минути');

        $.ajax({
            url: window.location.href,
            type:"POST",
            data: result,
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
            success:function(data){

                if(data['status'] == 'success'){

                    self.drawNode({
                        'lat' : parseFloat(data.info.lat),
                        'lng' : parseFloat(data.info.lng)
                    },data.info.name,data.info.id,true);

                    self.clear();
                    loadNextNode();
                }
                map.dom.stopWaiting();
                map.alert("Промените са запазени успешно","success");
            },
            error:function(data){ 
                $.each(data.responseJSON, function (key, value) {
                    var element = $('#add_node_box').find('#addNodeForm [name=' + key + ']');
                    element.css('border-color','red');
                    element.parent('div').find('.error-span').html(value);
                });
                map.dom.stopWaiting();
                
            }
        }); //end of ajax
    }

    this.drawNodes = function(array, overrite){
     	
    	for(var i in array){
    		self.drawNode({
    			'lat' : parseFloat(array[i].lat),
    			'lng' : parseFloat(array[i].lng),
    			
    		},array[i].name,array[i].id, overrite);
    	}
    }

    this.setVisible = function(array, overrite){

        for(var i in self.markers){
            var toRemove = true;
            for(var j in array){
                if(j == i){
                    toRemove = false;
                }
            }
            if(toRemove && self.dragging != i){
                self.removeNode(i);
            }
        }
    }

    this.drawNode = function(latLng, name, id, overrite){

        if(overrite){
            console.log(self.dragging + ' - ' + id);
            if(self.markers[id] && self.dragging != id){
                self.removeNode(id);
            }
        }

        if((!overrite && !self.markers[id]) || overrite){
            self.markers[id] = new MarkerWithLabel({
                position: new google.maps.LatLng(latLng),
                map: map.map,
                id : id,
                labelContent: name,
                labelAnchor: new google.maps.Point(22, 0),
                labelClass: "labels", // the CSS class for the label
                labelStyle: {opacity: 0.75},
                
            });

            if(self.editable){

                self.markers[id].setDraggable(true);

                google.maps.event.addListener(self.markers[id],'dragstart',function(){
                    self.dragging = id;
                })

                google.maps.event.addListener(self.markers[id],'dragend',function(evt){
                    if(confirm('Искате ли да преместите спирката?')){
                        self.saveChangesToNode({
                            'id' : id,
                            'lat' : evt.latLng.lat(),
                            'lng' : evt.latLng.lng(),
                            'name' : name,
                        })
                    }
                    else{
                        self.markers[id].setPosition(new google.maps.LatLng(latLng));
                    }
                    self.dragging = 0;
                })
            }

            google.maps.event.addListener(self.markers[id],'click',function(evt){
                if(self.editable){
                    self.clear();
                    self.actualName = name;
                    self.actualId = id;
                    self.openInput(latLng);
                }
                else if(edges.removeOnClick){
                    edges.remove(id);
                }
                else{

                    if(!edges.isNodeUsed(id)){
                        edges.draw(id);
                    }
                    else{
                        map.alert('Маршрутът не може да минава през една спирка повече от веднъж. Трябва да създадете нов маршрут', 'danger');
                    }
                }
            });

            google.maps.event.addListener(self.markers[id],'mousedown',function(evt){

                if(!self.editable && edges.isNodeUsed(id) && !edges.removeOnClick){
                    edges.startNodeDragging(id);
                }
            });

        }
        
        /**
         * overrite = true
         * if exists remove
         * append new
         *
         * overrite = false
         * if doesnt exist append
         * 
         */

		

        $('body').on('mouseup',function(evt){

            if(!self.editable){
                edges.stopNodeDragging();
            }
        });
	}

    /**
     * removeNode method
     * Used to remove a node from the map and from the array
     * @param  int id - The id of the node
     */
    self.removeNode = function(id){

        if(self.editable || !edges.points.indexOf(id)){
            google.maps.event.clearListeners(self.markers[id], 'click');
            self.markers[id].setMap(null);
            delete self.markers[id];
            self.clear();
        }

    }

    this.updateBeginAndEnd = function(points){
        for(var i in self.markers){
            self.markers[i].set('labelContent',self.markers[i].get('labelContent').replace('<br><span style="font-size:14px">Начало</span>',''));
            self.markers[i].set('labelContent',self.markers[i].get('labelContent').replace('<br><span style="font-size:14px">Край</span>',''));
        }

        self.markers[points[0]].set('labelContent',self.markers[points[0]].get('labelContent') + ' <br><span style="font-size:14px">Начало</span>');
        self.markers[points[points.length - 1]].set('labelContent',self.markers[points[points.length - 1]].get('labelContent') + ' <br><span style="font-size:14px">Край</span>' );
    }

    this.getClosest = function(x1,y1){

        var minId = 0;
        var minValue = 9999999999999;

        for(var i in nodes.markers){

            var x2 = nodes.markers[i].getPosition().lat();
            var y2 = nodes.markers[i].getPosition().lng(); 

            var distance = Math.sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
            if(distance < minValue){
                minValue = distance;
                minId = i;
            }
        }

        return {
            'id' : minId,
            'distance' : minValue
        }
    }


    this.changeMode = function(mode){
    	if(mode == 'add'){
    		$('#submitNode').html('Добави');
    		$('#removeNode').hide();
    	}
    	else if(mode == 'edit'){
    		$('#submitNode').html('Редактирай');
    		$('#removeNode').show();
    	}
    }


    this.clear = function(){
    	if(self.circles[self.actualId]){
    		self.circles[self.actualId].setMap(null);
    	}
    	if(self.infoWindow){
    		self.infoWindow.setMap(null);
    	}
    }

}
