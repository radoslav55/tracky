var height;
$(document).ready(function(){

	height = $('.welcome').height();
	$('.button').click(function(){
		$('.map').css('visibility','visible');
	    $("html, body").animate({ scrollTop: $(window).height() },800);
	    $('.container-background').animate({
	    	'top' : - $(window).height()
	    },function(){
	    	$('.navbar a').css('color','black');
	    	$('.container-background').remove();
	    	$('.navbar').hide();

	    });
	});

   	resize();

   	// User's control bar event handling

   	var cacheText = null;

	$('.toSelect').click(function(){
		map.changeChoosing('to');
        if(map.destination){
            map.map.setCenter(map.destination);
        }
	})

	$('.fromSelect').click(function(){
		map.changeChoosing('from');
        if(map.position){
            map.map.setCenter(map.position);
        }
	})

	$('.getCurrentLocation').click(function(){
		map.locate();
	})

	$('#from').focus(function(){

		cacheText = $(this).val();

		$(this).select();
		$(this).attr('class','');

		map.autocomplete('from');

	})

	$('#to').focus(function(){

		$(this).select();

		$(this).attr('class','');

		map.autocomplete('to');

	})

	$('#to').blur(function(){

		map.synchTextField('to');
	})

	$('#from').blur(function(){

		map.synchTextField('from');
	})

	$('.switchButton').click(function(){

		map.reverseFields();
	})

	$('body').on('click','.stop-animation',function(){
		map.animation = false;
		$(this).attr('class','start-animation');
	})

	$('body').on('click','.start-animation',function(){
		map.animation = true;
		$(this).attr('class','stop-animation');
	
	})

	$('.fast-forward').click(function(){
		map.speed = map.initialSpeed * 2;
	})

	$('.fast-rewind').click(function(){
		map.speed = map.initialSpeed / 2;
	})

	$('.skip-animation').click(function(){
		map.speed = 1;
		map.infoWindow.setMap(null);
	})

	$('#close-statistics').click(function(){
		$('.general-statistic').parent('.black-background').parent('.general-statistic-fullbox').fadeOut('medium');
	})

	$('.status-table .row').each(function(){
		$(this).find('.col').last().css('text-align','right');
	})

	$('#open-general-statistic').click(function(){
		map.terminate();
	})

	$('#new-marchrute').click(function(){

		map.clearPolylines();
		map.cleanNodes();
		map.infoWindow.setMap(null);
		$('.animation-control-buttons').slideUp('slow');
		$('.animation-control-buttons-right').hide();
		$('.block-controls').hide();
		map.speed = map.initialSpeed;
		map.textRoute = [];
		map.blockedControls = false;

		map.startPin.setDraggable(true);
		map.endPin.setDraggable(true);
		map.absoluteTime = new Date().getTime();

	})

	var hourControllerCanBeAnimated = true;
	var expanded = false;

    $('.hourController').on('mouseenter',function(){
        $(this).animate({
            top:0
        },300,function(){
            $(this).find('.hourTitle').animate({
                opacity: 0
            },300);
        });
        
    })

    $('.hourController').on('mouseleave',function(){
        $(this).animate({
            top:'-31px'
        },200,function(){
            $(this).find('.hourTitle').animate({
                opacity: 1
            },300);
        });
        
    })

    $('#understood').click(function(){
        $.get('/acceptedwarning');
        $('#warning').animate({
            'bottom' : function(){
                return '-' + $('#warning').outerHeight()
            }()
        },500);
    });

	/*$('.hourController').on('mouseenter',function(){
		
		if(hourControllerCanBeAnimated){
			hourControllerCanBeAnimated = false;
			$(this).animate({
	    		'right' : 0
	    	},300,function(){
	    		hourControllerCanBeAnimated = true
	    	})
	    	expanded = true;
		}
	    	
    })

	$('.hourController').on('mouseleave', function(){

		if(hourControllerCanBeAnimated){
			hourControllerCanBeAnimated = false;
	    	$(this).animate({
	    		'right' : '-260px'
	    	},300,function(){
    			hourControllerCanBeAnimated = true;
    		});
	    }
    })*/

    $('.hourController .clockIcon').click(function(){

    	/*var right = 0;
    	if(expanded){	
    		right = '-260px';
    		expanded = false;
    	}
    	else{
    		expanded = true;
    	}

    	$('.hourController').animate({
    		'right' : right
    	},300,function(){
    		hourControllerCanBeAnimated = true
    	})*/
    })

	//var today = date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear();
	//$('#leavingTime').bootstrapMaterialDatePicker({ date: false,format : 'HH:mm' });
	//$('#leavingDate').bootstrapMaterialDatePicker({ weekStart : 0, time: false  });

	//$('#leavingTime').val(date.getHours() + ':' +  (date.getMinutes()<10?'0':'') + date.getMinutes() );
	//$('#leavingDate').val(today);

})

$(window).resize(function(){
	resize();
})

function resize(){
	var divide = 1;
	if($(window).width() < 700){
		$('.navbar').hide();
		divide = 2;
	}
	$('.welcome').css('padding-top',((($(window).height()- height) / 2) - height / 8) - $('nav').height() );
	$('.button').css('margin-top',
		$(window).height() - 
		(height + parseInt($('.welcome').css('padding-top')) +
		 $('.button').height() / divide ) - 
		$('nav').height()
	);
	if($('.button').css('margin-top') < 0){
		$('.button').css('margin-top',0);
	}
	if($(window).width() < 700){
		$('.navbar').hide();
	}

	var left =  ($(window).width() - $('.directionsController').outerWidth()) / 2;
    $('.block-controls').width($('.directionsController').outerWidth());
    $('.block-controls').height($('.directionsController').height());
    if(left > 0){
    	$('.animation-control-buttons').css('left',left);
    	$('.directionsController').css('left',left);
    	$('.block-controls').css('left',left);
    }
    $('.animation-control-buttons').width($('.directionsController').outerWidth() - 8);
    $('.animation-control-buttons').css('top',$('.directionsController').height() + 20);

    var totalAnimationControlButtonsWidth = 0;

    $('.animation-control-buttons-centerer div').each(function() {
	    if($(this).attr('class') != 'clear'){

	    	totalAnimationControlButtonsWidth += parseInt($(this).width());
			
	    }
	});


    //$('.animation-control-buttons-centerer').width(totalAnimationControlButtonsWidth);


    var box = $('.status-table').closest('.general-statistic');
    var neightbourWidth = $('.status-table').closest('.general-statistic').find('.arrive-icon').width();

   	$('.status-table').width(box.width() - neightbourWidth - 90);
   	$('.status-table').height(box.height() - $('.message-text').height() - 80);

   	//left = parseInt($('.directionsController').css('left')) + parseInt($('.directionsController').width()) + 110;

}