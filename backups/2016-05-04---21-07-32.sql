-- 2016-05-04T21:07:32+03:00 - mysql:dbname=tracky;host=localhost

-- Table structure for table `edges`

DROP TABLE IF EXISTS `edges`;
CREATE TABLE `edges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `route_id` int(10) unsigned DEFAULT '0',
  `from` int(10) unsigned NOT NULL,
  `to` int(10) unsigned NOT NULL,
  `distance` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `edges_route_id_foreign` (`route_id`),
  KEY `edges_from_foreign` (`from`),
  KEY `edges_to_foreign` (`to`),
  CONSTRAINT `edges_from_foreign` FOREIGN KEY (`from`) REFERENCES `nodes` (`id`),
  CONSTRAINT `edges_route_id_foreign` FOREIGN KEY (`route_id`) REFERENCES `routes` (`id`),
  CONSTRAINT `edges_to_foreign` FOREIGN KEY (`to`) REFERENCES `nodes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table `edges`

LOCK TABLES `edges` WRITE;
UNLOCK TABLES;

-- Table structure for table `holidays`

DROP TABLE IF EXISTS `holidays`;
CREATE TABLE `holidays` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `day` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `holidays_day_unique` (`day`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table `holidays`

LOCK TABLES `holidays` WRITE;
UNLOCK TABLES;

-- Table structure for table `lines`

DROP TABLE IF EXISTS `lines`;
CREATE TABLE `lines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `line` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `vehicle_id` int(10) unsigned NOT NULL,
  `has_wizard` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lines_vehicle_id_foreign` (`vehicle_id`),
  CONSTRAINT `lines_vehicle_id_foreign` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table `lines`

LOCK TABLES `lines` WRITE;
UNLOCK TABLES;

-- Table structure for table `logs`

DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(511) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table `logs`

LOCK TABLES `logs` WRITE;
INSERT INTO `logs` VALUES (1,2,'http://localhost:8000','home',NULL,1462385234);
INSERT INTO `logs` VALUES (2,2,'http://localhost:8000/panel/login','adminPageLoaded',NULL,1462385240);
INSERT INTO `logs` VALUES (3,2,'http://localhost:8000/panel/login','adminPageLoaded',NULL,1462385245);
INSERT INTO `logs` VALUES (4,2,'http://localhost:8000/panel/login','adminLogIn',NULL,1462385245);
INSERT INTO `logs` VALUES (5,2,'http://localhost:8000/panel','adminPageLoaded',NULL,1462385246);
UNLOCK TABLES;

-- Table structure for table `migrations`

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table `migrations`

LOCK TABLES `migrations` WRITE;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1);
INSERT INTO `migrations` VALUES ('2015_10_11_124409_CreateVehiclesTable',1);
INSERT INTO `migrations` VALUES ('2015_10_28_231037_CreateRoutesTable',1);
INSERT INTO `migrations` VALUES ('2015_12_10_161815_CreateEdgesTable',1);
INSERT INTO `migrations` VALUES ('2015_12_10_161827_CreateNodesTable',1);
INSERT INTO `migrations` VALUES ('2016_02_09_215644_CreateScheduleTable',1);
INSERT INTO `migrations` VALUES ('2016_02_09_215729_CreateShceduleHoursTable',1);
INSERT INTO `migrations` VALUES ('2016_02_22_222729_CreateLogsTable',1);
INSERT INTO `migrations` VALUES ('2016_02_24_164334_CreateSettingsTable',1);
INSERT INTO `migrations` VALUES ('2016_03_09_145406_CreateHolidaysTable',1);
INSERT INTO `migrations` VALUES ('2016_03_17_163029_CreateTownsTable',1);
INSERT INTO `migrations` VALUES ('2016_03_29_183057_CreateLinesTable',1);
INSERT INTO `migrations` VALUES ('2016_05_03_122402_AddForeignKeys',1);
UNLOCK TABLES;

-- Table structure for table `nodes`

DROP TABLE IF EXISTS `nodes`;
CREATE TABLE `nodes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lat` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `lng` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `town_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nodes_town_id_foreign` (`town_id`),
  CONSTRAINT `nodes_town_id_foreign` FOREIGN KEY (`town_id`) REFERENCES `towns` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table `nodes`

LOCK TABLES `nodes` WRITE;
UNLOCK TABLES;

-- Table structure for table `routes`

DROP TABLE IF EXISTS `routes`;
CREATE TABLE `routes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `line_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `routes_line_id_foreign` (`line_id`),
  CONSTRAINT `routes_line_id_foreign` FOREIGN KEY (`line_id`) REFERENCES `lines` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table `routes`

LOCK TABLES `routes` WRITE;
UNLOCK TABLES;

-- Table structure for table `schedule`

DROP TABLE IF EXISTS `schedule`;
CREATE TABLE `schedule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `days` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table `schedule`

LOCK TABLES `schedule` WRITE;
INSERT INTO `schedule` VALUES (1,'weekdays');
INSERT INTO `schedule` VALUES (2,'holidays');
UNLOCK TABLES;

-- Table structure for table `schedule_hours`

DROP TABLE IF EXISTS `schedule_hours`;
CREATE TABLE `schedule_hours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schedule_id` int(10) unsigned NOT NULL,
  `route_id` int(10) unsigned NOT NULL,
  `node_id` int(10) unsigned NOT NULL,
  `hour` time NOT NULL,
  `status` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `schedule_hours_schedule_id_foreign` (`schedule_id`),
  KEY `schedule_hours_route_id_foreign` (`route_id`),
  KEY `schedule_hours_node_id_foreign` (`node_id`),
  CONSTRAINT `schedule_hours_node_id_foreign` FOREIGN KEY (`node_id`) REFERENCES `nodes` (`id`),
  CONSTRAINT `schedule_hours_route_id_foreign` FOREIGN KEY (`route_id`) REFERENCES `routes` (`id`),
  CONSTRAINT `schedule_hours_schedule_id_foreign` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table `schedule_hours`

LOCK TABLES `schedule_hours` WRITE;
UNLOCK TABLES;

-- Table structure for table `settings`

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table `settings`

LOCK TABLES `settings` WRITE;
INSERT INTO `settings` VALUES (1,'site_name','http://tracky-app.com');
INSERT INTO `settings` VALUES (2,'posts_by_page',10);
INSERT INTO `settings` VALUES (3,'max_nodes_distance',8000);
INSERT INTO `settings` VALUES (4,'max_edges_count',20);
INSERT INTO `settings` VALUES (5,'animation_speed','0.00002');
INSERT INTO `settings` VALUES (6,'ignore_walking_percent',5);
INSERT INTO `settings` VALUES (7,'last_admin_map_location','{\"lat\":\"42.59217410588496\",\"lng\":\"25.544529855251312\"}');
INSERT INTO `settings` VALUES (8,'last_admin_map_zoom',7);
UNLOCK TABLES;

-- Table structure for table `towns`

DROP TABLE IF EXISTS `towns`;
CREATE TABLE `towns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table `towns`

LOCK TABLES `towns` WRITE;
UNLOCK TABLES;

-- Table structure for table `users`

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(60) COLLATE utf8_unicode_ci DEFAULT 'guest',
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cookie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL,
  `blockedTo` int(11) NOT NULL,
  `next_reset` datetime NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table `users`

LOCK TABLES `users` WRITE;
INSERT INTO `users` VALUES (1,'root','$2y$10$SOIjk2B/411PtbRggftOPeYBWv4xjQjINB/ZThgT5t.3m8RVPqM46','root',NULL,NULL,0,0,'0000-00-00 00:00:00',NULL,'2016-05-04 21:06:37','2016-05-04 21:06:37',NULL);
INSERT INTO `users` VALUES (2,'guest',NULL,'guest','::1','3daFCLuyPrvrRwCyUPqWBXXy2H3irML9ZG5MkkajXbt4p7EYNkpdwKIanxOs',0,0,'0000-00-00 00:00:00',NULL,'2016-05-04 21:07:14','2016-05-04 21:07:14',NULL);
UNLOCK TABLES;

-- Table structure for table `vehicles`

DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table `vehicles`

LOCK TABLES `vehicles` WRITE;
UNLOCK TABLES;

-- Completed on: 2016-05-04T21:07:32+03:00
