<?php

namespace App\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;

class RoutesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('route', function($attribute, $value, $parameters, $validator) {

            $array = json_decode($value);

            if(!empty($array)){

                if(count($array) != count(array_unique($array, SORT_REGULAR))){
                    return false;
                }
            }
            
            return true;
        });

        Validator::extend('noEmptyJson', function($attribute, $value, $parameters, $validator) {

            $array = json_decode($value);

            if(empty($array)){
                return false;
            }

            return true;
        });


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Connection::class, function ($app) {
            return new Connection($app['config']['riak']);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [Connection::class];
    }}
