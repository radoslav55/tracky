<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group([
    //'middleware' => 'csrf'
    //'middleware' => 'RedirectToHttps'
],function(){

    // Routes for users
    Route::get('/','HomeController@index');
    Route::get('/about-us','HomeController@aboutUs');
    Route::get('/download','HomeController@download');

    Route::any('/sendmail','HomeController@postMail');

    Route::get('/routes/calculate','RouteController@calculate');
    Route::get('/routes/debug','RouteController@debug');

    Route::get('/acceptedwarning','HomeController@warning');

    // Control panel routes 
    Route::group([
        'namespace' => 'Admin',
    ],function(){

        // Routes for users that are not logged in 
        Route::group([
            'middleware' => 'checkGuest'
        ],function(){

            Route::get('/panel/login','AdminController@getLogin');
            Route::post('/panel/login','AdminController@postLogin');
        });

        // Routes for logged in administrators    
        Route::group([
            'middleware' => 'checkAdmin'
        ],function(){

            Route::get('/panel/migrate','DatabaseController@migrate');
            Route::get('/panel/mysql','DatabaseController@backup');
            Route::get('/panel/mysqlUpload','DatabaseController@upload');
            
            Route::get('/panel','HomeController@index');
            Route::get('/panel/logout','AdminController@logout');

            Route::controller('/panel/nodes','NodesController');

            Route::controller('/panel/vehicles','VehiclesController');

            Route::controller('/panel/bin','BinController');

            Route::controller('/panel/routes','RoutesController');

            Route::controller('/panel/lines','LinesController');
            
            Route::get('/panel/schedule/add','ScheduleController@add');
            Route::get('/panel/schedule/gethours','ScheduleController@getHours');
            Route::get('/panel/schedule/delete','ScheduleController@delete');
            Route::get('/panel/schedule/{id}','ScheduleController@show');

            Route::get('/panel/stats','StatsController@index');
            Route::get('/panel/stats/{search}','StatsController@getData');        

            Route::get('/panel/settings','SettingsController@index');
            Route::post('/panel/settings','SettingsController@save');
            Route::get('/panel/settings/addholiday','SettingsController@addHoliday');
            Route::get('/panel/settings/deleteholiday','SettingsController@deleteHoliday');
            Route::get('/panel/settings/set','SettingsController@set');

        });
    });


});