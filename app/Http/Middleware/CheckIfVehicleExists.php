<?php

namespace App\Http\Middleware;

use Closure;
use Vehicles;

class CheckIfVehicleExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $data = Vehicle::where('slug',$request->vehicle);
        if($data->exists()){
            return $next($request);
        }
        else{
            return redirect('panel/vehicles')->with('message',[
                'type' => 'danger',
                'body' => 'Транспортното средсвто не бе намерено'
            ]); 
        }
    }
}
