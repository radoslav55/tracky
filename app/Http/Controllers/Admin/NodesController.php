<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Node;
use App\Models\Vehicle;
use App\Models\Edge;
use App\Models\System;
use App\Models\Setting;
use App\Models\Town;
use App\Models\Search;
use App\Http\Requests\AddEditNode;
use URL;

class NodesController extends BaseController
{

    public $page = 'nodes';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request)
    {

        if($request->has('town')){

            $nodes = Node::where('town_id',$request->input('town'));
        }
        else{

            $nodes = new Node();
        }

        return view('panel.nodes',$this->inputs,[
            'title' => 'Спирки',
            'nodes' => $nodes->paginate(Setting::get('posts_by_page')),
            'towns' => Town::getForNodes(),
            'buttons' => [
                [
                    'text' => 'Добавяне / Редактиране на спирки',
                    'link' => url('/panel/nodes/manage'),
                    'action' => 'add-new'
                ]
            ]
        ]);
    }

    /**
     * Show the map to manage all the nodes
     *
     * @return \Illuminate\Http\Response
     */
    public function getManage(Request $request)
    {

        if($request->has('nodeId') && $node = Node::find($request->input('nodeId'))){
            $this->inputs['currentNode'] = $node;
        }
        return view('panel.forms.nodes',$this->inputs,[
            'title' => 'Добавяне / Редактиране на спирки',
            'location' => Setting::get('last_admin_map_location'),
            'zoom' => Setting::get('last_admin_map_zoom'),
            'buttons' => [
                [
                    'action' => 'go-back'
                ]
            ]
        ]); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postManage(AddEditNode $request)
    {
        
        $node = Node::findOrCreate($request->input('id'));

        $node->name = $request->input('name');
        $node->lat = System::formatCoordinate($request->input('lat'));
        $node->lng = System::formatCoordinate($request->input('lng'));
        $node->town_id = Town::handle(System::getTown([
            'lat' => $request->input('lat'),
            'lng' => $request->input('lng')
        ]));

        $action = $node->save();

        /*foreach($node->closest() as $closeNode){

            $node->connectWith($closeNode)->walking();
        }*/

        if($action){
            return [
                'status' => 'success',
                'info' => $node
            ];
        }

        return ['status' => 'error'];

    }

    public function getExport(Request $request){

        if(
            $request->has('fromLat') &&
            $request->has('fromLng') &&
            $request->has('toLat') &&
            $request->has('toLng')
        ){
            $nodes = Node::where('lat','<',$request->input('fromLat'))
                ->where('lat','>',$request->input('toLat'))
                ->where('lng','<',$request->input('fromLng'))
                ->where('lng','>',$request->input('toLng'))
                ->limit(100)
                ->get(['id','name','lat','lng'])
                ->keyBy('id');

            echo json_encode([
                'status' => $nodes->count() < 100 ? 'success' : 'warning',
                'nodes' => $nodes
            ]);
        }
    }

    public function getFind(Request $request){

        if($request->has('string')){
            $nodes = Node::all();
            $search = Search::strings();

            $output = [];

            foreach($nodes as $node) {           

                if(search($request->input('string'))->in($node->name)){
                //if($search->in($node->name)->for($request->input('string'))){
                    
                    $output[] = array_merge($node->toArray(),[
                        'town' => $node->town->toArray()
                    ]);

                }   
            }


            return [
                'status' => 'success',
                'data' => $output
            ];
        }

        return [
            'status' => 'danger',
            'message' => 'Невалиден вход'
        ];
    }

    public function getNext(){

        return Node::where('updated_at','<','2016-05-12 16:39:17')->first()->toJson();
    }

    public function getPercents(){


        $all = Node::all()->count();
        $still = Node::where('updated_at','<','2016-05-12 16:39:17')->count();
        $edited = $all - $still;

        $last = Node::where('id','>',1)->orderBy('updated_at',DESC)->first()->updated_at;
        $last = strtotime((string) $last);

        $timeForAll = $last - strtotime('2016-05-12 16:39:17');
        $timeForOne = (int) ($timeForAll / $edited);
        echo $all . '<br>' . $edited . '<br>' . ($edited * 100 / $all) . '%<br><br>';
        echo $timeForOne . ' secs per node<br>';
        echo 'You need ' . (int) ($still * $timeForOne / 60 / 60) . ' hours more'; 

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDelete(Request $request,$id)
    {
        $message = [];
        $node = Node::find($id);
        $isUsed = Edge::isNodeUsed($id);

        if($node){
            if(!$isUsed){
                
                $node->delete();

                $message = [
                    'type' => 'success',
                    'body' => 'Спирката е успешно изтрита'
                ];
            }
            else{
                $message = [
                    'type' => 'danger',
                    'body' => 'Тази спирка се използва в някои маршрути. Не може да бъде изтрита'
                ];
            }
            
        }
        else{
            $message = [
                'type' => 'danger',
                'body' => 'Спирката не е намерена'
            ];
        }

        if($request->ajax()){
            return $message;
        }
        else{
            if($message['type'] == 'success'){
                return redirect()->to('/panel/nodes')->with('message',$message);
            }
            else{
                return redirect()->back()->with('message',$message);
            }
        }
    }

    public function getRestore($id){

        $data = Node::withTrashed()->find($id);

        if($data){

            $action = $data->restore();

            return redirect()->back()
                ->with('message',[
                    'type' => 'success',
                    'body' => 'Спирката е възстановена успешно'
            ]);
            
        }
        else{
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Възникна грешка при възстановяването на спирката'
            ]);
        }
    }

    public function getDestroy($id){
        $data = Node::withTrashed()->find($id);

        if($data){

            $data->destroyWithChildElements();

            return redirect()->back()
                ->with('message',[
                    'type' => 'success',
                    'body' => 'Спирката е изтрита успешно'
            ]);
        }
        else {
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Възникна грешка при изтриването на спирката'
            ]);
        }
    }
}
