<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Log;
use App\Models\Town;

class StatsController extends BaseController
{
    
    public $page = 'stats';
    
    public function index()
    {
        return view('panel.stats',$this->inputs,[
            'title' => 'Статистики',
            'visitedTowns' => Log::towns(),
            'towns' => Town::all()->keyBy('id'),
        ]);
        
    }
    
    public function getData($search){
        
        if($search == 'last_24' || $search == 'last_30' || $search == 'all'){
            return Log::data($search);
        }
    }

}