<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Bin;

class BinController extends BaseController
{

    public $page = 'bin';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {

        return view('panel.bin',$this->inputs,[
            'title' => 'Кошче',
            'data' => Bin::getRecicledItems(),
            'buttons' => [
                [
                    'action' => 'go-back'
                ]
            ]
        ]);
    }
    
}
