<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Setting;
use App\Models\Holiday;
use App\Http\Requests\SettingsForm;

class SettingsController extends BaseController
{

    public $page = 'settings';
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('panel.settings',$this->inputs,[
            'title' => 'Настройки',
            'settings' => Setting::getAll(),
            'holidays' => Holiday::all(),
            'buttons' => [
                [
                    'action' => 'go-back'
                ]
            ]
        ]);
    }

    public function save(SettingsForm $request){

        foreach(Setting::$settings as $setting){
            Setting::set($setting,$request->input($setting));
        }

        return redirect()->to('/panel/settings')->with('message',[
            'type' => 'success',
            'body' => 'Настройките са обновени успешнo'
        ]);
    }

    public function addHoliday(Request $request){

        if($request->has('date')){

            if(strtotime($request->input('date')) > time()){

                if(!Holiday::where('day',$request->input('date'))->count()){
                    $action = Holiday::create([
                        'day' => $request->input('date')
                    ]);

                    if($action){
                        return [
                            'status' => 'success',
                            'id' => $action->id,
                            'date' => $request->input('date')
                        ];
                    }
                }
                else{
                    return [
                        'status' => 'error',
                        'message' => 'Датата вече съществува'
                    ]; 
                }
                
            }
            else{

                return [
                    'status' => 'error',
                    'message' => 'Датата трябва да бъде след днес'
                ];
            }
        }

        return [
            'status' => 'error',
            'message' => 'Невалиден вход'
        ];
    }

    public function deleteHoliday(Request $request){

        Holiday::where('id',$request->input('id'))->delete();

        return [
            'status' => 'success'
        ];
    }

    public function set(Request $request){

        foreach($request->all() as $setting=>$val){

            Setting::set($setting, $val);
        }
    }
}
