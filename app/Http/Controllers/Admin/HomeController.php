<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Log;
use App\Models\Setting;


class HomeController extends BaseController
{

    public function index()
    {

        return view('panel.index',$this->inputs,[
        	'title' => 'Tracky - Начало',
        	'logs' => Log::getArrayForHome(),
        	'settings' => Setting::getForHome()
        ]);
    }
}
