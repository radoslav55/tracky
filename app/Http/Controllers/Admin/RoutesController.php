<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\AddRoute;
use App\Models\Node;
use App\Models\Setting;
use App\Models\Line;
use App\Models\Vehicle;
use App\Models\System;
use App\Models\Schedule;
use App\Models\Route;
use Session;


class RoutesController extends BaseController
{


    public function getCreate($line)
    {

        $line = Line::find($line);

        if($line){

            $this->inputs['page'] = 'vehicle.' . $line->vehicle_id;
            $this->inputs['buttons'] = [];

            $wizardStatus = $line->wizardStatus();

            // We are in the wizard
            if($wizardStatus){

                // The current step is the second
                if($wizardStatus == 2){

                    // Ads the first route's mirror route
                    $this->inputs['routeNodes'] =  json_encode(Route::getMirror($line->routes()->first()->id));

                    // Ads the nodes inside the mirror route for preload and an alert message
                    $this->inputs['nodes'] =  Node::whereIn('id',json_decode($this->inputs['routeNodes']))->get()->keyBy('id')->toJson();
                    $this->inputs['mapAlert'] = [
                        'text' => 'Автоматично генериран огледален маршрут',
                        'type' => 'success'
                    ];
                }

                // Sets the current step for the visualization
                $this->inputs['wizard'] = $wizardStatus;
                $this->inputs['buttons'][] = [
                    'text' => 'Изключи помощника',
                    'link' => url('panel/routes/cancelwizard/' . $line->id),
                    'type' => 'default',
                ];
            }

            $this->inputs['buttons'][] = [
                'action' => 'go-back'
            ];



            return view('panel.forms.route',$this->inputs,[
                'title' => 'Добави нов маршрут',
                'location' => Setting::get('last_admin_map_location'),
                'zoom' => Setting::get('last_admin_map_zoom'),
                
            ]);
        }

        return redirect('panel/vehicles')->with('message',[
            'type' => 'danger',
            'body' => 'Линията не е намерена'
        ]);
        
    }

    public function postCreate(AddRoute $request,$line)
    {

        $lineInstance = Line::find($line);
        if($lineInstance){

            $needsRoutes = $lineInstance->wizardStatus();

            $action = Route::createOrUpdate([
                'line_id' => $line,
                'route' => $request->input('route'),
            ]);

            if($action){

                if($needsRoutes == 2){
                    
                    return redirect('/panel/routes/endwizard/' . $line);
                }
                else if($needsRoutes == 1){

                    return redirect('/panel/routes/create/' . $line);
                }

                return redirect('/panel/lines/show/' . $lineInstance->vehicle_id)->with('message',[
                    'type' => 'success',
                    'body' => 'Маршрутът е създаден успешно'
                ]);

                
            }
            else {
                return redirect()->back()->with('message',[
                    'type' => 'danger',
                    'body' => 'Възникна грешка при добавянето на маршрута'
                ]);
            }
        }

        return redirect()->back()->with('message',[
            'type' => 'danger',
            'body' => $this->getMessage('action_show_notFound')
        ]);
                
    
    }

    public function getWizard($line){

        if(Line::find($line)){
            if(Line::needsRoutes($line)){

                $this->inputs['page'] = 'vehicle.' . Line::find($line)->vehicle_id;

                return view('panel.wizards.routeHome',$this->inputs,[
                    'title' => '',
                    'line' => $line,
                ]);
            }
            else{
                return redirect('/panel/lines/show/' . $line);
            }
        }
        else{
            return redirect('/panel/vehicles')->with('message',[
                'type' => 'danger',
                'body' => 'Линията не е намерена'
            ]);
        }

    }

    public function getCancelwizard($lineId){

        $line = Line::find($lineId);
        if($line){
            Line::where('id',$lineId)->update([
                'has_wizard' => 0,
            ]);

            return redirect('/panel/lines/show/' . $line->vehicle_id);
        }
        else{
            return redirect('/panel/vehicles')->with('message',[
                'type' => 'danger',
                'body' => 'Линията не е намерена'
            ]);
        }
    }

    public function getEndwizard($line){

        $line = Line::find($line);

        if($line && !$line->wizardStatus()){

            $this->inputs['page'] = 'vehicle.' . $line->vehicle_id;

            $routes = array_keys($line->routes()->get()->keyBy('id')->toArray());

            return view('panel.wizards.routeEnd',$this->inputs,[
                'title' => 'Помощника е завършен',
                'line' => $line->id,
                'vehicle' => $line->vehicle_id,
                'routes' => $routes
            ]);
        }
        else{
            return redirect('/panel/routes/create/' . $line);
        }
    }

    public function getEdit(Request $request,$id)
    {
        
        $route = Route::find($id);
        if($route){

            $this->inputs['page'] = 'vehicle.' . $route->line->vehicle_id;

            return view('panel.forms.route',$this->inputs,[
                'title' => 'Редакция на маршрут №' . $route->id,
                'menuVehicle' => $route->vehicle_id,
                'nodes' => Node::whereIn('id',$route->getNodes())->get()->keyBy('id')->toJson(),
                'location' => Setting::get('last_admin_map_location'),
                'zoom' => Setting::get('last_admin_map_zoom'),
                'data' => $route,
                'routeNodes' => json_encode($route->getNodes()),
                'buttons' => [
                    [
                        'text' => 'Върни се назад',
                        'link' => url('panel/lines/show/' . $route->line->vehicle_id)
                    ]
                ]
            ]);
        }

        return redirect()->back()->with('message',[
            'type' => 'danger',
            'body' => 'Маршрутът не бе намерен'
        ]);
    
    }

    public function postEdit(Request $request, $id)
    {  


        $route = Route::find($id);
        if($route){
            
            $action = Route::createOrUpdate([
                'route' => $request->input('route'),
            ],$route->id);

            if($action){
                return redirect('panel/lines/show/' . $route->line->vehicle_id)
                    ->with('message',[
                        'type' => 'success',
                        'body' => 'Маршрутът е успешно редактиран'
                    ]);
            }
        }
        else {
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Маршрутът не бе намерен'
            ]);
        }
    }

    public function getDelete($id)
    {   
        $route = Route::find($id);
        if($route){
            
            $vehicleId = $route->line->vehicle_id;
            $route->delete();
            
            return redirect()->back()
                ->with('message',[
                    'type' => 'success',
                    'body' => 'Маршрутът е изтрит успешно'
                ]);
        }
        else {
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Маршрутът не бе намерен'
            ]);
        }
    }

    public function getRestore($id){
        $data = Route::withTrashed()->find($id);
        $action = $data->restore();
        if($action){
            return redirect()->back()
                ->with('message',[
                    'type' => 'success',
                    'body' => 'Маршрутът е възстановен успешно'
            ]);
        }
        else{
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Възникна грешка при възстановяването на маршрута'
            ]);
        }
    }

    public function getDestroy($id){
        $data = Route::withTrashed()->find($id);
        if($data){
            
            $data->destroyWithChildElements();

            return redirect()->back()
                ->with('message',[
                    'type' => 'success',
                    'body' => 'Маршрутът е изтрит успешно'
            ]);
        }
        else {
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Възникна грешка при изтриването на маршрута'
            ]);
        }
    }
}
