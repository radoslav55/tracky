<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\StoreVehicle;
use App\Models\Vehicle;

class VehiclesController extends BaseController
{

    public $page = 'vehicles';

    public function getIndex()
    {
        return view('panel.vehicles',$this->inputs,[
            'title' => 'Превозни средства',
            'vehicles' => Vehicle::all(),
            'iconPrefix' => Vehicle::iconPrefix(),
            'buttons' => [
                [
                    'text' => 'Добави ново транспортно средство',
                    'link' => url('/panel/vehicles/create'),
                    'action' => 'add-new'
                ]
            ]
        ]);
    }


    public function getCreate()
    {
        return view('panel.forms.vehicles',$this->inputs,[
            'title' => 'Добави ново транспортно средство',
            'vehicles' => Vehicle::all(),
            'buttons' => [
                [
                    'action' => 'go-back'
                ]
            ]
        ]); 
    }

    public function postCreate(StoreVehicle $request)
    {

        $filename = Vehicle::handleIconUpload($request->file('icon'));

        $action = Vehicle::create([
            'name' => $request->input('name'),
            'icon' => $filename
        ]);

        if($action){
            return redirect('/panel/vehicles')->with('message',[
                'type' => 'success',
                'body' => 'Транспортното средство бе успешно добавено'
            ]);
        }
        else{
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Възникна грешка при добавянето на транспортното средство'
            ]);
        }
    }

    public function getEdit($id)
    {
        $data = Vehicle::find($id);
        
        if($data){
            $data->icon = Vehicle::iconPrefix() . $data->icon;
            return view('panel.forms.vehicles',$this->inputs,[
                'title' => $data->name . ' - Редакция',
                'data' => $data,
                'buttons' => [
                    [
                        'action' => 'go-back'
                    ]
                ]
            ]); 
        }
        else{
            return redirect('panel/vehicles')->with('message',[
                'type' => 'danger',
                'body' => 'Транспортното средство не е намерено'
            ]);
        }
    }

    public function postEdit(StoreVehicle $request, $id)
    {   

        $vehicle = Vehicle::find($id);

        $vehicle->name = $request->input('name');

        if($request->hasFile('icon')){

            $vehicle->icon = Vehicle::handleIconUpload($request->file('icon'));
        }

        $action = $vehicle->save();

        if($action){
            return redirect('/panel/vehicles')->with('message',[
                'type' => 'success',
                'body' => 'Транспортното средство бе успешно редактирано'
            ]);
        }
        else{
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Възникна грешка при редакцията на транспортното средство'
            ]);
        }
    }

    public function getDelete($id)
    {

        $vehicle = Vehicle::find($id);
        if($vehicle){
            
            if($vehicle->routes()->exists()){
                return redirect()->back()->with('message',[
                    'type' => 'danger',
                    'body' => 'Има добавени маршрути към това транспортно средсто. Не може да бъде изтрио'
                ]);
            }
            $vehicle->delete();

            return redirect('/panel/vehicles')->with('message',[
                'type' => 'success',
                'body' => 'Транспортното средство бе успешно изтрито'
            ]);
        }
        else{
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Възникна грешка при изтриването на транспортното средство'
            ]);
        }

    }

    public function getRestore($id){
        $data = Vehicle::withTrashed()->find($id);
        $action = $data->restore();
        if($action){
            return redirect()->back()
                ->with('message',[
                    'type' => 'success',
                    'body' => 'Транспортното средство е възстановено успешно'
            ]);
        }
        else{
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Възникна грешка при възстановяването на транспортното средство'
            ]);
        }
    }

    public function getDestroy($id){
        $data = Vehicle::withTrashed()->find($id);
        if($data){
            $data->forceDelete();
            return redirect()->back()
                ->with('message',[
                    'type' => 'success',
                    'body' => 'Транспортното средство е изтрито завинаги'
            ]);
        }
        else {
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Възникна грешка при изтриването на транспортното средство'
            ]);
        }
    }
}
