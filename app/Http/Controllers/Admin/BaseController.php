<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Log;
use App\Models\Vehicle;
use Session;

class BaseController extends Controller
{
    
    public $inputs = [];
    
    public function __construct(){
        
        if(Session::has('errors')) {
            Session::get('errors')->getBag('default')->setFormat('<span class="glyphicon glyphicon-remove form-control-feedback"></span><div class="text-danger" style="padding:5px">:message</div>');
        }

        $this->inputs = [
            'user' => User::data(),
            'vehicles' => Vehicle::all(),
            'page' => $this->page ?? 'home'
        ];

        Log::adminPage(Request::url());

        //$this->url = '/panel/' . $this->index . (Request::segment(3) ? '/' . Request::segment(3) : null);
    }
    
    public function getMessage($action,$type,$custom = null){

        /*
         * Info variable structure : 
         * status => used in bootstrap alert box (danger/success)
         * message => name index in panel language file 
         * parameters => [optional] parameters for message in panel language file
         */

        if($action){
            return redirect($this->url)->with('message',[
                'type' => 'success',
                'body' => $this->text(
                    $custom ?: $type . '_success'
                )
            ]); 
        }
        else{
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => $this->text(
                    $custom ?: $type . '_error'
                )
            ]);
        }

        /*if($info['status'] == 'success'){
            return redirect('/panel/' . $this->index)->with('message',[
                'type' => 'success',
                'body' => $this->text(
                    'success_' . (isset($info['message']) ? $info['message'] . '_' : null) . $type,
                    isset($info['parameters']) ? $info['parameters'] : []
                )
            ]);
        }
        else{
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => $this->text(
                    'error_' . (isset($info['message']) ? $info['message'] . '_' : null) . $type,
                    isset($info['parameters']) ? $info['parameters'] : []
                )
            ]);
        }*/
    }
}
