<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Backup;
use App\Models\CrossURL;
use App\Models\System;

use Rah\Danpu\Dump;
use Rah\Danpu\Export;
use Rah\Danpu\Import;

use Illuminate\Support\Facades\Artisan;
class DatabaseController extends Controller
{
    
    public function backup(){

        $backup = Backup::create();

        return redirect()->back()->with('message',$backup ? [
            'type' => 'success',
            'body' => 'Бекъпа е успешно генериран'
        ] : [
            'type' => 'danger',
            'body' => 'Възникна грешка при създаването на бекъпа'
        ]);
    }

    public function update(Request $request){

        $file = $request->file('sql');

        $imageName = $file->getClientOriginalName();

        $movedFile = $file->move(Backup::directory(),$imageName);
        if($request->exists('key') && $request->input('key') == CrossURL::key()){
            try {
                $dump = new Dump;
                $dump
                    ->file($movedFile)
                    ->dsn('mysql:dbname=' . System::dbConfig('database') . ';host=localhost')
                    ->user(System::dbConfig('username'))
                    ->pass(System::dbConfig('password'))
                    ->tmp('/tmp');

                new Import($dump);
                return 'success';
            } catch (\Exception $e) {
                return 'error';
            }
        }
        
        return 'error';
    }

    public function upload(){

        $backup = Backup::create();
        if($backup){

            $upload = CrossURL::request('tracky');

            if($upload){

                return [
                    'status' => 'success',
                    'body' => 'Базата данни е успешно синхронизирана'
                ];
            }

            return [
                'status' => 'danger',
                'body' => 'Възникна грешка при синхронизирането на базата данни'
            ];
        }

        return [
            'status' => 'danger',
            'body' => 'Възникна грешка при създаването на нов бекъп'
        ];
        
        
    }

    public function migrate(){

        //Artisan::call('migrate:reset');
        Artisan::call('migrate');
        //Artisan::call('db:seed');
        
        return redirect('/panel')->with('message',[
            'type' => 'success',
            'body' => 'Базата данни е успешно рестартирана'
        ]);
    }
}
