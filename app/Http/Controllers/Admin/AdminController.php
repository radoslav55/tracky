<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\User;

class AdminController extends BaseController
{
    
    public function getLogin(Request $request)
    {

        return view('panel.login');
    }
    
    public function postLogin(Request $request)
    {

        $user = User::findRootByUsername($request->input('username'));

        if($user){

            $isBlocked = $user->isBlocked();

            if($isBlocked['status'] == 'success'){

                if($user->passwordMatches($request->input('password'))){

                    $user->login();
                    return redirect('/panel');
                }

            }
            else{
                
                return redirect()->back()->with('message',[
                    'type' => $isBlocked['status'],
                    'body' => $isBlocked['body']
                ]);
            }

            $user->newAttempt();
        }

        return redirect()->back()->with('message',[
            'type' => 'danger',
            'body' => 'Грешно потребителско име / парола'
        ]);
        
    }

    public function logout(){
    	$action = User::logout();
    	return redirect('/');
    }

}
