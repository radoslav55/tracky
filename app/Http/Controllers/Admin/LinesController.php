<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Vehicle;
use App\Models\Line;
use App\Models\System;
use App\Http\Requests\AddEditLine;

class LinesController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getShow($vehicleId)
    {

        $vehicle = Vehicle::find($vehicleId);

        if($vehicle){

            $this->inputs['page'] = 'vehicle.' . $vehicleId;

            return view('panel.vehicle',$this->inputs,[
                'title' => $vehicle->name . ' - Линии',
                'lines' => $vehicle->lines()->paginate(5),
                'buttons' => [
                    [
                        'text' => 'Добави нова линия',
                        'link' => url('panel/lines/create/' . $vehicle->id),
                        'action' => 'add-new'
                    ]
                ]
                
            ]); 
        }
        else {
            return redirect('panel/vehicles')->with('message',[
                'type' => 'danger',
                'body' => 'Превозното средство не бе намерено'
            ]);        
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate($vehicle = 0)
    {

        if(Vehicle::find($vehicle)){

            $this->inputs['page'] = 'vehicle.' . $vehicle;

            return view('panel.forms.line',$this->inputs,[
                'title' => 'Добави нова линия',
                'vehiclesArray' => Vehicle::lists('name','id'),
                'vehicle' => $vehicle,
                'buttons' => [
                    [
                        'action' => 'go-back'
                    ]
                ]
            ]);
        }

        return redirect('panel/vehicles')->with('message',[
            'type' => 'danger',
            'body' => 'Превозното средство не бе намерено'
        ]);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postCreate(AddEditLine $request)
    {
        
        $action = Line::create([
            'line' => $request->input('line'),
            'vehicle_id' => $request->input('vehicle')
        ]);

        if($action){
            return redirect('/panel/routes/wizard/' . $action->id);
        }
        else {
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Възникна грешка при добавянето на маршрута'
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        $line = Line::find($id);
        if($line){

            $this->inputs['page'] = 'vehicle.' . $line->vehicle_id;

            return view('panel.forms.line',$this->inputs,[
                'title' => 'Линия №' . $line->id . ' - редакция',
                'vehiclesArray' => Vehicle::lists('name','id'),
                'data' => $line,
                'buttons' => [
                    [
                        'action' => 'go-back'
                    ]
                ]
            ]);
        }

        return redirect()->back()->with('message',[
            'type' => 'danger',
            'body' => 'Линията не бе намерена'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postEdit(AddEditLine $request, $lineId)
    {

        $line = Line::find($lineId);

        if($line){
            
            $action = Line::where('id',$line->id)->update([
                'line' => $request->input('line'),
                'vehicle_id' => $request->input('vehicle')
            ]);

            if($action){

                return redirect('panel/lines/show/' . $line->vehicle_id)
                ->with('message',[
                    'type' => 'success',
                    'body' => 'Линията е успешно редактирана'
                ]);
            }
        }
        else {
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Линята не бе намерена'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDelete($id)
    {

        $line = Line::find($id);

        if($line){

            if($line->canBeDeleted()){

                $line->delete();

                return redirect()->back()->with('message',[
                    'type' => 'success',
                    'body' => 'Линята е успешно изтрита'
                ]); 
            }

            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Линята не може да бъде изтрира, понеже има маршрути в нея.'
            ]); 
            
        }

        return redirect()->back()->with('message',[
            'type' => 'danger',
            'body' => 'Линята не бе намерена'
        ]);  


    }

    public function getDestroy($id){

        $line = Line::withTrashed()->find($id);
        if($line){

            $line->destroyWithChildElements();

            return redirect()->back()
                ->with('message',[
                    'type' => 'success',
                    'body' => 'Линията е изтрита успешно'
            ]);
        }
        else {
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Възникна грешка при изтриването на линията'
            ]);
        }
    }

    public function getRestore($id){

        $line = Line::withTrashed()->find($id);
        $action = $line->restore();
        if($action){
            
            return redirect()->back()
                ->with('message',[
                    'type' => 'success',
                    'body' => 'Маршрутът е възстановен успешно'
            ]);
        }
        else{
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Възникна грешка при възстановяването на маршрута'
            ]);
        }
    }
}

