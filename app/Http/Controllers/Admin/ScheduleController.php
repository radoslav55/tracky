<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Route;
use App\Models\Vehicle;
use App\Models\Node;
use App\Models\Schedule;
use App\Models\Hour;

class ScheduleController extends BaseController
{


    public function show($id){

        $route = Route::find($id);
        if($route){

            $this->inputs['page'] = 'vehicle.' . $route->line->vehicle_id;

            $nodes = $route->nodesAsTail();

            return view('panel.schedule',$this->inputs,[
                'title' => $route->getFirstNode()->name . ' - ' . $route->getLastNode()->name,
                'menuVehicle' => $route->vehicle_id,
                'nodes' => $nodes,
                'routeId' => $id,
                'categories' => Schedule::all()->keyBy('id'),
                'buttons' => [
                    [
                        'action' => 'go-back'
                    ]
                ]
            ]); 
        }
        else{
            return redirect()->back()->with('message',[
                'type' => 'danger',
                'body' => 'Маршрутът не бе намерен'
            ]);
        }
    }

    public function add(Request $request){

        $route = Route::find($request->input('routeId'));
        if(in_array($request->input('nodeId'),$route->getNodes())){
            $node = $request->input('nodeId');
        }
        else {
            //Error
        }

        $input = $request->input('hours');
        $input = str_replace(' ','',$input);
        $lines = explode(PHP_EOL, $input);

        foreach($lines as $line){

            if(strlen($line) > 1){

                $line = explode('-',$line);

                if($line[1]){
                    $interval = explode('|',$line[1]);
                    if($interval[1]){
                        $from = strtotime($line[0]) - strtotime('today');
                        $to = strtotime($interval[0]) - strtotime('today');
                        if($interval[1] == 'rand'){
                            $interval = rand(600,3600);
                        }
                        else{
                            $interval = strtotime($interval[1]) - strtotime('today');
                        }

                        while($from <= $to){
                            if(!Hour::existsByHour($request->input('routeId'),$request->input('nodeId'), $request->input('scheduleId'),gmdate('H:i',$from))){
                                Hour::create([
                                    'schedule_id' => $request->input('scheduleId'),
                                    'route_id' => $request->input('routeId'),
                                    'node_id' => $request->input('nodeId'),
                                    'hour' => gmdate('H:i',$from),
                                    'status' => 'ok'
                                ]);
                            }
                            
                            $from += $interval;                            
                        }
                    }
                }
                else{
                    if(!Hour::existsByHour($request->input('routeId'),$request->input('nodeId'), $request->input('scheduleId'),$line[0])){
                        Hour::create([
                            'schedule_id' => $request->input('scheduleId'),
                            'route_id' => $request->input('routeId'),
                            'node_id' => $request->input('nodeId'),
                            'hour' => $line[0],
                            'status' => 'ok'
                        ]);
                    }
                }

            }
        }

        return 'ok';

    }

    public function getHours(Request $request){

        $output = [];

        $hours = Hour::where('node_id',$request->input('nodeId'))
            ->where('route_id',$request->input('routeId'))
            ->where('schedule_id',$request->input('scheduleId'))
            ->get();

        foreach($hours as $hour){

            $key = gmdate('G',strtotime($hour->hour) - strtotime('today'));

            $output[$key][$hour->id] = $hour->hour;
        }

        return json_encode($output);
    }

    public function delete(Request $request){
        Hour::whereIn('id',$request->input('items'))->delete();
        return 'ok';
    }

}
