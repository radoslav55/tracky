<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ContactsMail;
use App\Http\Controllers\Controller;
use App\Models\Log;
use App\Models\Vehicle;
use App\Models\Line;
use App\Models\Route;
use App\Models\Node;
use App\Models\Hour;
use App\Models\Edge;
use App\Models\Setting;
use App\Models\System;
use Cookie;
use Mail;

class HomeController extends Controller
{

    private $_microtime;

    public function __construct(){
        //die('<meta charset="UTF-8"></meta><h1 align="center">Затворен за поддръжка</h1><h3 align="center">Ще бъде отворен след 14:20 на 4.19.2016</h3>');
        parent::__construct();
        $this->_microtime =  microtime(true);
    }
    
    public function index(Request $request)
    {
        
    	Log::add();
        return view('index',[
            'vehicles' => Vehicle::all(),
            'iconPrefix' => Vehicle::iconPrefix(),
            'animationSettingSpeed' => Setting::get('animation_speed')
        ]);
    }

    public function aboutUs(){
    	return view('about_us');
    }

    public function download(){
    	return view('download');
    }

    public function postMail(ContactsMail $request){



        Mail::send('emails.contacts', ['content' => $request->input('message')], function ($m) use ($request) {
            
            $m->from($request->input('email'));

            $m->to('support@tracky-app.com')->subject('Контактна форма');
        });

        return [
            'status' => 'success'
        ];
    }

    public function warning(Request $request){
        if($request->cookie('warning') == null){

            Cookie::queue('warning','read',9999999);

        }
    }

    //http://maps.googleapis.com/maps/api/distancematrix/json?origins=51.519894,-0.105667&destinations=51.129079,1.306925&mode=bicycling&language=en-EN&sensor=false
}

