<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Route;
use App\Models\System;
use App\Models\Node;
use App\Models\Graph;
use App\Models\Town;
use App\Models\Edge;

class RouteController extends Controller
{
    
    /*
     *
     * Route calculation algorithm 
     * Creted and developed by Radoslav Stefanov
     * © Copyright 2015/2016 - All rights reserved!
     *
    */

    public function calculate(Request $request){

        $time = microtime(true);

        /*
         * Validating the input
         * It must have two arrays - from and to
         * Each array must contain lat and lng
         */
        if(
            $request->has('from') && 
            $request->has('to') &&
            array_key_exists('lat',$request->input('from')) && 
            array_key_exists('lng',$request->input('from')) &&
            array_key_exists('lat',$request->input('to')) && 
            array_key_exists('lng',$request->input('to'))
        ){

            $fromTown = System::getTown($request->input('from'));
            $toTown = System::getTown($request->input('to'));

            $from = new Node();
            $from->id = 'from';
            $from->lat = $request->input('from')['lat'];
            $from->lng = $request->input('from')['lng'];

            $to = new Node();
            $to->id = 'to';
            $to->lat = $request->input('to')['lat'];
            $to->lng = $request->input('to')['lng'];
            
            $nodes = Node::where('town_id',Town::where('name',$fromTown)->first()->id)
            ->orWhere('town_id',Town::where('name',$toName)->first()->id);
            
            // Checks if there are nodes in the area of the beggining and the ending points
            if($nodes->exists()){

                $nodes = $nodes->get();
                $nodes->push($from);
                $nodes->push($to);

                // Get all nodes that will be part of the graph
                $nodesIds = array_keys($nodes->toArray());

                // Get all edges linked to the nodes in the graph
                $edges = Edge::whereIn('from',$nodesIds)->orWhereIn('to',$nodesIds)->get();

                foreach($from->closest() as $node){
                    $edge = new Edge();
                    $edge->route_id = 0;
                    $edge->from = 'from';
                    $edge->to = $node->id;
                    $edge->distance = $node->distance * 1000;
                    $edges->push($edge);
                }

                foreach($to->closest() as $node){
                    $edge = new Edge();
                    $edge->route_id = 0;
                    $edge->from = $node->id;
                    $edge->to = 'to';
                    $edge->distance = $node->distance * 1000;
                    $edges->push($edge);
                }

                $edge = new Edge();
                $edge->from = 'from';
                $edge->to = 'to';
                $edge->distance = System::getAbsoluteDistanceViaRoads(
                    $request->input('from'),
                    $request->input('to')
                );
                $edges->push($edge);

                $graph = new Graph();

                $graph->setNodes($nodes);
                $graph->setEdges($edges);

                $graph->findPath('from','to');
                return $graph->printJson();

            }
            
            return [
                'status' => 'error',
                'errorCode' => 3
            ];

            

            //return Route::getRoute($request->input('from'),$request->input('to'),$request->has('date') ? $request->input('date') : false);
            //return '{"status":"success","data":[{"type":"walk","timestamp":251.55469217868},{"type":"5","timestamp":840},{"type":"walk","timestamp":636.012418085},{"type":"end"}],"length":"00:28","path":[{"type":"from"},{"type":"node","lat":"42.89777","lng":"23.91107"},{"type":"node","lat":"42.89314","lng":"23.91337"},{"type":"to"}],"time":1457265664.3211}';
        }
        else {
            return json_encode([
                'status' => 'error'
            ]);
        }

        
    }

    public function debug(){
        /*$output = '{"status":"success","initi":"19:31:45","totalTime":714.08973658403,"walkingTime":291.08973658403,"travelingTime":420,"waitingTime":3,"data":[{"type":"walk","timestamp":125.16012091199},{"type":2,"routeNumber":"1","waitingTime":true,"travelingTime":120,"timestamp":121},{"type":2,"routeNumber":"1","waitingTime":true,"travelingTime":120,"timestamp":121},{"type":3,"routeNumber":"78","waitingTime":true,"travelingTime":60,"timestamp":61},{"type":2,"routeNumber":"4","waitingTime":0,"travelingTime":120,"timestamp":120},{"type":"walk","timestamp":165.92961567204},{"type":"end"}],"path":[{"type":"from"},{"type":"node","name":"<span class=\"stopName\">\u0423\u041b. \u0412\u0410\u0421\u0418\u041b \u041a\u042a\u041d\u0427\u0415\u0412<\/span><div style=\"margin-top:5px\">\u0422\u0440\u043e\u043b\u0435\u0439\u0431\u0443\u0441 \u21161<\/div>19:25<br><span style=\"color:red\">19:38<\/span><br>19:53<br>20:13<br>","lat":"42.70573","lng":"23.35599"},{"type":"node","name":"<span class=\"stopName\">\u0425\u0418\u041f\u0415\u0420\u041c\u0410\u0420\u041a\u0415\u0422 \u0411\u0418\u041b\u0410<\/span><div style=\"margin-top:5px\">\u0422\u0440\u043e\u043b\u0435\u0439\u0431\u0443\u0441 \u21161<\/div>19:27<br><span style=\"color:red\">19:40<\/span><br>19:55<br>20:15<br>","lat":"42.70379","lng":"23.35103"},{"type":"node","name":"<span class=\"stopName\">\u041c\u041e\u0421\u0422\u0410 \u0427\u0410\u0412\u0414\u0410\u0420<\/span><div style=\"margin-top:5px\">\u0410\u0432\u0442\u043e\u0431\u0443\u0441 \u211678<\/div>19:28<br><span style=\"color:red\">19:45<\/span><br>20:04<br>20:24<br>","lat":"42.70125","lng":"23.34413"},{"type":"node","name":"<span class=\"stopName\">\u0423\u041b. \u0411\u042f\u041b\u041e \u041c\u041e\u0420\u0415<\/span><div style=\"margin-top:5px\">\u0422\u0440\u043e\u043b\u0435\u0439\u0431\u0443\u0441 \u21164<\/div>19:24<br><span style=\"color:red\">19:38<\/span><br>19:53<br>20:10<br>","lat":"42.70015","lng":"23.33877"},{"type":"node","name":"<span class=\"stopName\">\u0423\u041b. \u041a\u0420\u0410\u041a\u0420\u0410<\/span><div style=\"margin-top:5px\">\u0422\u0440\u043e\u043b\u0435\u0439\u0431\u0443\u0441 \u21164<\/div>19:26<br><span style=\"color:red\">19:40<\/span><br>19:55<br>20:12<br>","lat":"42.69829","lng":"23.33697"},{"type":"to"}]}';
        $array = (array) json_decode($output);
        echo '<meta charset="UTF-8">';
        foreach($array['data'] as $key=>$moment){

            $node = (array) $array['path'][$key];
            if($node['type'] == 'node'){
                echo 'Node ' . $node['name'] . '<br>';
            }
            else{
                echo $node['type'] . '<br>';
            }

            //die(var_dump());
            //die(var_dump($moment));
        }*/

        $nodes = Node::where('id','>',246)->get();
        foreach($nodes as $node){
            Node::insertRoutes($node->id);
        }
    }
    
}
