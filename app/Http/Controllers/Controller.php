<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\System;
use App\Models\Log;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public $microtime = null;

    public function __construct(){
    	$this->microtime = microtime(true);
    }

    public function __destruct(){
    	//echo System::time($this->microtime)->string();
    }
    public function text($string,$parameters = []){
        return trans('panel.' . $this->index . '_' . $string,$parameters);
    }
}
