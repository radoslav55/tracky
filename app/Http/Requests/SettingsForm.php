<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SettingsForm extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'site_name' => 'required',
            'posts_by_page' => 'required|integer',
            'max_nodes_distance' => 'required|integer',
            'max_edges_count' => 'required|integer'
        ];
    }
}
