<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreVehicle extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $iconRules = '';
        $id = 0;
        if(Request::segment('4') != null){
            $id = Request::segment('4');
        }
        else{
            $iconRules = 'required|';
        }

        return [
            'name' => 'required|min:3|max:255|unique:vehicles,name,' . $id,
            'icon' => $iconRules . 'image|mimes:jpeg,ico,png'
        ];
    }
}
