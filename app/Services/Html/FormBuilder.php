<?php namespace App\Services\Html;

use Input;
use Session;
use \Illuminate\Support\MessageBag;
use Request;

class FormBuilder extends \Illuminate\Html\FormBuilder {

    public $errors;
    
    public function __construct($html,$url,$store){
        parent::__construct($html,$url,$store);
        
        $this->errors = Session::get('errors',new MessageBag());
    }

    public function inputGroup($name,$value,$options){

        return 
            '<div class="form-group ' . ($this->errors->has($name) ? 'has-error has-feedback' : null) . '">' . 
            
                '<div class="input-group" style="margin-left:10px;">' . 
                    '<span class="input-group-addon " id="' . $name . '-addon">' . 
                        (isset($options['text']) ? $options['text'] : '<span class="glyphicon glyphicon-' . $options['glyphicon'] . '"></span>') . 
                    '</span>' . 
                    parent::text($name,$value,[
                        'class' => 'form-control',
                        'placeholder' => $options['placeholder'],
                        'aria-describedby' => '#' . $name . '-addon'
                    ]) . 
                    
                '</div>' . 
                $this->errors->first($name) . 
            '</div>';
    }
    
    public function group($type,$name,$value,$options = []){
        return 
            '<div class="form-group ' . ($this->errors->has($name) ? 'has-error has-feedback' : null) . '">' . 
                parent::label($name,$options['text'],[
                    'class' => 'control-label col-md-' . (isset($options['labelWidth']) ? $options['labelWidth'] : 2)
                ]) . 
                '<div class="col-md-9">' . 
                    (($type == 'toggle' || $type == 'imageUpload') ? $this->$type($name,$value) : 
                    (($type == 'select') ? 
                        parent::select($name,$options['options'],$value,[
                            'class' => 'form-control',
                        ]) : 

                        parent::$type($name,$value,[
                            'class' => 'form-control',
                            'placeholder' => $options['placeholder'],
                            'style' => isset($options['style']) ? $options['style'] : 'null'
                        ])
                    ))
                    . 
                    $this->errors->first($name) . 
                '</div>

            </div>';
    }
    
    public function controlButtons($id,$first = 'edit',$second = 'delete'){
        $htmls = [
            'edit' =>   '<a href="' .  url('panel/' . Request::segment(2) . '/' . $id . '/edit') . '" title="Редактиране" class="btn btn-success btn-xs">
                            <span class="glyphicon glyphicon-pencil"></span> Редактирай
                        </a>',
            'delete' => '<a href="' .  url('panel/' . Request::segment(2) . '/' . $id . '/delete') . '" class="btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-remove"></span> Изтрий
                        </a>',
            'view' =>   '<a href="' .  url('panel/' . Request::segment(2) . '/' . $id) . '" title="Повече опции" class="btn btn-primary btn-xs">
                            <span class="glyphicon glyphicon-arrow-right"></span> Повече опции
                        </a>',
            'block' =>  '<a href="' .  url('panel/' . Request::segment(2) . $id . '/block') . '" class="btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-ban-circle"></span> Блокирай
                        </a>'
        ];
        
        return '
            <td width="1">' 
                . $htmls[$first] .
            '</td>
            <td width="1">'
                . $htmls[$second] . 
            '</td>';
    }
    
    public function textfield($name, $label, $errors, $labelOptions = array(), $inputOptions = array())
    {
        $labelOptions['class'] = 'form-label';
        $inputOptions['class'] = 'form-control';
        $inputOptions['placeholder'] = $label;

        return sprintf(
            '<div class="form-group">%s<div%s>%s%s</div></div><!-- end form-group -->',
            parent::label($name, $label, $labelOptions),
            $errors->has($name) ? ' class="error-control"' : '',
            parent::text($name, null, $inputOptions),
            $errors->has($name) ? '<span class="error"><label class="error" for="' . $name . '">' . $errors->first($name) . '</label></span>' : ''
        );
    }

    public function submit($value = null, $options = [])
    {
        $options['class'] = 'btn btn-cons btn-success' . (isset($options['class']) ? ' ' . $options['class'] : '');

        return parent::submit($value, $options);
    }
    
    
    public function toggle($name,$value = 0){
        if(Input::old($name) != ''){
            $value = Input::old($name);
        }
        
        $status = $value ? null : 'off';
        
        return '<div class="toggler ' . $status . '">
            <div class="round ' . $status . '"></div>
            <input type="checkbox" name="' . $name . '" value="' . $value . '" checked>
            
        </div>';
    }
    
    public function imageUpload($name,$src){
        return '<div class="image-upload">
            <span class="btn btn-primary btn-file">
                Избери файл… ' . parent::file($name) . '
            </span>
            <div class="image_preview_div">
                <img class="image-preview" height="100" src="' . $src . '">
            </div>
        </div>';
    }

}