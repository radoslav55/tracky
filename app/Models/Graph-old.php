<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Graph
{
    
    private $_edges = [];
    private $_nodes = [];
    private $_hours = [];

    private $_node = 0;

    private $_path = [];

    public $from = 0;
    public $to = 0;

    public $beginTime = 0;

    public function __construct(){

        $rate = Setting::get('ignore_walking_percent');

        if($rate < 0){
            $rate = 1 - $rate / -1000;
        }

        $this->walkingTimeModification = $rate;
        $this->beginTime = 1462810000; 

        $this->totalTime = 0;
    }

    public function setNodes($nodes){

        $this->_nodes = $nodes;
    }

    public function setEdges($edges){
        $this->_edges = $edges;
    }

    public function findPath($from, $to){

        $this->from = $from;
        $this->to = $to;

        $this->_initializeNodes();
        $this->_hours = Hour::withCache();

        while($this->_nodes->count() > 0){

            
            // 2 sec
            $shortest = $this->shortestNode();
            // end
            

            if($this->_node->id == $this->to){
                break;
            }
            
            $time = microtime(true);

            // 25 sec
            foreach($shortest->childs() as $edge){

                $to = $this->_nodes->where('id',$edge->to)->first();

                // 13 sec
                $edge = $this->calculateTime($edge);
                // end

                if($edge['weight'] + $this->_node->weight < $to->weight){

                    $to->weight = $edge['weight'] + $this->_node->weight;
                    $to->time = $edge['time'] + $this->_node->time;
                    $to->route_id = $edge['route_id'];

                    $this->_path[$to->id] = [
                        'parent' => $shortest->id,
                        'data' => $edge
                    ];
                }
            }
            // End

            $time = microtime(true) - $time;
            $this->totalTime += $time;

            // Remove the current shortest
            $this->_nodes = $this->_nodes->keyBy('id')->forget($this->_node->id);

        }
        die(var_dump($this->totalTime));
    }

    private function _initializeNodes(){
        
        foreach($this->_nodes as $node){
            $node->time = INFINITY;
            $node->weight = INFINITY;
            $this->_path[$node->id] = 0;
        }

        $node = $this->_nodes->where('id', $this->from)->first();
        $node->time = 0;
        $node->weight = 0;
        $node->route_id = 0;

    }


    public function shortestNode(){

        $this->_node = $this->_nodes->sortBy('weight')->first();
        return $this;
    }   

    public function childs(){

        // TODO - Smarter system when the edge is by walking
        //return array_keys($this->_edges->where('from',$this->_node->id)->keyBy('to')->toArray());
        return $this->_edges->where('from',$this->_node->id);
    }

    public function calculateTime($edge){

        $from = $this->_nodes->where('id',$edge->from)->first();
        $to = $this->_nodes->where('id',$edge->to)->first();

        
        if($edge->route_id == 0){

            $humanSpeed = 2.1;

            return [
                'type' => 'walk',
                'time' => $edge->distance / $humanSpeed,
                'weight' => ($edge->distance / $humanSpeed) * $this->walkingTimeModification,
                'route_id' => 0
            ];
        }
        else{

            $timeAtThisMoment = $from->time + $this->beginTime - strtotime('today');
            $leaveHour = $this->_hours->findNext($timeAtThisMoment, $edge->route_id, $from->id);
            
            if($leaveHour){
                $arriveHour = $this->_hours->findNext(strtotime($leaveHour) - strtotime('today'), $edge->route_id, $to->id);
                if($arriveHour){
                    //die(var_dump($edge->route->line->id));
                    $addOnWeight = 0;

                    if($from->route_id != null && $from->route_id != $edge->route_id){
                        $addOnWeight = 120;
                    }

                    return [
                        'type' => $edge->route->line->vehicle->id,
                        'line' => $edge->route->line->line,
                        'waitingTime' => $leaveHour - $timeAtThisMoment,
                        'travelingTime' => $arriveHour - $leaveHour,
                        'time' => $arriveHour - $timeAtThisMoment,
                        'weight' => $arriveHour - $timeAtThisMoment + $addOnWeight,
                        'route_id' => $edge->route_id
                    ];
                }
            }

            return [
                'weight' => INFINITY
            ];
            
        }
    }

}
