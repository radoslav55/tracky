<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;
use Hash;
use Request;
use Cookie;

class User extends Model
{

    protected $table = 'users';
    protected $guarded = [];
    public $timestamps = true;

    /**
     * findUserByUsername method
     * Checks if there is a ROOT user with the given username
     * If the user exists, the method will return the finded user as a class
     * 
     * @param  string  $username - The user's username
     * 
     * @return User 
     */
    public static function findRootByUsername($username){
        
        $user =  User::where('username',$username)->where('type','root');
        return $user->exists() ? User::find($user->first()->id) : false;
        /*if($user->exists() && Hash::check($password,$user->first()->password)){

            return User::find($user->first()->id);
        }*/

    }

    /**
     * passwordMatches method
     * Checks if the given password matches with the current user's password
     * 
     * @param  string $password - The input password
     * @return boolean - Defines if the password matches or no
     */
    public function passwordMatches($password){

        return Hash::check($password,$this->password);
    }

    /**
     * isBlocked method
     * Checks if the current user is blocked or not
     * 
     * @return string[]
     *  - status = danger / success
     *  - body = Message to show to the user
     */
    public function isBlocked(){

        if($this->blockedTo > time()){
            
            return [
                'status' => 'danger',
                'body' => User::buildWaitingMessage($this->blockedTo)
            ];
        }

        return[
            'status' => 'success'
        ];
    }

    /**
     * login method
     * Saves the session for the current user
     * This session gives access to the admin panel
     */
    public function login(){
        Session::put('admin',$this->id);
        Log::loginEvent();
    }
    
    /**
     * logout method
     * Removes the admin session
     */
    public static function logout(){
        Session::forget('admin');
    }
    
    /**
     * data method
     * @return User - The current user's data from the session
     */
    public static function data(){
        if(Session::has('admin')){
            return User::where('id',Session::get('admin'))->first();
        }
        return [];
    }

    /**
     * handleTimer method
     * Restarts the attempts if the last reset was before more than 24 hours
     * Must be called in each attempt to log in
     */
    private function handleTimer(){

        $time = strtotime($this->next_reset);

        if($time < time()){

            $this->next_reset = date('Y-m-d H:i:s',time() + 86400);
            $this->attempts = 0;
            $this->save();
        }
    }

    /**
     * newAttempt method
     * If the attempts are less than 10, it increments the attempts
     * If they are more than 10, it blocks the user for 300 secons [5 minutes] and removes the attempts
     */
    public function newAttempt(){

        if($this->attempts < 10){

            $this->increment('attempts',1);
        }
        else{

            $this->attempts = 0;
            $this->blockedTo = time() + 300;
            $this->save();
        }
    }

    /**
     * buildWaitingMessage method
     * Constructs the messae for the user when the user is blocked
     * 
     * @param  int $blockedTo - Defines when will the user be unblocked
     * @return string - The message
     */
    public static function buildWaitingMessage($blockedTo){
        return 'Вашият профил ще бъде отблокиран след ' . (($blockedTo - time()) / 60 < 1 ?
                    'по малко от минута' : 
                    round(($blockedTo - time()) / 60,0) . ' минути ');
    }

    public static function getGuestId(){

        /*
         * Checks if the user has a cookie that defines his id
         * If he has not, the system creates one random
         */
        if(Request::cookie('main') == null){

            $cookie = str_random(60);
            Cookie::queue('main',$cookie,9999999);
        }
        else {  
            $cookie = Request::cookie('main');
        }
        
        /* Gets the cookie that is stored in the database with the users' IP
         * If his actual cookie doesn't match, the user's cookie has changed someway and it will be changed with the original
         */
        $originalCookie = User::where('ip',Request::getClientIp());
        if($originalCookie->exists()){

            $originalCookie = $originalCookie->first()->cookie;

            if($originalCookie != null && $originalCookie != $cookie){

                $cookie = $originalCookie;
                Cookie::queue('main',$cookie,9999999);
            }
        }
        

        /* Gets the unique userId that differs the distinct machines
         * If either the ip or the cookie match a log in the database, the userId will be that record
         * If there isn't anything found in the database, the system will generate a new userId
         */
        $userId = User::where('ip',Request::getClientIp())->orWhere('cookie',$cookie);

        if($userId->exists()){

            $userId = $userId->first()['id'];
        }
        else {

            $action = User::create([
                'username' => 'guest',
                'ip' =>  Request::getClientIp(),
                'cookie' => $cookie
            ]);

            $userId = $action->id;
        }

        return $userId;
    }
    
}
