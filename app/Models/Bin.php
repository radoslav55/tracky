<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bin extends Model
{

	public static $tables = [
		'Route' => [
            'label' => 'Маршрут',
            'question' => 'Всички свързани часове'
        ],
        'Node' => [
            'label' => 'Спирка',
            'name' => 'name',
            'question' => 'Всички свързани маршрути'
        ],
        'Vehicle' => [
            'label' => 'Транспортно средство',
            'question' => 'Всички свързани маршрути'
        ],
        'Line' => [
            'label' => 'Линия',
            'question' => 'Всички свързани маршрути'
        ]
	];


    public static function getRecicledItems(){
    	$array = [];

        foreach(self::$tables as $model=>$info){

            $class = 'App\Models\\' . $model;
            $elements = $class::onlyTrashed()->get();

            foreach($elements as $element){
                $array[] = [
                    'name' => ($info['name'] ? $element[$info['name']] : ($info['label'] . ' №' . $element->id)),
                    'url' => strtolower($model) . 's',
                    'id' => $element->id,
                    'question' => $info['question']
                ];
            }
        }

    	return $array;
    }
}
