<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Route extends Model
{

	use SoftDeletes;

    protected $guarded = [];

    private static $_instance = null;
    private static $_routes = [];
	private static $_stops = [];    
	private static $_get;
	private static $_buffer;


	public static function createOrUpdate($data,$id = null)
	{
        $data['route'] = json_decode($data['route']);
		$routeIndex = DB::transaction(function() use($data,$id) {

			$routeId = 0;

			if($id){
				$route = Route::find($id);
                $route->edges()->delete();
			}
			else {
				$route = Route::create([
					'line_id' => $data['line_id'],
				]);
			}

            //$stops = Node::whereIn('id', $data['route'])->get()->toArray();

            $stops = $data['route'];
			foreach($stops as $key=>$stop){

				if(array_key_exists($key + 1,$stops)){

					Node::find($stop)->connectWith(Node::find($stops[$key + 1]))->route($route->id);
					
				}
			}

			return $route->id;
		});


		return $routeIndex;
	}


	/**
     * getRoute method
     * Controls the route calculation
     * @param int[] from - The starting point of the route
     * @param int[] to - The edning point of the route
     *
     * @return string[]
     */
	public static function getRoute($from,$to, $date = 0){

		$edges = Edge::getExistingEdges()->toArray();

		Log::route($to);

        foreach($edges as $edge){
            if($edge['route_id'] == 0){
                $edges[] = [
                    'from' => $edge['to'],
                    'to' => $edge['from'],
                    'route_id' => 0,
                    'distance' => $edge['distance']
                ];
            }
        }
    
        $addonNodes = Node::closest($from);

        foreach($addonNodes as $node){
            $edges[] = [
                'route_id' => 0,
                'from' => 'from',
                'to' => $node->id,
                'distance' => $node->distance * 1000
            ];
        }

        $addonNodes = Node::closest($to);
        foreach($addonNodes as $node){
            $edges[] = [
                'route_id' => 0,
                'from' => $node->id,
                'to' => 'to',
                'distance' => $node->distance * 1000
            ];
        }

        $search = new Search();

        $search->setDate($date);
        $search->setEdges($edges);
        $search->execute();

        //$search->printDebug();
        $search->printJSON();

	}

	public function nodes(){

       	$points = $this->getNodes();
       	return Node::where(function($query) use($points){
       		foreach($points as $point){
       			$query->orWhere('id',$point)->get();
       		}
       	});

    }

    /**
     * nodesAsTail method
     * Returns the nodes of the current route as objects
     * 
     * @return Node[]
     */
    public function nodesAsTail(){
    	$points = $this->getNodes();

    	foreach($this->getNodes() as $node){
    		$output[] = Node::find($node);
    	}

    	return $output;
    }

    public function edges(){
        return $this->hasMany('App\Models\Edge','route_id');
    }

    public function hours(){

        $nodes = [];
        foreach($this->nodes()->get() as $node){
            $nodes[] = $node->id;
        }

        return Hour::where('route_id',$this->id)->whereIn('node_id', $nodes);
    	

    }

    public function line(){

        return $this->hasOne('App\Models\Line', 'id', 'line_id');
    }

    public function destroyWithChildElements(){

    	$this->hours()->delete();
        $this->edges()->delete();
        $this->forceDelete();

    }

    /**
     * getMirror method
     * Converts the given route by
     * Reversing the route
     * If there are nodes that have 'brothers' - Nodes with the same name, they will be replaced
     * 
     * @param  int $route - The id of the route
     * 
     * @return int[] - The id's of the nodes in the route by order
     */
    public static function getMirror($route){

        $mirrorNodes = [];

        $nodes = Route::find($route)->getNodes();
        
        foreach($nodes as $node){
            $node = Node::find($node);
            $mirrorNode = Node::where('name',$node->name)->where('id','!=',$node->id);

            if($mirrorNode->exists()){
                $mirrorNodes[] = $mirrorNode->first()->id;
            }
            else{
                $mirrorNodes[] = $node->id;
            }
        }

        return array_reverse($mirrorNodes);


    }

    /**
     * getFirstNode method
     * Gets the first node of the route
     * 
     * @return Node
     */
    public function getFirstNode(){

        $coordinates = Edge::where('route_id', $this->id)->divideNodes();
        $result = array_diff($coordinates['startingNodes'], $coordinates['endingNodes']); 
        return Node::find(array_values($result)[0]);
    }

    /**
     * getFirstNode method
     * Gets the last node of the route
     * 
     * @return Node
     */
    public function getLastNode(){

        $coordinates = Edge::where('route_id', $this->id)->divideNodes();
        $result = array_diff($coordinates['endingNodes'], $coordinates['startingNodes']); 

        return Node::find(array_values($result)[0]);
    }


    /**
     * getNodes method
     * returns the nodes' ids from the route in order
     * the output contains all the nodes' data
     *
     * @return int[] - The node's ids in order
     */
    public function getNodes(){

        $tim = microtime(true);

    	$nodes = [];

    	$edge = Edge::whereNotExists(function($query)
        {
            $query->select(DB::raw('*'))
                  ->from('edges as b')
                  ->whereRaw("
              			b.`to` <> 'null' AND
                        b.`to` = edges.`from` AND
                        b.`route_id` = edges.`route_id`
                 ");
        })->where('route_id',$this->id);

    	$nodes[] = $edge->first()->from;

    	while($edge->exists()){
    		$to = $edge->first()->to;
    		$edge = $this->edges()->where('from',$to);
    		if($edge->exists()){
    			$nodes[] = $edge->first()->from;
    		}
    		else {
    			$nodes[] = $to;
    		}
    	}

    	return $nodes;
    }

}