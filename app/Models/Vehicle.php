<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{

    use SoftDeletes;

    protected $table = 'vehicles';
    protected $guarded = [];

    public static $imagesDirectory = 'icons/';

    public function routes(){
        return $this->hasManyThrough('App\Models\Route', 'App\Models\Line', 'vehicle_id', 'line_id');
    }

    public function lines(){
        return $this->hasMany('App\Models\Line','vehicle_id');
    }

    public static function iconPrefix(){
        return '/' . self::$imagesDirectory;
    }

     /*
     * Handling the file
     * The new file will be saved to the local directory with a random name
     */
    public static function handleIconUpload($file){

        $imageName = str_random(10) . '.' . $file->getClientOriginalExtension();
        $file->move(self::$imagesDirectory,$imageName);

        return $imageName;

    }


}
