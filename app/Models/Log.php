<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cookie;
use Request;
use URL;

class Log extends Model
{

    protected $table = 'logs';
    protected $guarded = [];
    public $timestamps = false;

    public static $requestType;
    public static $coordinates;

    private static $intervals = [
    	'last_24' => [
    		'dateIndex' => 'H',
    		'count' => 24,
    		'timestamp' => 86400
    	],
    	'last_30' => [
    		'dateIndex' => 'd',
    		'count' => 30,
    		'timestamp' => 2592000
    	],
    	'all' => [
    		'dateIndex' => 'd',
    		'count' => 30,
    		'timestamp' => 0
    	]
    ];

    private static $homeTable = [
    	'home' => 'Брой презареждания',
        'google' => 'Брой заявки към google',
        'route' => 'Брой заявки за маршрут',
        'users' => 'Брой уникални потребители'
    ];

    public static function adminPage($page){
        Log::create([
            'user_id' => User::getGuestId(),
            'from' => $page,
            'type' => 'adminPageLoaded',
            'timestamp' => time()
        ]);
    }

    public static function loginEvent(){
        Log::create([
            'user_id' => User::getGuestId(),
            'from' => URL::previous(),
            'type' => 'adminLogIn',
            'timestamp' => time()
        ]);
    }
    
    /*
     * add method
     * records a new log in the database
     */
    public static function add($page = 'home',$location = null){

        //Checks if the request is from a bot
        if(!preg_match('/robot|spider|crawler|curl|^$/i', $_SERVER['HTTP_USER_AGENT'])){
            Log::create([
                'user_id' => User::getGuestId(),
                'from' => URL::previous(),
                'type' => $page,
                'location' => $location,
                'timestamp' => time()
            ]);
        }
        else{
            Log::create([
                'from' => URL::previous(),
                'type' => 'bot',
                'location' => $location,
                'timestamp' => time()
            ]);
        }

    }

    /*
	 * google method
	 * records a new google request event to the database
	 */
    public static function google(){

    	Log::create([
            'type' => 'google',
            'timestamp' => time()
        ]);
    }

    public static function setRequestType($requestType){
    	self::$requestType = $requestType;
    	return new self;
    }
    
    /*
     * dates method
     * Returns the intervals for the x-axis of the diagram 
	 * The output can be:
	 * - The last 24 hours, starting from now
	 * - The last 30 days, starting from today
     */
    public static function dates(){

        $array = [];

        $time = date(Log::intervals('dateIndex'));

        for($i = Log::intervals('count');$i >= 0;$i--){

            $array[] = $time;

            if($time >= Log::intervals('count') - 1){
                 
                $time = 0;
            }
            else{

                $time++;
            }            
        }

        return $array;
    }


   	/* 
   	 * coordinates method
   	 * returns the number of reloads and unique users for each time interval
   	 * The time interval can be 1 hour or 1 day, depending of the reqest type
   	 */    
    public static function coordinates(){
        
        $output = [];
        
        for($i = Log::intervals('count');$i >= 0;$i--){

        	$query = Log::whereBetween('timestamp',Log::_getTimeIntervals($i))
        		->where('type','home')->get();

            $output[] = [
                'reloads' => $query->count(),
                'users' => $query->groupBy('user_id')->count()
            ];
            
        }

        return $output;
    }
    
    /*
	 * max method
	 * returns the max number of reloads in an interval of time
	 * the method is used to determinate the interval if the y-axis
	*/
    public static function max(){
        
        return self::round_up(array_reduce(self::$coordinates, function ($a, $b) {
            return @$a['reloads'] > $b['reloads'] ? $a : $b ;
        })['reloads'],-1);
    }

    public static function getState($state){

    	$output = Log::where('timestamp','>',Log::_timestamp());

    	if($state == 'users'){

    		return $output->where('type','home')->get()->groupBy('user_id')->count();
    	}

    	return $output->where('type',$state)
    		->count();

    }
    
    public static function mInfo(){

    	$byPages = [];

        //Stats by pages
        
        $pages = Log::groupBy('location')->get();

        foreach($pages as $page){

            $query = Log::where('timestamp','>',Log::_timestamp())
            	->where('location',$page->location);

            $byPages[strtolower($page->location)] = [
                'reloads' => $query->get()->count(),
                'users' => $query->groupBy('user_id')->get()->count()
            ];
        }
       	

        return [
            'reloads' => Log::getState('home'),
            'users' => Log::getState('users'),
            'google' => Log::getState('google'),
            'byPages' => $byPages
        ];
    }

    /*
     * data method
     * returns the json for the diagram
     */
    public static function data($request){

    	Log::setRequestType($request);
    	self::$coordinates = Log::coordinates();

        echo json_encode([
            'dates' => Log::dates(),
            'coordinates' => self::$coordinates,
            'max' => Log::max(),
            'mInfo' => Log::mInfo()
        ]);
    }

    public static function getArrayForHome(){

    	$output = [];

    	foreach(self::$homeTable as $key=>$title){

    		$output[$key] = [
    			'title' => $title,
    			'last_24' => Log::setRequestType('last_24')->getState($key),
    			'all' => Log::setRequestType('all')->getState($key),
    		];
    	}

    	return $output;
    }
    
    /*
     * types method
     * returns a list of towns from the database
     */
    public static function towns(){

    	$output = [];
    	$pages = Log::where('type','route')->groupBy('location')->get(); 

    	foreach($pages as $page){
    		$output[] = [$page->location,strtolower($page->location)];
    	}

    	return $output;

    }

    public static function route($latLng){

        $town = System::getTown($latLng);

        Log::add('route',Town::handle($town));
    }
    
    public static function round_up($value, $places)
    {
        $mult = pow(10, abs($places));
        return $places < 0 ?
        ceil($value / $mult) * $mult :
        ceil($value * $mult) / $mult;
    }

    /*
	 * intervals method
	 * returns a setting value from the $intervals variable
    */
    public static function intervals($key){
    	return self::$intervals[self::$requestType][$key];
    }

    /*
	 * _getTimeIntervals method
	 * returns the interval of logs to search in
	 * the output is an array with two values - from and to
	 * the values are timestamps
	 */
    private static function _getTimeIntervals($i){

    	$timeIntervals = [

    		'last_24' => [
                (time() - $i * 3600) - date('i') * 60 - date('s'),
                ((time() - $i * 3600) + 3600) - date('i') * 60 - date('s')
            ],
            'last_30' => [

                (time() - $i * 86400) - date('H') * 3600 - date('i') * 60 - date('s'),
                ((time() - $i * 86400) + 86400) - date('H') * 3600 - date('i') * 60 - date('s')
            ]
    	];

    	return $timeIntervals[self::$requestType];
    }

    /*
     * _timestamp method
     * returns the timestamp to search in
     */
    private static function _timestamp(){
    	if(Log::$intervals[self::$requestType]['timestamp'] == 0){
    		return 0;
    	}
    	return time() - Log::$intervals[self::$requestType]['timestamp'];
    }
}