<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Line extends Model
{

    use SoftDeletes;

    protected $guarded = [];

    public function routes(){
        return $this->hasMany('App\Models\Route','line_id');
    }


    /**
     * wizardStatus method
     * Used to check if the current line has the needed routes
     * The needed routes are the main route and optionaly - the reverse route
     *
     * @param  int $line - The line id
     * @return  int 
     * 0 - The line has all the needed routes
     * 1 - The line doesn't have anything
     * 2 - The line has the main route
     */
    public function wizardStatus(){

        if($this->has_wizard){
            $routes = $this->routes();
            if($routes->count() >= 2){
                return 0;
            }
            return $routes->count() + 1;
        }

        return 0;
    }

    public function canBeDeleted(){

        $routes = Route::where('line_id',$this->id);
        return $routes->count() ? false : true;
    }

    public function destroyWithChildElements(){

        $routes = Route::where('line_id', $this->id)->withTrashed()->get();

        foreach($routes as $route){
            $route->destroyWithChildElements();
        }
        $this->forceDelete();
    }

    public function vehicle(){

        return $this->belongsTo('App\Models\Vehicle');
    }
}
