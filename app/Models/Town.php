<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
    
    protected $guarded = [];
    public $timestamps = false;

    /**
     * handle method
     * used to add a town if it doesn't exist
     * 
     * @param  string $town - the town name
     * @return integer - the id of the town
     */
    public static function handle($town){

        $townInDatabase = Town::where('name',$town);

        if($townInDatabase->exists()){

            return $townInDatabase->first()->id;
        }

        return Town::create([
            'name' => $town
        ])['id'];
    }  

    /**
     * getForNodes method
     * Gets the towns that are used by one or more nodes
     * @return Towns[]
     */
    public static function getForNodes(){

        $townsWithNodes = [];
        $towns = Town::all();

        foreach($towns as $town){
            if(Node::where('town_id',$town->id)->exists()){
                $townsWithNodes[$town->id] = $town;
            }
        }
        return $townsWithNodes;
    }
}
