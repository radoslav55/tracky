<?php

namespace App\Models;

class Search{

    public $edge;
    public $edges;
    public $direction = false; // 'right' or 'reverse'

    public $from = 0;
    public $to = 0;

    // Stores information about the actual node while doing the search
    public $temporalNodeData = [];

    public $times = [];
    public $parents = [];
    public $completed = [];

    public $walkingTimeModification = 0;

    // ONLY FOR TEST
    public $counter = 0;
    public $sun = 0;

    public function __construct(){

        $this->microtime = microtime(true);

        $rate = Setting::get('ignore_walking_percent');
        if($rate < 0){
            $rate = 1 - $rate / -1000;
        }

        $this->walkingTimeModification = $rate;
        

    }

    public function setDate($date){
        if($date){
            $this->initialHour = $date;
        }
        else{
            $this->initialHour = date('H:i:s');
            //$this->initialHour = '09:41:16';
            //die(var_dump($this->initialHour));
        }
    }
    
    public function findEdge($from, $to, $route){

        foreach($this->edges as $key=>$edge){

            if($edge['from'] == $from && $edge['to'] == $to && $edge['route_id'] == $route){
                return $key;
            }
        }



    }

    /**
     * getNeededTime method
     * Calculates the needed time to pass between one node to other
     *
     * @param int $from - Specifies the edge's starting node's id
     * @param int $to - Specifies the edge's edning node's id
     * @param int $route - Specifies the edge's route's id, if it is zero, the edge is not a part of a route
     */
    public function getNeededTime($from, $to, $route, $parentRoute){

        $timeAtThatMoment = $this->times[$from]['time'];

        $edge = $this->edges[$this->findEdge($from, $to, $route)];

        if($edge['route_id']){

            //echo $edge['route_id'] . ' - ' . $parentRoute . '<br>';

            $actualHour = strtotime($this->initialHour) - strtotime('today') + $timeAtThatMoment;
            
            $nextLeavingHour = $this->times[$from]['hour'] ?? Hour::getNextHour($edge['route_id'], $from, strtotime(gmdate('H:i',$actualHour)) - strtotime('today'));

            //echo gmdate('H:i:s',$actualHour) . '<br>';
            if($nextLeavingHour['timestamp']){

                $nextArrivingHour = Hour::getNextHour($edge['route_id'], $to, $nextLeavingHour['timestamp']);
                $totalTravelingTime = $nextArrivingHour['timestamp'] - $nextLeavingHour['timestamp'];
                $totalWaitingTime = $nextLeavingHour['timestamp'] - $actualHour;
                $totalWaitingTime = $totalWaitingTime > 10 ? $totalWaitingTime : 0;

                $route = Route::find($edge['route_id']);
                $weight = 0;
                if($edge['route_id'] != $parentRoute){
                    $weight = 200;
                }
                
                return [
                    'type' => $route->line->vehicle_id,
                    'line' => $route->line->id,
                    'waitingTime' => $totalWaitingTime,
                    'travelingTime' => $totalTravelingTime,
                    'time' => $totalWaitingTime + $totalTravelingTime,
                    'weight' => $totalWaitingTime + $totalTravelingTime + $totalWaitingTime + $weight,
                    'hour' => $nextLeavingHour,
                    'hourId' => $nextLeavingHour['id'],
                    'hourEndId' => $nextArrivingHour['id'],
                    'routeId' => $edge['route_id']
                ];
            }
            else{

                return [
                    'type' => 'walk',
                    'time' => 99999999,
                    'weight' => 99999999
                ];

            }

        }
        else{

            $humanSpeed = 2.1;
            $modification = $from == 'from' && $to == 'to' ? $this->walkingTimeModification : 0;
            return [
                'type' => 'walk',
                'time' => $edge['distance'] / $humanSpeed,
                'weight' => ($edge['distance'] / $humanSpeed) * $modification
            ];
        }

    }

    /*
     * setEdges method
     * stores the input edges array to the local edges variable
     */
    public function setEdges($edges){
        
        $this->edges = $edges;
        $this->setNodesInformation();

    }

    /*
     * setNodesInformation method
     * stores all the used nodes from the edges in the vertices variable
     */
    public function setNodesInformation(){
        foreach($this->edges as $edge){
            $this->times[$edge['from']] = [
                'time' => 9999999999,
                'weight' => 9999999999
            ];
            $this->times[$edge['to']] = [
                'time' => 9999999999,
                'weight' => 9999999999
            ];

            $this->parents[$edge['from']] = '';
            $this->parents[$edge['to']] = '';

        }

        $this->parents['to'] = '';

        $this->times['from'] = [
            'time' => 0,
            'weight' => 0,
            'routeId' => 0
        ];
        $this->times['to'] = [
            'time' => 9999999999,
            'weight' => 9999999999,
            'routeId' => 0
        ];
    }

    /*
     * execute method
     * calculcates the route by the edges
     */
    public function execute(){
        $tim = microtime(true);
        $from = 'from';
        $to = 'to';

        //echo 'start - ' . (microtime(true) - $this->microtime) . '<br>';
        while(count($this->completed) < count($this->parents)){

            $shortest = $this->getShortest();
            $shortestRoute = $shortest['route'];
            $shortest = $shortest['key'];
            
            $childs = $this->getChilds($shortest);

            if($shortest == $to){
                break;
            }

            foreach($childs as $child){

                $edgeInfo = $this->getNeededTime($shortest, $child['node'], $child['route'],$shortestRoute);

                //echo $child['route'] . ' - ' . $edgeInfo['time'] . ' - ' . $edgeInfo['timeWeight'] . '<br>';
                if($edgeInfo['weight'] + $this->times[$shortest]['weight'] < $this->times[$child['node']]['weight']){
                    $this->times[$child['node']]['weight'] = $edgeInfo['weight'] + $this->times[$shortest]['weight'];
                    $this->times[$child['node']]['time'] = $edgeInfo['time'] + $this->times[$shortest]['time'];
                    $this->times[$child['node']]['routeId'] = $edgeInfo['routeId'] ?? 0;
                    $this->parents[$child['node']] = [
                        'node' => $shortest,
                        'data' => $edgeInfo
                    ];
                }
            }  

            $this->completed[] = $shortest;
        }

        //die(microtime(true) - $tim);

        //echo 'end - ' . (microtime(true) - $this->microtime) . '<br>';

        /*
        {
            "status":"success",
            "totalTime":404.60618028816,
            "walkingTime":162.95249180856,
            "travelingTime":241.65368847959,
            "data":[
                {
                    "type":"walk",
                    "timestamp":118.34631152041
                },{
                    "type":1,
                    "routeNumber":"1",
                    "waitingTime":181.65368847959,
                    "travelingTime":60,
                    "timestamp":241.65368847959
                },{
                    "type":"walk",
                    "timestamp":44.606180288157
                },{
                    "type":"end"
                }
            ],"path":[
                {
                    "type":"from"
                },{
                    "type":"node","name":"\u0421\u043f\u0438\u0440\u043a\u0430 \u21162","lat":"42.89467","lng":"23.91163"
                },{
                    "type":"node","name":"\u0421\u043f\u0438\u0440\u043a\u0430 \u21163","lat":"42.89089","lng":"23.91774"
                },{
                    "type":"to"
                }]
            }
         */
                
    }

    public function getShortest(){

        $min = 999999999;
        $minKey = 0;
        $route = 0;

        foreach($this->times as $key=>$distance){
            if(!in_array($key, $this->completed)){
                if($distance['weight'] < $min){
                    $min = $distance['weight'];
                    $route = $distance['routeId'];
                    $minKey = $key;
                }
            }
        }

        return [
            'key' => $minKey,
            'route' => $route
        ];

    }

    public function getChilds($element){

        $output = [];

        foreach($this->edges as $edge){
            if($edge['from'] == $element){
                $output[] = [
                    'node' => $edge['to'],
                    'route' => $edge['route_id']
                ];
            }
        }

        return $output;
        
    }

    public function printJSON(){

        $from = 'from';
        $to = 'to';

        $current = $to;
        $path = [];

        $walkingTime = 0;
        $travelingTime = 0;
        $waitingTime = 0;
        
        while($current != $from){

            $path[] = [
                'node' => $this->parents[$current]['node'],
                'data' => $this->parents[$current]['data']
            ];
            $current = $this->parents[$current]['node'];

        }

        $path = array_reverse($path);
        $path[] = [
            'node' => $to
        ];


        $pathResult = [];
        $dataResult = [];
        foreach($path as $key=>$part){
            if($part['node'] == 'from' || $part['node'] == 'to'){

                $pathResult[] = [
                    'type' => $part['node']
                ];
            }
            else{
                $nodeName = '<span class="stopName">' . Node::find($part['node'])->name . '</span>';
                if(Hour::find($part['data']['hourId'])){
                    $nodeName .= '<div style="margin-top:5px">' . Vehicle::find($part['data']['type'])->name . ' №' . Line::find($part['data']['line'])->line . '</div>';
                    foreach(Hour::find($part['data']['hourId'])->getRoundedHours(1,2) as $hour){
                        if($hour['current']){
                            $nodeName .= '<span style="color:red">' . $hour['date'] . '</span><br>';
                        }
                        else{
                            $nodeName .= $hour['date'] . '<br>';
                        }
                        
                    }
                }
                else if($path[$key - 1]['data']['hourEndId']){
                    $nodeName .= '<div style="margin-top:5px">' . Vehicle::find($path[$key - 1]['data']['type'])->name . ' №' . Line::find($path[$key - 1]['data']['line'])->line . '</div>';
                    foreach(Hour::find($path[$key - 1]['data']['hourEndId'])->getRoundedHours(1,2) as $hour){
                        if($hour['current']){
                            $nodeName .= '<span style="color:red">' . $hour['date'] . '</span><br>';
                        }
                        else{
                            $nodeName .= $hour['date'] . '<br>';
                        }
                        
                    }
                }
                $pathResult[]  = [
                    'type' => 'node',
                    'name' => $nodeName,
                    'lat' => Node::find($part['node'])->lat,
                    'lng' => Node::find($part['node'])->lng
                ];

            }

            if($part['node'] == 'from'){
                $walkingTime += $part['data']['time'];
                $dataResult[] = [
                    'type' => 'walk',
                    'timestamp' => $part['data']['time']
                ];
            }
            else if($part['node'] == 'to'){
                $dataResult[] = [
                    'type' => 'end',
                ];
            }
            else if($part['data']['type'] == 'walk'){
                $walkingTime += $part['data']['time'];
                $dataResult[] = [
                    'type' => 'walk',
                    'timestamp' => $part['data']['time']
                ];
            }
            else{
                $waitingTime += $part['data']['waitingTime'];
                $travelingTime += $part['data']['travelingTime'];
                $dataResult[] = [
                    "type" => $part['data']['type'],
                    "routeNumber" => Line::find($part['data']['line'])->line,
                    "waitingTime" => $part['data']['waitingTime'],
                    "travelingTime" => $part['data']['travelingTime'],
                    "timestamp" => $part['data']['time'],
                ];

            }
        }

        echo json_encode([
            'status' => 'success',
            "initi" => $this->initialHour,
            "totalTime" => $walkingTime + $travelingTime + $waitingTime,
            "walkingTime" => $walkingTime,
            "travelingTime" => $travelingTime,
            "waitingTime" => $waitingTime,
            "data" => $dataResult,
            "path" => $pathResult
        ]);
    }
   

    public function abort($message,$parameters = []){
        echo '<meta charset="UTF-8">';
        $messages = [
            'no_hours' => 'Маршрутът не може да бъде изчисле, тъй като няма информация за някое разписание.',
            'not_found' => 'Не е намерен възможен маршрут до тази точка.'
        ];

        die($messages[$message] . ' Администрацията на сайта е осведомена. Ако искате да получите информация относно проблема, моля, въведете имейл адреса си <hr>' . implode(', ', array_map(
            function ($v, $k) { return $k . '=' . $v; }, 
            $parameters, 
            array_keys($parameters)
        )));
    }

    public static function strings(){

        return new Search();
    }


    public function in($string){

        return (
            new class(System::adaptForSearch($string)){

            public function __construct($searchIn){
                $this->searchIn = $searchIn;
            }

            public function for($string){
                
                return strpos($this->searchIn,System::adaptForSearch($string)) !== false;
            }
        });
    }

    
}
