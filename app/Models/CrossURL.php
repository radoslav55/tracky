<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/*
 * CrossURL class
 * used to handle the requests to other sites, like google
 */

class CrossURL extends Model
{

    /**
     * _key variable
     * stores a 2048 lenght key
     * used to comunicate between localhost and tracky server
     * if the key isn't equal in a request, the request will be canceled
     */
    private static $_key = 't0sxazI0IU6FQIgxkeXW4hjVSB0GTcPeaRZeW4kWrhM2IlCQypQMPAJzw3FPtsEC78s8zzi4gSYynCSTTayQxBN7wEfDPi0pKBPY8aczWO8fu1poesQcUhCllvVDKAQ2K0TtUTPOQpz1pjXAtL37S5hukQBqJqteHBrmY64zWDl3G75J4l0T2BFIMHGXZ8gxoZVPcXRgrtmybUMfaZPLAJSQRPkGUQjIMaW6GjleeDKaB8uICaxUTNRRX0fvVhrkOmxmbvgJT66ZS0l89n9ZvJLJMhSrbmEvTYBySN8KQ7Fc3yBT3dywO6unvZuDvQzVHbKsJjzQTI0dlUBHPhBEezuGeAEDWjt2w6OHPbmoF3XScenSscWOeWLmxmJQgsIQA8c126FVqoCZ4OYvYIjG3RRCVW1YSvJw0lZ2bQ9KQF3JrLri6xMCJ1zGvDYFGPg9zgNYJHePssLSR394LYAXsgy0JKINpqEpireabVNTWDqqiAgkeeuWkzblxGI9dJ3SnDnWeZRCA39wBfgsyCif7as1mEFLWudzcs9PCW7o38gSVgUnEzWvBR2dCuxsh9wHgdvH3s6VYgwpR9SzWntByKzYpWQBUUdDetEm3mq4wie0TDsMzOLPqlBfeB7gevT4cJKATcUM4bXtB2MzMYZHFeLPlAr0yu51JzpVKEXl3c9pT5HjssicNPoaaJPvXNlHs9lFbB54Q01domnqbbnv3hX8WY7zaGm0VFi8UUhoQYRFC9UPi1weNMUSb6dPzHAmP7NBS9Lgyeqje780ESa9j3UXPugSgijbR7JwI017WEPtvEBWxUMfPtF7DJNDRITIGk8hFstRHA6LKdjL8NNHu3Roo1dxtJWikWE45dMKaAEJlAsBc4MGAURqsWyG1Y3qG9cEOA67U1ffLEzGfNUygBErozqdkE7L6QAz4CS6lJRsx8ChMFwKknvV5t7LJXuaHEmn7gxuU4ICkE8qduq7Cyyg5QRQGHbTd4Pkpxzy8LHjDV5T0ZsgC3t0aKhyPMJRqGCxDzFDpCmQAT9jbD775s5MGj91z97MXzHcvWg4eMnF6rHkb5EIl7Hdy2dNylWFNIEWl5h7DYI08kkNP6NDeJHpHl5Bdf15ow7RtGDQcmwmwzu36bdcSoZD94UEIYfoMXc6B4dx21e1he6vvJAMP1071M4eSA2smYSHSPgFrLKC8We9JUxKk2UgLEX17R6AP4VQAAYhOJYliLQPFGOjGOvTGkyqsWlTIO9uG0IDWgedSViceYVS9QfY68wVyZwaVOSpFBcevMN2Q8KRVLGnciKEJqTFq3PdOpahQGAFnLXHWgpCsRpzmL79fBujhnYz2JQ3djcVB6mzaBQon5o0sHnaj7Q93wH8dbMqSkp62Ovbl44WTxx5oXJkdsfoHp8zuYBpl9k6up3ZfOqIKvjaX2jXumyx2kyKKcFnGlYZj0P8gDf5PoxHZgvxLLbfSLEKNPxLkmNksGlIwctKsrNKZoqvVmTaxLmr1rPr71IBdBAf2RMF5R9BnRKV9TC73TAdsVfhGYzIGs7nYIZnzEf5Eytm95pQWJMMviGBimme2fxL98oxAgV3TuFf4NETCnnvPlL1vp2GD3kNK8YCcicxAfW4AI3Ef4oJ4jyeptXILP1CXW6V8WLg0FAFBmnX7dncOpFKdsBErt98TiK3oPj9ffMUNrXquVuTmzONBLW7OaICS0MLtUzyCQTQ5ZjtTwbottq34W0yd2baDAhpIrTkJWVbyEBU0drwbBfMO1tSlQMOHgX5AHimgRoggPuuD2SZzkIQcxOPRUCsuzjTumbcQ8JZ3vQsvMpG3dr6fDLAptNdw5kz9DZX91fgakP3gg5qupXvomRqO64d3qUZMI63Zsxme9G8XCDzOTUB0Xq4l6heoDnEhjTswRKSbk6vPYkLtxhtwCPuQm5JhOH9tiUlifYkw62AW0uplaO150tS82C9HeA3OGUJTjZRDSnZY2kwBmz64BSNpFGkS3bxbu0qOCKX46Ik19USN0FlwJmTcorYbv0zxr1eDFeSVQYEaphb';

    /**
     * addresses function
     * @return array - the possible cross url destinations
     * and the parameters to send to each of them
     * 
     * to store a curl file into a parameter, set the following as value
     * file-->[realpath_location]
     * the brackets shouldn't be writed
     *
     * to store the app key into a parameter, set the following as value:
     * @key
     *
     */
    private static function _addresses(){

        return [
            'tracky' => [
                'type' => 'internal',
                'url' => 'http://tracky-app.com/savemysql',
                'post' => [
                    'key' => '@key',
                    'sql' => 'file-->' . realpath(Backup::last()->directory())
                ],
                'response' => 'string'
            ],
            'town' => [
                'type' => 'google',
                'url' => 'https://maps.googleapis.com/maps/api/geocode/json',
                'proxy' => 3128,
                'response' => 'json',
            ],
            'distance' => [
                'type' => 'google',
                'url' => 'https://maps.googleapis.com/maps/api/distancematrix/json',
                'response' => 'json',
                'proxy' => 3128
            ]
        ];
    } 

    /**
     * request method
     * sends a curl request with the data from the addresses array
     * 
     * @param string $operation - defines the key of the addresses array
     * @param array $getParameters - defines the get parameters for the url address
     * 
     * @return boolean
     */
    public static function request($operation, $getParameters = []){

        $curl = curl_init();

        // Gets the current operation information
        $currentOperation = self::_addresses()[$operation];


        // Sets the default options
        curl_setopt($curl, CURLOPT_URL, $currentOperation['url'] . '?' . http_build_query($getParameters));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_PROXYPORT, $currentOperation['proxy'] ?? 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        // Gets the current operation fields
        $post = $currentOperation['post'];

        // Converts the operation fields to valide post data
        $post = self::convertToValideParameters($post);

        // Sets the post information
        curl_setopt($curl, CURLOPT_POST, count($post));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);

        // Runs the request
        $content = curl_exec($curl);

        if($currentOperation['type'] == 'google'){

            Log::google();
        }

        if($currentOperation['response'] == 'json'){

            return json_decode($content,true);
        }

        return $content == 'success' ? true : false;
    }

    /**
     * key method
     * returns the app key
     * 
     * @return string
     */
    public static function key(){

        return self::$_key;
    }

    /**
     * convertToValideParameters method
     * converts each array value if it matches with some of the predefined configurations
     * the configurations are explained in the _addresses method
     *
     * @param array $post
     *
     * @return array
     */
    public static function convertToValideParameters($post){

        foreach($post as $key=>$parameter){

            $file = explode('-->',$parameter)[1] ?? false;
            
            if($file){

                $post[$key] = new \CurlFile($file, 'sql');
            }
            else if($parameter == '@key'){

                $post[$key] = self::key();
            }
        }

        return $post;
    }
}
