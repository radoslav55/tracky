<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

    protected $guarded = [];
    public $timestamps = false;

    public static $settings = [
        'site_name',
        'posts_by_page',
        'max_nodes_distance',
        'max_edges_count',
        'animation_speed',
        'ignore_walking_percent',
        'last_admin_map_location',
        'last_admin_map_zoom'
    ];

    private static $_settings = [
    	'site_name' => [
    		'http://tracky-app.com',
    		'Адрес на сайта',
            true,
    	],
    	'posts_by_page' => [
    		10,
    		'Записки на страница',
            true,
    	],
    	'max_nodes_distance' => [
    		1000,
    		'Максимално разстояние между спирки',
            true
    	],
    	'max_edges_count' => [
    		20,
    		'Максимален брой връзки между спирки',
            true
    	],
        'animation_speed' => [
            '0.00002',
            'Скорост на анимацията',
            true
        ],
        'ignore_walking_percent' => [
            '5',
            'Степен на избягване на ходенето',
            false
        ],
        'last_admin_map_location' => [
            '{"lat":"42.59217410588496","lng":"25.544529855251312"}',
            'Начална точка на картата в панела',
            false
        ],
        'last_admin_map_zoom' => [
            7,
            'Начален zoom на картата в панела',
            false
        ],
        'las_cache_update' => [
            0,
            'Последно обновяване на кеша',
            false
        ]

    ];


    public static function setDefault(){

    	foreach(self::$_settings as $key=>$setting){
    		Setting::create([
    			'name' => $key,
    			'value' => $setting[0]
    		]);
    	}
    }

    public static function get($name){
        
    	return Setting::where('name',$name)->first()->value;
    }

    public static function getForHome(){
        $output = [];

        foreach(self::$_settings as $key=>$setting){
            if($setting[2]){

                $output[$setting[1]] = Setting::get($key);
            }
        }

        return $output;
    }

    public static function getAll(){

    	$output = [];

    	foreach(self::$_settings as $key=>$setting){
    		$output[$setting[1]] = [
                'key' => $key,
                'value' => Setting::get($key)
            ];
    	}

    	return $output;
    }

    public static function set($name,$value){

    	if(in_array($name,self::$settings)){

            if(is_array($value)){
                $value = json_encode($value);
            }

            Setting::where('name',$name)->update([
                'value' => "$value"
            ]);
            return true;
        }

        return false;
    }

}
