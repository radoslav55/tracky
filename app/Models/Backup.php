<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Rah\Danpu\Dump;
use Rah\Danpu\Export;
use Rah\Danpu\Import;

use Config;

class Backup
{
    
    public static $file;

    public static $relativeDirectoryFromPublic = '/../backups/';
    public static $absoluteDirectory = '/backups';

    public static function create(){

    	try {
            $dump = new Dump;
            $dump
                ->file('../backups/' . date('Y-m-d---H-i-s') . '.sql')
                ->dsn('mysql:dbname=' . System::dbConfig('database') . ';host=localhost')
                ->user(System::dbConfig('username'))
                ->pass(System::dbConfig('password'))
                ->tmp('/tmp');

            new Export($dump);
            return true;
        } catch (\Exception $e) {
        	die($e);
            return false;
        }
    }

    public static function last(){

    	$files = scandir(self::directory());
    	$filesTimestamps = [];

    	foreach($files as $file){

    		$filesTimestamps[Backup::fileToTimestamp($file)] = $file;
    	}

    	self::$file = $filesTimestamps[max(array_keys($filesTimestamps))];

    	return new self;

    }

    public static function name(){

    	return self::$file;
    }

    public static function directory(){

    	$dir = realpath('./') . self::$relativeDirectoryFromPublic . self::$file;

    	return $dir;
    }

    public static function fileToTimestamp($file){

    	$date = str_replace('.sql','',$file);
    	
    	$divideDateByTime = explode('---',$date);

    	$date = str_replace('-','/',$divideDateByTime[0]);
    	$time = str_replace('-',':',$divideDateByTime[1]);

    	if($time){
    		return strtotime($date . ' ' . $time);
    	}

    	return 0;
    	
    }
}
