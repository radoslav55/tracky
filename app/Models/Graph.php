<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Graph
{
    
    private $_edges = [];
    private $_nodes = [];
    private $_hours = [];

    private $_node = 0;

    private $_path = [];

    public $from = 0;
    public $to = 0;

    public $beginTime = 0;

    public function __construct(){

        $rate = Setting::get('ignore_walking_percent');

        if($rate < 0){
            $rate = 1 - $rate / -1000;
        }

        $this->walkingTimeModification = $rate;
        //die(var_dump(time()));
        $this->beginTime = time(); 
        $this->totalTime = 0;
    }

    public function setNodes($nodes){

        $this->_nodes = $nodes;
    }

    public function setEdges($edges){
        $this->_edges = $edges;
    }

    public function findPath($from, $to){

        $this->from = $from;
        $this->to = $to;

        $this->_initializeNodes();
        $this->_hours = Hour::withCache();

        while(count($this->_nodes) > 0){

            
            // 2 sec
            $shortest = $this->shortestNode();
            // end

            if($this->_node['id'] == $this->to){
                //die('ok');
                break;
            }
            
            $time = microtime(true);

            // 25 sec
            foreach($shortest->childs() as $to){

                // 13 sec
                $edge = $this->calculateTime($this->_node, $to['node'], $to['edge']);
                // end
                if($edge['weight'] + $this->_node['weight'] < $this->_nodes[$to['node']['id']]['weight']){
 
                    $this->_nodes[$to['node']['id']]['weight'] = $edge['weight'] + $this->_node['weight'];
                    $this->_nodes[$to['node']['id']]['time'] = $edge['time'] + $this->_node['time'];
                    $this->_nodes[$to['node']['id']]['route_id'] = $edge['route_id'];

                    $this->_path[$to['node']['id']] = [
                        'parent' => $this->_node['id'],
                        'data' => $edge
                    ];
                }
            }
            // End

            $time = microtime(true) - $time;
            $this->totalTime += $time;

            // Remove the current shortest
            
            unset($this->_nodes[$this->_node['id']]);

        }

    }

    private function _initializeNodes(){
        
        foreach($this->_nodes as $node){
            $node->time = INFINITY;
            $node->weight = INFINITY;
            $this->_path[$node->id] = 0;
        }

        $node = $this->_nodes->where('id', $this->from)->first();
        $node->time = 0;
        $node->weight = 0;
        $node->route_id = 0;

        $this->_nodes = $this->_nodes->keyBy('id')->toArray();
        $this->_edges = $this->_edges->toArray();

    }


    public function shortestNode(){

        // 0.004
        //$this->_node = $this->_nodes->sortBy('weight')->first();
        /*$nodes = $this->_node->toArray();
        */
        
        $min = array_values($this->_nodes)[0];

        foreach($this->_nodes as $node){
            if($min['weight'] > $node['weight']){
                $min = $node;
            }
        }

        $this->_node = $min;

        return $this;
    }   

    public function childs(){

        // TODO - Smarter system when the edge is by walking
        //return array_keys($this->_edges->where('from',$this->_node->id)->keyBy('to')->toArray());
        $childs = [];
        foreach($this->_edges as $edge){

            if($edge['from'] == $this->_node['id']){
                $childs[] = [
                    'node' => $this->_nodes[$edge['to']],
                    'edge' => $edge
                ];
            }
        }
        return $childs;
    }

    public function calculateTime($from, $to, $edge){


        if($edge['route_id'] == 0){

            $humanSpeed = 1.4;

            //echo $edge['from'] . ' - ' . $edge['to'] . ' - ' .  ($edge['distance'] / $humanSpeed) * $this->walkingTimeModification . '<br>';
            return [
                'type' => 'walk',
                'time' => $edge['distance'] / $humanSpeed,
                'weight' => ($edge['distance'] / $humanSpeed) * $this->walkingTimeModification,
                'route_id' => 0
            ];
        }
        else{

            $timeAtThisMoment = $from['time'] + $this->beginTime - strtotime('today');
            $leaveHour = $this->_hours->findNext($timeAtThisMoment, $edge['route_id'], $from['id']);
            
            if($leaveHour){
                $arriveHour = $this->_hours->findNext($leaveHour['hour'], $edge['route_id'], $to['id']);
                if($arriveHour){
                    //die(var_dump($edge->route->line->id));
                    $addOnWeight = 0;

                    if($from['route_id'] != null && $from['route_id'] != $edge['route_id']){
                        $addOnWeight = 800;
                    }

                    return [

                        'type' => 'traveling',
                        'route_id' => $edge['route_id'],
                        'hereAt' => $timeAtThisMoment,
                        'waitingTime' => $leaveHour['hour'] - $timeAtThisMoment,
                        'travelingTime' => $arriveHour['hour'] - $leaveHour['hour'],
                        'time' => $arriveHour['hour'] - $timeAtThisMoment,
                        'weight' => $arriveHour['hour'] - $timeAtThisMoment + $addOnWeight,
                        'hourId' => $leaveHour['id'],
                        'hourEndId' => $arriveHour['id'],
                    ];
                }
            }

            return [
                'weight' => INFINITY
            ];
            
        }
    }

    public function printJson(){
        $from = $this->from;
        $to = $this->to;

        $current = $to;
        $path = [];

        $walkingTime = 0;
        $travelingTime = 0;
        $waitingTime = 0;

        while($current != $from){

            $path[] = [
                'node' => $this->_path[$current]['parent'],
                'data' => $this->_path[$current]['data']
            ];
            $current = $this->_path[$current]['parent'];

        }

        $path = array_reverse($path);
        $path[] = [
            'node' => $to
        ];


        $pathResult = [];
        $dataResult = [];
        foreach($path as $key=>$part){
            if($part['node'] == 'from' || $part['node'] == 'to'){

                $pathResult[] = [
                    'type' => $part['node']
                ];
            }
            else{

                $nodeName = '<span class="stopName">' . Node::find($part['node'])->name . '</span>';
                if(Hour::find($part['data']['hourId'])){

                    $hourId = $part['data']['hourId'];
                    $part['data']['hereAt'] = gmdate('H:i',$part['data']['hereAt']);
                }
                else if($path[$key - 1]['data']['hourEndId']){

                    $hourId = 0;
                    //die(var_dump(strtotime(Hour::find($path[$key - 1]['data']['hourId'])->hour)));

                    $part['data']['hereAt'] = Hour::find($path[$key - 1]['data']['hourEndId'])->hour;
                    
                }

                $nodeName .= '<div style="margin-top:2px; font-style:italic">Тук сте в ' . $part['data']['hereAt'] . '</div>';

                if($hourId){

                    $hour = Hour::find($hourId);
                    
                    $nodeName .= '<div style="margin-top:3px">' . $hour->route->line->vehicle->name . ' №' . $hour->route->line->line . '</div>';

                    foreach(Hour::find($hourId)->getRoundedHours(1,2) as $roundedHour){
                        if($roundedHour['current']){
                            $nodeName .= '<span style="color:red">' . $roundedHour['date'] . '</span><br>';
                        }
                        else{
                            $nodeName .= $roundedHour['date'] . '<br>';
                        }
                        
                    }
                }

                $pathResult[]  = [
                    'type' => 'node',
                    'name' => $nodeName,
                    'lat' => Node::find($part['node'])->lat,
                    'lng' => Node::find($part['node'])->lng
                ];

            }

            if($part['node'] == 'from'){
                $walkingTime += $part['data']['time'];
                $dataResult[] = [
                    'type' => 'walk',
                    'timestamp' => $part['data']['time']
                ];
            }
            else if($part['node'] == 'to'){
                $dataResult[] = [
                    'type' => 'end',
                ];
            }
            else if($part['data']['type'] == 'walk'){
                $walkingTime += $part['data']['time'];
                $dataResult[] = [
                    'type' => 'walk',
                    'timestamp' => $part['data']['time']
                ];
            }
            else{
                $waitingTime += $part['data']['waitingTime'];
                $travelingTime += $part['data']['travelingTime'];
                $route = Route::find($part['data']['route_id']);
               
                $dataResult[] = [
                    "type" => $route->line->vehicle->id,
                    "routeNumber" => $route->line->line,
                    "waitingTime" => $part['data']['waitingTime'],
                    "travelingTime" => $part['data']['travelingTime'],
                    "timestamp" => $part['data']['time'],
                ];

            }
        }

        return [
            'status' => 'success',
            "initi" => $this->beginTime,
            "totalTime" => $walkingTime + $travelingTime + $waitingTime,
            "walkingTime" => $walkingTime,
            "travelingTime" => $travelingTime,
            "waitingTime" => $waitingTime,
            "data" => $dataResult,
            "path" => $pathResult
        ];
    }

}
