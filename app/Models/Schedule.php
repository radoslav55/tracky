<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{

    protected $table = 'schedule';
    protected $guarded = [];
    public $timestamps = false;

    public static $types = [
    	'weekdays','holidays'
    	
    ];

   	public function hours(){
        return $this->hasMany('App\Models\Hour','schedule_id');
    }

    public static function storeTypes(){
		foreach(self::$types as $day){
	        Schedule::create([
                'days' => $day
            ]);
		}
    }

    public static function getName($key){
    	$array = [
    		'weekdays' => 'Делнични дни',
    		'holidays' => 'Празнични дни'
    	];
    	return $array[$key];
    }

}
