<?php

namespace App\Models;
use Request;
use Config;

class System
{
    
    /**
     * formatCoordinate method
     * Converts a given float number to a number with 5 characters after the decimal
     * @param float $number
     * 
     * @return float
     */
	public static function formatCoordinate($number){
		return number_format((float)$number, 5, '.', '');
	}

    /**
     * getTown method
     * Finds the town located in the given latlng
     * @param  float[] latLng - The coordinates of the point
     * 
     * @return string - The name of the town
     */
	public static function getTown($latLng){

		$response = CrossURL::request('town',[
            'latlng' => $latLng['lat'] . ',' . $latLng['lng']
        ]);

        $array = $response['results'][0]['address_components'];
        $town = 'Unknown';

        foreach($array as $el){

            if($el['types'][0] == 'locality'){
                $town = $el['long_name'];
            }
        }

        return $town;
	}

    /**
     * log method
     * Writes a log message into the errorlog.txt file
     * @param string $error - The message to write
     * 
     */
	public static function log($error){

		$text = date('Y-m-d H:i:s') . '  [' . Request::getClientIp() . '] -> ' . $error . '\n';
		file_put_contents('../errorlog.txt', $text, FILE_APPEND);
	}

    /**
     * dbConfig method
     * Gets a specific database configuration 
     * 
     * @param string $key - The key of the searched configuration 
     * @return string - The value of the searched configuration
     */
	public static function dbConfig($key){
		return Config::get('database.connections.' . Config::get('database.default') . '.' . $key);
	}

    /**
     * getAbsoluteDistanceViaRoads method
     * Calculates the distance between two points using roads by walking
     * The calculation is made by Google Maps
     * 
     * @param float[] $from - The coordinates of the starting point
     * @param float[] $to - The coordinates of the destination point
     * 
     * @return float - The distance in meters
     */
	public static function getAbsoluteDistanceViaRoads($from,$to){
        
		$response = CrossURL::request('distance',[
			'origins' => $from['lat'].",".$from['lng'],
			'destinations' => $to['lat'].",".$to['lng'],
			'mode' => 'driving',
			'language' => 'pl-PL' 
		]);

	    if($response['status'] == 'OK'){
	    	
	    	$dist = $response['rows'][0]['elements'][0]['distance']['value'];
	    }
	    else {

	    	sleep(5);
	    	return self::getAbsoluteDistanceViaRoads($from, $to);
	    }
	    
	    //sleep(0.2);
	    $dist = floatval(str_replace(',','.',$dist));
	    return $dist;
	}

    /**
     * getAbsoluteDistance method
     * Calculates the absolute distance between two points in a straight line
     * 
     * @param float[] $from - The coordinates of the starting point
     * @param float[] $to - The coordinates of the destination point
     * 
     * @return float - The distance in meters
     */
	public static function getAbsoluteDistance($from,$to){

		$theta = $from['lng'] - $to['lng'];
		$dist = sin(deg2rad($from['lat'])) * sin(deg2rad($to['lat'])) +  cos(deg2rad($from['lat'])) * cos(deg2rad($to['lat'])) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
		return intval(($miles * 1.609344) * 1000);
	}

    /**
     * defineDayType method
     * Defines the day type of today or tomorrow [weekday or holiday]
     * 
     * @param boolean $nextDay - Defines if we are searching the day type of tomorrow or no
     * @return int - The index of the day type
     */
	public static function defineDayType($tomorrow = false){
        return 1;
		$alter = 0;
		if($tomorrow){
			$alter = -1;
		}
		if(date('N') == 6 + $alter || date('N') == 7 + $alter){
			return 2;
		}
		else{
			$holiday = Holiday::where('day',date('Y-d-m'))->exists();
			if($holiday){
				return 2;
			}
			else{
				return 1;
			}
		}
	}


    /**
     * adaptForSearch method
     * removes the blank spaces and turns the letters to lowercase
     * this is useful when searching
     *
     * @param  string $string - The string to modify
     * @return  string - The converted string
     */
    public static function adaptForSearch($string){

        return mb_strtolower($string);
    }
}