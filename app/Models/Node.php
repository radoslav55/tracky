<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Node extends Model
{

    use SoftDeletes;

    protected $guarded = [];
    

    /**
     * closest method
     * Calculates the closest nodes to given latLng
     * the distance limit and the max number of nodes are defined as settings
     * @param float[] $latLng - The location of the node
     *  - lat = The lat coordinate
     *  - lng = The lng coordinate
     * @param integer $id - The id of the node, by default it is 0
     * @return Nodes[]
    */
    public function closest()
	{  

        $latLng = [
            'lat' => $this->lat,
            'lng' => $this->lng
        ];
        $id = $this->id ?? 0;

        $_distanceLimit = Setting::get('max_nodes_distance');
        $_distanceCount = Setting::get('max_edges_count');

		return DB::select("SELECT id, lat, lng, SQRT(
			    POW(69.1 * (lat - $latLng[lat]), 2) +
			    POW(69.1 * ($latLng[lng] - lng) * COS(lat / 57.3), 2)) AS distance
			FROM nodes WHERE id != ? HAVING distance < ? ORDER BY distance LIMIT 0,?" ,[$id,$_distanceLimit / 1500,$_distanceCount]);
	}

    public function connectWith($node){

        return Edge::make($this, $node);
    }

    /**
     * findOrCreate method
     * If the record id exists, it will be overriten
     * If it doesn't, a new node will be stored
     * @param integer $id - The id of the node to search for
     * @return Nodes
     */
    public static function findOrCreate($id)
	{
	    $obj = Node::find($id);
	    return $obj ?: new Node;
	}

    /*
     * edges method
     * returns the linked edges to this node
     */
    public function edges()
    {   

        return Edge::where(function($query) {
            $query->where('from',$this->id);
            $query->orWhere('to',$this->id);
        });
    }

    /*
     * routes method
     * returns the linked routes to this node
     */
    public function routes(){

        $edgesIds = [];
        foreach($this->edges()->withRoute()->get() as $edge){

            $edgesIds[] = $edge->route_id;
        }

        return Route::whereIn('id',$edgesIds);
    }

    /*
     * destroyWithChildElements method
     * destroys each linked edge and route with it's childs
     */
    public function destroyWithChildElements(){

        foreach($this->routes()->withTrashed()->get() as $route){
            Route::withTrashed()->find($route->id)->destroyWithChildElements();
        }

        $this->edges()->withoutRoute()->delete();
        $this->forceDelete();
        return true;
    }

    public function town(){

        return $this->belongsTo('App\Models\Town');
    }

}
