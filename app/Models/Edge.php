<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Edge extends Model
{
 
    protected $guarded = [];
    public $timestamps = false;
 
    public static $edge;
    public static $edges;
    public static $direction = false; // 'right' or 'reverse'

    public static $from = 0;
    public static $to = 0;

    public static function setEdges($edges){
        self::$edges = $edges;
    }


    /*
     * isNodeUsed method
     * Checks if the given node is used in a route
     */
    public static function isNodeUsed($id){

        $edges = Edge::where('route_id','!=',0)
        ->where(function($query) use($id) {
            $query->where('from',$id);
            $query->orWhere('to',$id);
        })->get();

        foreach($edges as $edge){
            if(Route::find($edge->route_id)){
                return true;
            }
        }

        return false;

    }

    /**
     * getExistingEdges method
     * extracts the edges that are not part of deleted routes
     * 
     * @return Edges
     */
    public static function getExistingEdges(){

        $array = [];

        $edges = Edge::where('route_id','!=',0)->where(function($query){
            foreach(Route::onlyTrashed()->get() as $route){
                $query->where('route_id','!=',$route->id);
            }
        })->get();

        return $edges;
    }

    /**
     * fromToExists method
     * checks if an edge with given from and to is stored
     * @param integer $node1 - The first point
     * @param integer $node2 - The second point
     * @return Edges
     */
    public static function existsViaWalking($node1,$node2){
        return Edge::where(function($query) use($node1,$node2){
            $query->where('from',$node1)
                  ->where('to',$node2)
                  ->where('route_id',NULL);
        })
        ->orWhere(function ($query) use($node1,$node2) {
            $query->where('to',$node2)
                  ->where('from',$node1)
                  ->where('route_id',NULL);
        })->exists();
        
    }

    /**
     * make method
     * Creates a new connection between two given nodes
     * 
     * @param  Node $from - The starting node
     * @param  Node $from - The ending node
     * @return class - This method returns an annonymous class to choose if the connection will be by walking or via a specific route
     */
    public static function make($from, $to){

        return new class($from, $to){

            public function __construct($from, $to){

                $this->from = $from;
                $this->to = $to;
            }

            /**
             * walking method
             * Defines that the connection is with type walking
             * @return boolean
             */
            public function walking(){
                if(!Edge::existsViaWalking($this->from->id, $this->to->id)){
                    $this->routeId = null;
                    $this->distance = System::getAbsoluteDistanceViaRoads(
                        [
                            'lat' => $this->from->lat,
                            'lng' => $this->from->lng
                        ],
                        [
                            'lat' => $this->to->lat,
                            'lng' => $this->to->lng
                        ]           
                    );
                    $this->_proceed();
                }
            }

            /**
             * route method
             * Defines the route id of the connection
             * @param int $id - The id of the route
             * @return boolean 
             */
            public function route($id){

                $this->routeId = $id;
                $this->distance = 0;
                $this->_proceed();
            }

            /**
             * _proceed method
             * Makes the final connection with the constructed data
             * 
             * @return boolean
             */
            private function _proceed(){

                Edge::create([
                    'route_id' => $this->routeId,
                    'from' => $this->from->id,
                    'to' => $this->to->id,
                    'distance' => $this->distance
                ]);
            }
        };
    }

    public static function scopeDivideNodes($query){
        $edges = $query->get(['from','to'])->toArray();
        $startingNodes = [];
        $endingNodes = [];
        foreach($edges as $edge){
            $startingNodes[] = $edge['from'];
            $endingNodes[] = $edge['to'];
        }
        return [
            'startingNodes' => $startingNodes,
            'endingNodes' => $endingNodes
        ];
    }
    
    
    public function scopeWithoutRoute($query)
    {

        return $query->where('route_id',0);
    }

    public function scopeWithRoute($query)
    {

        return $query->where('route_id','!=',0);
    }

    public function route(){

        return $this->belongsTo('App\Models\Route');
    }

}
 