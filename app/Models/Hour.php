<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Hour extends Model
{

    protected $table = 'schedule_hours';
    protected $guarded = [];

    public static function existsByHour($route,$node,$schedule,$hour){
    	return Hour::where('route_id',$route)
	    	->where('node_id',$node)
	    	->where('schedule_id',$schedule)
	    	->where('hour',$hour)
	    	->exists();
    }

    public static function getNextHour($route, $node, $relativeHour, $schedule = false){

        $tolerance = 0;

        if(!$schedule){
            $schedule = System::defineDayType();
        }

        //$startingHours = DB::select("SELECT * FROM schedule_hours WHERE route_id = ? AND schedule_id = ? AND node_id = ? ORDER BY hour",[$route, $schedule, $node]);

        $startingHours = Hour::where('schedule_id',$schedule)
        ->where('route_id',$route)
        ->where('node_id',$node)
        ->orderBy('hour')
        ->get();



        $closestHour = 9999999999;
        $closestHourId = 0;

        foreach($startingHours as $hour){
            $timestamp = strtotime($hour->hour) - strtotime('today');
            if($timestamp >= $relativeHour - $tolerance && $timestamp <= $closestHour + $tolerance){
                $closestHour = $timestamp;
                $closestHourId = $hour->id;
            }
        }
        
        return $closestHour == 9999999999 ? false : [
            'timestamp' => $closestHour,
            'id' => $closestHourId
        ];

    }

    public function getRoundedHours($before, $after){

        $output = [];

        $closestHour = 0;
        $relativeHour = strtotime($this->hour) - strtotime('today');

        $startingHours = Hour::where('schedule_id',System::defineDayType())
        ->where('route_id',$this->route_id)
        ->where('node_id',$this->node_id)
        ->orderBy('hour','desc')
        ->get();

        foreach($startingHours as $hour){
            $timestamp = strtotime($hour->hour) - strtotime('today');
            if($timestamp < $relativeHour && $timestamp > $closestHour){
                $closestHour = $timestamp;
            }
        }

        $output[] = [
            'date' => gmdate('H:i',$closestHour),
            'current' => false
        ];

        $lastHour = gmdate('H:i',strtotime($this->hour) - strtotime('today'));
        
        for($i = 0; $i <= $after; $i++){

            $output[] = [
                'date' => $lastHour,
                'current' => ($lastHour == gmdate('H:i',strtotime($this->hour) - strtotime('today')))
            ];

            $lastHour = gmdate('H:i',Hour::getNextHour($this->route_id, $this->node_id, strtotime($lastHour) - strtotime('today') + 1)['timestamp']);

        }

        return $output;
    }

    public static function withCache(){
        
        $hours = new Hour();
        $hours->useCache();
        return $hours;
    }

    public function useCache(){
        $this->usingCache = true;

        $this->cache = require(app_path() . '/Cache/Sofia' . System::defineDayType() . '.php');

    }

    public function findNext($biggerThan, $route, $node){

        if($this->usingCache){
            
            $hours = $this->cache[$route][$node];

            if(count($hours)){
                foreach($hours as $id => $hour){

                    if(strtotime($hour) - strtotime('today') >= $biggerThan){

                        return [
                            'hour' => strtotime($hour) - strtotime('today'),
                            'id' => $id
                        ];
                    }
                }
                return false;
                return $this->findNext(0, $route, $node);
            }

            return false;
        }
    }

    public static function saveCache(){

        $cache = [];

        $microtime = microtime(true);

        //$hours = Hour::all();

        $con = mysqli_connect('localhost','trackyap_admin','d3dx9admin','trackyap_production');
        $query = mysqli_query($con, "SELECT * FROM schedule_hours") or die(mysqli_error($con));
        while($hour = mysqli_fetch_assoc($query)){

            $cache['Sofia' . $hour['schedule_id']][$hour['route_id']][$hour['node_id']][$hour['id']] = $hour['hour'];
            
        }

        foreach($cache as $file=>$array){
            fopen(app_path() . '/Cache/'.  $file . '.php','w');

            $php = '<?php return ';
            ob_start();
            var_export($array);
            $php .= ob_get_clean();

            $php .= ';';

            file_put_contents(app_path() . '/Cache/' . $file . '.php', $php);
        }

        die();

    }

    public function route(){

        return $this->belongsTo('App\Models\Route');
    }
}
