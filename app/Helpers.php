<?php

use App\Models\System;

define('INFINITY',999999999);

function search($string){

    return new class(System::adaptForSearch($string)){

        public function __construct($searchFor){
            $this->searchFor = $searchFor;
        }

        public function in($string){
            
            return strpos(System::adaptForSearch($string),$this->searchFor) !== false;
        }
    };
}