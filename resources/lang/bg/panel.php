<?php 

/*
 * Messages key structure 
 * 
 * [module]_[method]_status_[?more info]
 * Examples : 
 * vehicles_update_success
 * routes_store_error
 * 
 */

return [
    /* Words for guest pages */
    
    'login_login' => 'Вие успешно влязохте в системата!',
    'login_error_notFound_login' => 'Невалидни данни за вход',
    'login_error_time_login' => 'Профилът ви ще бъде отблокиран след :time',
    
    /* End guest page */
    /* Words for panel */
    
    'brands_title' => 'Марки',
    'brands_add' => 'Добави нова марка',
    
    'brands_store' => 'Марката е успешно добавена',
    'brands_error_store' => 'Възникна проблем при добавянето на марката',
    
    'brands_update' => 'Марката е успешно редактирана',
    'brands_error_update' => 'Възникна проблем при редакцията на марката',
    
    'brands_destroy' => 'Марката е успешно изтрита',
    'brands_error_destroy' => 'Възникна проблем при изтриването на марката',

    'vehicles_create_success' => 'Транспортното средство бе успешно добавено',
    'vehicles_create_error' => 'Възникна проблем при запазването на информацията.Моля,опитайте по-късно',
    'vehicles_create_error_notFound' => 'Транспортното средство не бе намерено',

    'vehicles_update_success' => 'Транспортното средство бе успешно редактирано',
    'vehicles_update_error_database' => 'Възникна проблем при редакцията на Транспортното средство.Моля,опитайте по-късно',
    'vehicles_update_error_notFound' => 'Транспортното средство не бе намерено',
    
    'vehicles_edit_notFound' => 'Транспортното средство не бе намерено',
    'vehicles_show_notFound' => 'Транспортното средство не бе намерено',


    'routes_action_show_notFound' => 'Транспортното средство не бе намерено',
    /* End panel */
];