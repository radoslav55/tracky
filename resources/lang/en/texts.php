<?php 

return [
    /* Words from guest pages */
    
    'login' => 'Login',
    'enter' => 'Go!',
    'register' => 'Register',
    'username' => 'Username...',
    'password' => 'Password...',
    'password-confirm' => 'Password confirmation...',
    'email' => 'Email...',
    
    /* End guest page */
];