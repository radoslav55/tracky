@extends('layouts.page')

<?php $page = 3; ?>

@section('head')
<script src="js/contacts.js"></script>
@endsection

@section('content')
<script>
    $(function(){
        var height = $(window).height() - $('nav').height();
        $('.fullheihgt').height($(window).height() - $('nav').height());

        $(window).scroll(function(){

            $('.fullheihgt').css('opacity',(height - $(window).scrollTop()) / height);
        })
    })
</script>
<div id="content-main" style="position: relative;">
<!--<div class="simple-text">
    Лесен и удобен интерфейс
</div>-->
<!--<div class="phone-emulator" > 

    <div id="frame_htc_one_emulator" class="frame_scroller">                                                   
         <iframe src="http://tracky-app.com" frameborder="0" style="background-color: #fff;">                                                
        </iframe>                                        
    </div>                                    
</div>-->
<!--<div style="width:100%;height:auto;padding:0;margin:0;box-shadow: 0px 5px 50px;padding-bottom:-5px;">
    <img src="images/map.png" width="100%" >
</div>-->
<div class="row" style="z-index:30;">
    <div class="simple-text" >
        Достигнете до дестинацията си, без кола, само с градския транспорт!
    </div>
    <div class="simple-image" style="background-image: url(/images/map-route.png); background-repeat: no-repeat;">

    </div>
</div>
<div class="row" style="background-color: #EFEFEF; box-shadow: 0px 0px 50px #aaa;z-index: 3">
    <div class="simple-text">
        Защо Tracky?
    </div>

    <div class="col-3">
        <div class="icon icon-circle" style=" background: url(images/interface.png) center no-repeat;    background-size: 60% 60%;">
        </div>
        <div class="icon-title">
            Лесен и удобен интерфейс
        </div>
    </div>
    <div class="col-3">
        <div class="icon icon-circle" style=" background: url(images/mobile.png) center no-repeat;    background-size: 60% 60%;">
        </div>
        <div class="icon-title">
            Работи на всички устройства!
        </div>
    </div>
    <div class="col-3">
        <div class="icon icon-circle" style="background: url(images/map-icon.png) center no-repeat;    background-size: 60% 60%;">
        </div>
        <div class="icon-title">
            Гарантирана точност в изчислението
        </div>
    </div>
    <div style="clear:both"></div>
</div>

<div class="row" style="z-index: 5; background-color: white;">
    <div class="simple-text">
        Какво стои зад Tracky?
    </div>

    <div style="width:70%;margin:0px auto; position: relative;z-index:40">
        <div class="col-3">
            <div class="icon" style="background: url(images/html5-css3-js.png) center no-repeat; background-size: 100% 100%">
            </div>
        </div>
        <div class="col-3">
            <div class="icon" style="background: url(images/laravel-logo-big.png) center no-repeat; background-size: 100% 100%">
            </div>
        </div>
        <div class="col-3">
            <div class="icon" style="background: url(images/mysql.png) center no-repeat; background-size: 100% 70%">
            </div>
        </div>

        <div style="clear: both"></div>
    </div>
</div>


<div class="row" style="border-top:1px solid #ECECEC; background-color: #FAFAFA">
    <div class="simple-text">
        Свържете се с нас
    </div>

    <form method="post">
        <input type="text" placeholder="Имейл..." id="contactsEmail">
        <input type="text" placeholder="Тема..." id="contactsTitle">
        <textarea placeholder="Съобщение..." id="contactsMessage"></textarea>
        <input type="submit" value="Изпрати" id="sendContactsEmail">
    </form>

</div>

@endsection
