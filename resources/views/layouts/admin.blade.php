@section('buttons')

    @foreach($buttons as $button)
        <a href="{!! $button['link'] ?? ($button['action'] == 'go-back' ? URL::previous() : null) !!}" class="btn pull-right btn-{!! $button['type'] ?? 'primary' !!} {!! $button['action'] ?: null !!}" style="margin-left:20px;">
            {!! $button['text'] ?? ($button['action'] == 'go-back' ? 'Върни се назад' : null)  !!}
        </a>
    @endforeach
@endsection
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{!! csrf_token() !!}"/>
        <title>Tracky - Контролен панел</title>
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <script src="/js/jquery.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/dom.js"></script>
        <link rel="icon" href="/images/fav.png">
        <link href="/css/toggler.css" rel="stylesheet">
        <script src="/js/toggler.js"></script>
        <script src="/js/admin.js"></script>
        <script src="/js/admin/system.js"></script>
        <script src="/js/smartsearch.js"></script>
        <link rel="stylesheet" type="text/css" href="/css/toggler.css">
        <link rel="stylesheet" type="text/css" href="/css/wizard.css">
        <link rel="stylesheet" type="text/css" href="/css/admin.css">

        @yield('head')
        
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <button type="button" class="navbar-toggle collapsed" id="menu-collapse" style="padding:6px 9px">
                        <span style="color:white;">Меню</span>
                    </button>
                    <a class="navbar-brand" href="{!! url('/panel') !!}">Tracky</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right ">
                        <li>
                            <a href="{!! url('panel/user') !!}">
                                <span class="glyphicon glyphicon-user"></span>
                                {!! $user['username'] !!}
                            </a>                            
                        </li>
                        <li>
                            <p class="navbar-btn">
                                <a href="{!! url('/panel/logout') !!}" class="btn btn-primary">Излез</a>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container-fluid" >
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-2 admin">
                    <ul class="nav nav-menu">
                        <li class="{!! $page == 'home' ? 'active' : null !!}"><a href="{!! url('/panel')!!}">Начало</a></li>
                        <li class="{!! $page == 'nodes' ? 'active' : null !!}"><a href="{!! url('/panel/nodes')!!}">Спирки</a></li>
                    </ul>
                    <ul class="nav">
                        <li class="{!! $page == 'vehicles' ? 'active' : null !!}"><a href="{!! url('/panel/vehicles')!!}">Транспортни средства</a></li>

                        @foreach($vehicles as $vehicle)
                        <li class="{!! $page == 'vehicle.' . $vehicle->id ? 'active' : null !!}">
                            <a href="{!! url('/panel/lines/show/' . $vehicle->id)!!}" style="padding-left:26px;">
                                {{ $vehicle['name']}}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    <ul class="nav">
                        <li class="{!! $page == 'stats' ? 'active' : null  !!}"><a href="{!! url('/panel/stats')!!}">Статистики</a></li>
                        <li class="{!! $page == 'settings' ? 'active' : null !!}"><a href="{!! url('/panel/settings')!!}">Настройки</a></li>
                        <li class="{!! $page == 'bin' ? 'active' : null !!}"><a href="{!! url('/panel/bin')!!}">Кошче</a></li>
                    </ul>
                </div>
            
                <div class='col-xs-12 col-md-10 col-sm-9 col-md-offset-2 col-sm-offset-3 main'>
                    
                    @if(isset($title))
                    <div class="page-header">
                        <h3>{!! $title !!} @yield('buttons') </h3>
                    </div>
                    @endif
                    
                    @if(Session::has('message'))
                        <div class="alert alert-{!! Session::get('message.type') !!}">
                            @if(Session::has('message.head'))
                            <h4>{!! Session::get('message.head') !!}</h4>
                            @endif
                            <p>{!! Session::get('message.body') !!}</p>
                        </div>
                    @endif


                    @if($wizard)

                        <div class="wizard">

                            <div class="step">
                                <div class="ball {!! $wizard == 1 ? 'current' : ($wizard > 1 ? 'passed' : null) !!}"> 
                                    1 
                                </div>
                                <div class="ballText">
                                    Задаване на маршрут
                                </div>
                            </div>
                            <div class="step">
                                <div class="ball {!! $wizard == 2 ? 'current' : ($wizard > 2 ? 'passed' : null) !!}">
                                    2
                                </div>
                                <div class="ballText">
                                    Маршрут в обратна посока
                                </div>
                            </div>
                            <div class="step">
                                <div class="ball {!! $wizard == 3 ? 'current' : null !!}">
                                    3
                                </div>
                                <div class="ballText">
                                    Задаване на разписанията
                                </div>
                            </div>
                            <div class="line"></div>
                            <div style="clear:both"></div>
                        </div>
                    @endif
                    
                    @yield('content')
                    <div style="height:40px;"></div>
                </div>
            </div>
        </div>
    </body>
</html>
