<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" type="text/css" href="/css/style.css"/>
        <link rel="stylesheet" type="text/css" href="/css/glyphicons.css"/>
        @yield('styles')
        <script src="../js/jquery.js"></script>
        <script src="../js/mainJavascript.js"></script>
        @yield('scripts')
        <title>race2win</title>
    </head>
    <body>
        <header>
            <div class="upHeader">
                <div class="headerLeftPart">
                    <div class="headerLogo"></div>
                    <div class="userImage">
                        <div class="levelBar">
                            <span style="position:absolute;">{!!$user->level!!}</span>
                            <div class="levelStat">
                                <div style="height:100%;width:20%;background-color:#D7E421">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="userData">
                        <div ><b>{!!$user -> username!!}</b></div>
                        <div class="dollarIcon" style="margin-top:5px">{!!$user -> money!!}</div>
                        <div class="coinIcon">{!!$user -> credits!!}</div>
                    </div>
                    <div class="clubData">
                        <span class="groupIcon">BMW_PRO</span><br/>
                        <span style="padding-left:19px;">[12/18]</span><br/>
                        <span style="padding-left:19px;">Статус : <b style="color:#A5DC7F">В мир</b></span>
                        <span class="fuelIcon" style="position:absolute;bottom:3px;left:0px;">56/100</span><br/>
                    </div>
                </div>
                <div class="headerRightPart">
                    <ul class="headerMenu">
                        <li class="menuElement">Гараж</li>
                        <li class="menuElement">Профил</li>
                        <li class="menuElement">Съобшения&nbsp;<span style="color:#FFCC00;">[2]</span></li>
                        <li class="menuElement">Приятели</li>
                        <li class="menuElement">История</li>
                    </ul>
                </div>
                <div class='hiddenMenu'>

                </div>
            </div>
            <div class="downHeader">
                <div class='headerStats' style='font-size:0.9em;'>
                        <div>{!! date('d.m.Y') !!}</div>
                        <div style='color:#89FF00;'>Онлайн : 281</div>
                </div>
                @if($user -> rank == 1)
                <a href="{!! url('/panel') !!}" style="color:black">
                    <input type="button" style="float:right;height:100%;background-color:#00FF44;border-color:#038B27" value="Админ панел">
                </a>
                @endif
            </div>
        </header>
        <div class='bodyMenu'>
            <a href="/garage"><div class="menuLink{!! Request::segment(1) == 'garage' ? 'Clicked' : null !!}">Гараж</div></a>
            <a href="/shop"><div class="menuLink{!! Request::segment(1) == 'shop' ? 'Clicked' : null !!}">Автокъща</div></a>
            <a href="/service"><div class="menuLink{!! Request::segment(1) == 'service' ? 'Clicked' : null !!}">Сервиз</div></a>
            <a href="/races"><div class="menuLink{!! Request::segment(1) == 'races' ? 'Clicked' : null !!}">Състезания</div></a>
            <a href="/gas"><div class="menuLink{!! Request::segment(1) == 'gas' ? 'Clicked' : null !!}">Бензиностаниця</div></a>
            <a href="/clubs"><div class="menuLink{!! Request::segment(1) == 'clubs' ? 'Clicked' : null !!}">Клубове</div></a>
            <a href="/top"><div class="menuLink{!! Request::segment(1) == 'top' ? 'Clicked' : null !!}">Класация</div></a>
        </div>
        <div class="body">
            @yield('content')
        </div>
        <div class="footer">
            
        </div>
    </body>
</html>
