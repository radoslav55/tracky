<!DOCTYPE html>
<html>
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="_token" content="{!! csrf_token() !!}"/>
        <title>Tracky</title>

        <link href='https://fonts.googleapis.com/css?family=Lato:100,100italic,300,300italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,300italic,600,600italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link rel="icon" href="/images/fav.png">
        <!--<link rel="stylesheet" type="text/css" href="/css/fonts.css">-->
        
        <link rel="stylesheet" type="text/css" href="css/page.css">
        
        <script src="/js/jquery.js"></script>
       
        <script src="https://maps.googleapis.com/maps/api/js"></script>
        <script src="/js/welcome.js"></script>
        <script src="/js/Map.js"></script>
        <script src="/js/googleApi.js"></script>
        <script src="https://google-maps-utility-library-v3.googlecode.com/svn/trunk/maplabel/src/maplabel.js"></script>

        <script>
	        $(function(){

	        	var navAbsoluteHeight = $('#nav-main').height();


	        	$(' #content').css('min-height',$(window).height() - navAbsoluteHeight);
                //$(' #content').css('margin-top',navAbsoluteHeight);
                $('body').on('click','.mobile-menu',function(){
                    $('.nav').toggle(function () {
                        $("#user_button").css('display','block');
                    }, function () {
                        $("#user_button").css('display','none');
                    });

                })
                $(window).scroll(function(){

                    if($('body').scrollTop() > $('#nav-main').height()){
                        $('body').append('<nav id="scroll-nav"></nav>');
                        $('#scroll-nav').slideDown('medium');
                        $('#scroll-nav').html($('#nav-main').html());
                    }
                    else{
                        $('#scroll-nav').slideUp(300,function(){
                            $('#scroll-nav').remove();
                        });
                        
                    }
                })
	        })
        </script>

        @yield('head')

    </head>
    <body>
        <div class="container-background">
            <div class="container">
                <nav id="nav-main">
                    <div class="color-filter">
                        <div class="navbar-header">
                            <a href="http://tracky-app.com" class="navbar-brand">Tracky</a>
                        </div>
                        
                        <div class="collapse navbar-collapse">
                            <div class="mobile-menu"></div>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a class="btn btn-reversed" href="/">Начало</a></li>
                                <li class="{!! $page == 2 ? 'selected' : null !!}"><a class="btn btn-reversed" href="/download">Изтегляне</a></li>
                                <li class="{!! $page == 3 ? 'selected' : null !!}"><a class="btn btn-reversed" href="/about-us">За нас</a></li>
                                <div style="clear:both"></div>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                        <div style="clear:both"></div>
                    </div>
                </nav>
                <div id="content">
                    @yield('content')
                </div>
            </div>
            <footer>
				&copy; <?= date('Y') ?> - Всички права запазени!
			</footer>       	
        </div>
    </body>
</html>

