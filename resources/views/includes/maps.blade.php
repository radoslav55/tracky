<script src="https://maps.googleapis.com/maps/api/js"></script>
<script src="https://google-maps-utility-library-v3.googlecode.com/svn/trunk/maplabel/src/maplabel.js"></script>
<script src="/js/markerWithLabel.js"></script>
<script src="/js/admin/map.js"></script>
<script src="/js/admin/nodes.js"></script>
<script src="/js/admin/edges.js"></script>
<script src="/js/admin/panel.js"></script>
<script src="/js/dom.js"></script>