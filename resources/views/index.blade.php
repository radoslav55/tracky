<!DOCTYPE html>
<html>
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="_token" content="{!! csrf_token() !!}"/>
        <title>Tracky</title>

        <link href='https://fonts.googleapis.com/css?family=Lato:100,100italic,300,300italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,300italic,600,600italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!--<link rel="stylesheet" type="text/css" href="/css/fonts.css">-->
        <link rel="stylesheet" type="text/css" href="css/bootstrap-material-datetimepicker.css">
        <link rel="stylesheet" type="text/css" href="css/welcome.css">
        <link rel="icon" href="/images/fav.png">
        
        <script src="/js/jquery.js"></script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvwrkdWuaeXSdyBmoFk0B--od-W0t6HwQ&libraries=places"></script>
        
        <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYeynFJYl_asaLJf94F3SWJHZ4pSM7whU&libraries=places"></script>-->
        <script src="/js/welcome.js"></script>
        <script src="/js/Map.js"></script>
        <script src="/js/googleApi.js"></script>
        <script src="/js/moment.js"></script>
        
        <script src="https://google-maps-utility-library-v3.googlecode.com/svn/trunk/maplabel/src/maplabel.js"></script>
        <script src="/js/markerWithLabel.js"></script>
        <script type="text/javascript" src="js/bootstrap-material-datetimepicker.js"></script>
        <script type="text/javascript">
            @if(Request::input('debug'))
            var debug = true;
            @else
            var debug = true;
            @endif
            var map;
            var date = new Date(new Date().getTime());
            var dateString;
            var statsIcons = {
                @foreach($vehicles as $vehicle)
                {!! $vehicle->id !!} : '{!! $iconPrefix . $vehicle->icon !!}',
                @endforeach
                'walk' : '{!! $iconPrefix !!}walking.png',
                'waiting' : '{!! $iconPrefix !!}waiting.png'
            }

            var vehicles = {
                @foreach($vehicles as $vehicle)
                {!! $vehicle->id !!} : '{!! $vehicle->name !!}',
                @endforeach
            }

            var animationSettingSpeed = {!! $animationSettingSpeed !!};

        </script>
        

        <script>
            $(function(){

                $('.mobile-menu').click(function(){
                    $('.nav').css('display','block');
                })
            })

        </script>


    </head>
    <body>
        <!--<div class="container-background">
            <div class="container">
                <div class="welcome">
                    <div class="content">
                        <div class="title">Tracky</div>
                        <div class="subtittle">The new way to find your way</div>
                    </div>
                    <div class="button">
                        <div class="button-image"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar">
            <i class="fa fa-bars"></i>
            <a href="#">Map</a>
            <a href="#">Download</a>
            <a href="#">Help</a>
        </div>
        -->
        <div class="container-background">
            <div class="container">
                <nav id="nav-main">
                    <div class="collapse navbar-collapse" id="nav-main-site">
                        <div class="mobile-menu"></div>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="selected"><a href="/">Начало</a></li>
                            <li class=""><a href="/download">Изтегляне</a></li>
                            <li class=""><a href="/about-us">За нас</a></li>
                            <div style="clear:both"></div>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                    <div style="clear:both"></div>
                </nav>
                <div class="welcome">
                    <div class="content">
                        <div class="title">Tracky</div>
                        <div class="subtittle">The new way to find your way</div>
                    </div>
                    <div class="button">
                        <div class="button-image"></div>
                    </div>
                </div>
            </div>    
        </div>
        <div class="map">
            <div class="directionsController">
                <div class="form">
                    <div style="margin:0px auto">
                        <div style="float:left;">
                            <div style="position:relative;margin-bottom:5px" >
                                <div class="fromSelect"></div>
                                <input type="text" id="from" placeholder="От...">
                                <div class="getCurrentLocation">
                                </div>
                                <div style="clear:both"></div>
                            </div>
                            <div style="position:relative;">
                                <div class="toSelect"></div>
                                <input type="text" id="to" placeholder="До...">
                                <div style="clear:both"></div>
                            </div>
                        </div>
                        <div style="float:left;">
                            <div class="switchButton"></div>
                            <div id="calculate"></div>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                </div>
                <div class="hourSelector">
                </div>
            </div>
            <div class="block-controls">
            </div>
            <div class="animation-control-buttons">
                <div class="animation-control-buttons-left">
                    
                    <div class="fast-rewind">
                    </div>

                    <div class="stop-animation">
                    </div>

                    <div class="fast-forward">
                    </div>

                    <div class="skip-animation">
                    </div>
                    <div style="clear:both" class="clear"></div>
                </div>
                <div class="animation-control-buttons-right">
                    <div id="open-general-statistic">Обща статискита</div>
                    <div id="new-marchrute">Изчисли нов маршрут</div>
                </div>
            </div>
            <!--<div class="hourController">
                <!--<div class="clockIcon"></div>->
                <div>
                    <input id="leavingTime" type="text">
                    <input id="leavingDate" type="text">
                    <div id="reloadTime"></div>
                    <div style="clear: both"></div>
                </div>
                <div class="hourTitle">Тръгване в момента</div>
                <!--<div style="position: relative;z-index: -1;">
                    <input id="leavingTime" type="text">
                    <input id="leavingDate" type="text">
                    <div id="reloadTime"></div>
                    <div style="clear: both"></div>
                </div>->

            </div>-->

            <div id="googleMap">
            </div>

            @if(Request::cookie('warning') != 'read')
            <!--<div id="warning" style="position: absolute;width:80%;padding: 1%;margin-left:9%;background-color: rgba(200,80,10,0.7);bottom:0;">
                ВНИМАНИЕ! Може да има разминаване в местоположението на спирките<br>
                Местоположението им ще бъде фиксирано до <span style='font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;'>10 май 2016г.</span>
                <div style="position: absolute;top:5px;right: 5px;">
                    <a href="#" id="understood" style="color: white;text-decoration:none;">Разбрах</a>
                </dir>
            </div>-->
            @endif
        </div>

        <div style="display:none">
            <div class="info-window-scheme">
                <div class="action-image">
                </div>
                <div class="stats-box">
                    <div class="action-title">
                        
                    </div>
                    <div class="total-time">

                    </div>
                </div>
            </div>
        </div>

        <div class="general-statistic-fullbox">

            <div class="black-background">
                <div class="general-statistic">
                    <div class="message-text">
                        <h2 style="margin:0;padding:0;">Статистика</h2>
                    </div>
                    <div style="height:100%">
                        <div class="arrive-icon">
                            <img src="/images/arrive.png" height="90%" style="visibility: hidden">
                        </div>
                        <div class="status-table">
                            <div class="row">
                                <div class="col">
                                    Общо време на ходене
                                </div>
                                <div class="col" id="status-walking-time">
                                    
                                </div>
                                <div style="clear:both"></div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    Общо време на чакане
                                </div>
                                <div class="col" id="status-waiting-time">
                                    
                                </div>
                                <div style="clear:both"></div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    Общо време на пътуване
                                </div>
                                <div class="col" id="status-traveling-time">
                                    
                                </div>
                                <div style="clear:both"></div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    Общо време 
                                </div>
                                <div class="col" id="status-all-time">
                                    30 минути
                                </div>
                                <div style="clear:both"></div>
                            </div>
                            <div class="row-title">
                                Маршрут в текстова форма
                            </div>
                            <div id="statistics-marchrute">

                            </div>
                        </div>
                        <div class="statisctics-box-buttons">
                            <div id="close-statistics">Назад към картата</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(!Request::input('debug'))
        <!--<div class="black-background">
            <div class="general-statistic">
                <div style="height:100%;display: table">
                    
                    <div style="display: table-cell;vertical-align: middle;">
                        <h2 align="center" style="margin-top: 0;">Системата е в ремонт</h2>
                        <div style="background: url(images/sadicon.png) center no-repeat; width:150px;height:150px;background-size:100% 100%;margin:0px auto;"></div>
                        <h4 align="center" style="width:60%; margin:0px auto; margin-top:30px;">В момента работим над доизграждането на системата, поради което приложението е офлайн, но спокойно, съвсем скоро ще може пак да се възползвате от него!</h4>
                    </div>
                    
                </div>
            </div>
        </div>-->
        @endif
    </body>
</html>
