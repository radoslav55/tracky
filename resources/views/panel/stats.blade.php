@extends('layouts.admin')

@section('head')

<style>
    .nav-tabs li{
        text-align:center;
    }
    .tab-content div{
        padding:5px;
    }
    canvas{
        border:1px solid #CCC;
    }
</style>

<script>
    function random(min,max){
        return Math.floor(Math.random() * max) + min;
    }
    $(document).ready(function(){
        
        var initialWidth = $('#statsTab').width();

        if(initialWidth < 510){
            var width = 510;
            $('#statsTab').width(initialWidth);
            $('#statsTab').css('overflow-x','scroll');
            $('#statsTab').css('overflow-y','hidden');
        }
        else{
            var width =  $('#statsTab').width() - 22;
        }
        
        var height = $('#graphic').height() - 20;
        
        var leftPadding = 20;
        $('#graphic').attr('width',width + leftPadding);
        
        
        function Diagram(coordinates,dates,max,ctx){
            
            this.coordinates = coordinates; //The coordinates of all points
            this.dates = dates; //The x-row dates
            this.max = max;
            this.step = this.max / 10;
            this.ctx = ctx; //Context to draw
            this.ctx.clearRect(0,0,width + 20,height);
            
            this.ctx.textAlign = 'left'; 
            
            this.drawX();
            this.drawY();
            this.drawPoints();
        }
        
        Diagram.prototype.drawX = function(){
            
            this.ctx.beginPath();
            this.ctx.fillStyle = '#337ab7';
            this.ctx.rect(0,height,width + leftPadding,height); //The blue x-row bar
            this.ctx.fill();
            
            this.ctx.font = "12px sans-serif red";
            this.ctx.fillStyle = 'white';
            
            for(var i in this.dates){
                
                this.ctx.fillText(
                    this.dates[i].toString().length < 2 ? '0' + this.dates[i].toString() : this.dates[i].toString(),
                    (width / this.dates.length) * i + leftPadding,
                    height + 15
                );
        
                
            }
            
            
        }
        
        Diagram.prototype.drawY = function(){
            this.ctx.beginPath();
            this.ctx.fillStyle = '#337ab7';
            this.ctx.rect(0,0,20,height); //The blue y-row bar
            this.ctx.fill();
            
            
            
            
            for(var i = 0;i <= this.max;i += this.step){
                this.ctx.beginPath();
                this.ctx.font = "12px sans-serif red";
                this.ctx.fillStyle = 'white';
                this.ctx.textAlign = 'center';
                this.ctx.fillText(
                    i,
                    10,
                    //(height / this.max) * i - 5
                    height - (i * (height / (this.max + this.step)))
                );
                this.ctx.fill();
                
                //Draw helping lines
                this.ctx.beginPath();
                this.ctx.strokeStyle = 'black';
                this.ctx.lineWidth = 0.2;
                this.ctx.moveTo(leftPadding,height - (i * (height / (this.max + this.step))));
                this.ctx.lineTo(width + leftPadding,height - (i * (height / (this.max + this.step))));
                this.ctx.stroke();
            }
        }
        Diagram.prototype.drawPoints = function(){
            for(var i in this.dates){
                //Drawing points      
                          
                //Draw points for reloads
                
                this.ctx.beginPath();
                this.ctx.fillStyle = 'red';
                this.ctx.lineWidth = 1;
                this.ctx.rect(
                        (width / this.dates.length) * i + leftPadding,
                        height - (this.coordinates[i].reloads * (height / (this.max + this.step))),
                        2,2
                );
                this.ctx.fill();
                
                //Draw points for unique users
                
                this.ctx.beginPath();
                this.ctx.fillStyle = 'green';
                this.ctx.lineWidth = 1;
                this.ctx.rect(
                        (width / this.dates.length) * i + leftPadding,
                        height - (this.coordinates[i].users * (height / (this.max + this.step))),
                        2,2
                );                 
                this.ctx.fill();
                
                //Draw lines
                //Draw lines for reloads
                this.ctx.beginPath();
                this.ctx.strokeStyle = 'red';
                this.ctx.moveTo((width / this.dates.length) * i + leftPadding + 1,height - (this.coordinates[i].reloads * (height / (this.max + this.step))));
                if(i < this.dates.length - 1){
                    this.ctx.lineTo((width / this.dates.length) * (parseInt(i) + parseInt(1)) + leftPadding + 1,height - (this.coordinates[parseInt(i) + parseInt(1)].reloads * (height / (this.max + this.step))));
                }
                this.ctx.stroke();
                
                //Draw lines for uniqe users
                
                this.ctx.beginPath();
                this.ctx.strokeStyle = 'green';
                this.ctx.moveTo((width / this.dates.length) * i + leftPadding + 1,height - (this.coordinates[i].users * (height / (this.max + this.step))) + 1);
                if(i < this.dates.length - 1){
                    this.ctx.lineTo((width / this.dates.length) * (parseInt(i) + parseInt(1)) + leftPadding + 1,height - (this.coordinates[parseInt(i) + parseInt(1)].users * (height / (this.max + this.step))) + 1);
                }
                this.ctx.stroke();
            }
        }
        
        $('.tab-link').click(function(){
            loadData($(this).attr('for'));
        });
        
        loadData('last_24');
        
        
        function loadData(to){
            if($('.loading').css('display') == 'none'){
                $('.loading').fadeIn('fast');
            }
            $.get('/panel/stats/' + to,{},function(json){
                console.log(json);
                var data = JSON.parse(json);
                var ctx = document.getElementById('graphic').getContext('2d');
                var days = new Diagram(data.coordinates,data.dates,data.max,ctx);

                //Start typing data in table
                $('#loads_all').html(data.mInfo.reloads);
                $('#users_all').html(data.mInfo.users);
                $('#google_requests').html(data.mInfo.google);

                for(var i in data.mInfo.byPages){
                    $("#loads_" + i).html(data.mInfo.byPages[i].reloads);
                    $("#users_" + i).html(data.mInfo.byPages[i].users);
                }
                $('.loading').fadeOut('fast');
            });
        }
       
    });
    
</script>
<style>
    .loading{
        width:100%;
        text-align:center;
        height:400px;
        position:absolute;
        top:0px;
        background-color:white;
    }
    
    .table-responsive{
        border:0 !important;
    }

    
</style>
@endsection

@section('content')

<ul class="nav nav-tabs nav-justified">
    <li role="presentation" class="active col-lg-4">
        <a data-toggle="tab" class="tab-link" for="last_24">Последните 24 часа</a>
    </li>
    <li role="presentation" class="col-lg-4">
        <a data-toggle="tab" class="tab-link" for="last_30">Последните 30 дни</a>
    </li>
    <!--<li role="presentation" class="col-lg-4">
        <a data-toggle="tab" class="tab-link" for="last_all">От самото начало</a>
    </li>-->
</ul>
<div class="tab-content" style="position:relative">
    <div id="statsContent" class="tab-pane fade in active">
        <div class="col-md-8 table-responsive" id="statsTab">
            <canvas id="graphic" width="900" height="300"></canvas>
            <div class="col-md-4 col-sm-12">
                <table class="table">
                    <tr>
                        <td>
                            <div style="background-color:red;width:25px;height:2px;padding:0;margin-top:8px;">

                            </div>
                        </td>
                        <td>
                            Презареждания
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="background-color:green;width:25px;height:2px;padding:0;margin-top:8px;">

                            </div>
                        </td>
                        <td>
                            Уникални потребители
                        </td>
                    </tr>
                </table> 
            </div>
        </div>
        <div class="col-md-4 table-responsive">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td>
                            Общ брой презареждания
                        </td>
                        <td>
                            <span id="loads_all"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Общ брой уникални потребители
                        </td>
                        <td>
                            <span id="users_all"></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Заявки към google
                        </td>
                        <td>
                            <span id="google_requests"></span>
                        </td>
                    </tr>
                </tbody>
            </table>
            
            <table class="table table-striped table-responsive">
                <thead >
                <th>
                    Град
                </th>
                <th>
                    Заявки
                </th>
                <th>
                    Уникални потребители
                </th>
                </thead>
                @foreach($visitedTowns as $town)
                <tr>
                    <td>
                        {!! $towns[$town[0]]->name !!}
                    </td>
                    <td>
                        <span id="loads_{!! $town[1] !!}"></span>
                    </td>
                    <td>
                        <span id="users_{!! $town[1] !!}"></span>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="loading">
        
        <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" style="margin-top:100px;font-size:30px;"></span>
        <br>Loading
    </div>
</div>


@endsection