
@extends('layouts.admin')

@section('head')

@include('includes.maps')

<script>
    var map;
    var nodes;
    var edges;

    var redirectURL = '{!! url('/panel/routes/show/' . Request::segment(4)) !!}';
    var stops = {!! $nodes ?: '[]' !!};

    var routes = {!! $routeNodes ?: '[]' !!};

    function initialize() {

        map = new Map('mapContainer');
        map.run({!! $location !!},{!! $zoom !!},function(){
            nodes = new Nodes();
            edges = new Edges();
            nodes.drawNodes(stops);
            nodes.blockNodes(routes);
            //edges.drawEdges(routes);


            if($('#mapRoutes').val()){
                edges.drawRouteByPoints(JSON.parse($('#mapRoutes').val()));
            }
            else if(routes.length){
                edges.drawRoute(routes);
            }

            @if($mapAlert)
                map.alert('{!! $mapAlert['text'] !!}', '{!! $mapAlert['type'] !!}');
            @endif
        });


        
        $('.submitRoute').click(function(e){
            e.preventDefault();
            edges.submit();
        })

        var panel = new Panel();
        panel.append('#map');

    }

    google.maps.event.addDomListener(window, 'load', initialize);

</script>

@endsection

@section('content')

{!! Form::open([
    'method' => 'POST',
    'class' => 'form-horizontal',
    'role' => 'form',
    'files' => true
])!!}

<div class="form-group">
    {!! Form::label('route','Маршрут',[
        'class' => 'control-label col-md-2'
    ])
    !!}         
    <div class="col-md-9 mapDirectory">
        <div id="map" style="background-color:#dcdcdc;width:100%;height:300px;position:relative;overflow:hidden; {!! $errors->has('route') ? 'border:1px solid red;' : null !!}; ">
            <div id="mapContainer" style="width:100%;height:100%"></div>
            <div class="map-alert">
                <div class="map-alert-middle">

                </div>
            </div>
            
            <div style="position:absolute;top:10px;left:10px;z-index:10">
                <div>
                    <input type="text" id="searchNode" class="form-control" placeholder="Търсене на спирка...">
                    <ul class="search-node-results">
                        
                    </ul>
                    <!--<button type="button" class="btn btn-default addStop">
                        Add stops
                    </button>-->
                </div>
            </div>
        </div>

        {!! Form::input('hidden','route',null,[
            'id' => 'mapRoutes'
        ]) !!}
       {!! $errors->first('route') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        {!! Form::submit((isset($data) ? 'Редактирай' : 'Добави'),[
            'class' => 'btn btn-default submitRoute ' . (isset($data) ? 'are-you-sure' : ''),
            'question' => 'Сигурен ли сте? Всички стари връзки ще бъдат заличена завинаги?'
        ]) !!}

    </div>
</div>

{!! Form::close() !!}

@endsection
