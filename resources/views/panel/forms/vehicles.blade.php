@extends('layouts.admin')

@section('content')

{!! Form::open([
    'method' => 'POST',
    'class' => 'form-horizontal',
    'role' => 'form',
    'files' => true
])!!}

{!! Form::group('text','name',isset($data['name']) ? $data['name'] : null ,[
    'text' => 'Име',
    'placeholder' => 'Име...',
]) !!}

{!! Form::group('imageUpload','icon',(isset($data) ? $data['icon'] : null),[
    'text' => 'Иконка'
]) !!}




<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        {!! Form::submit((isset($data) ? 'Редактирай' : 'Добави'),[
            'class' => 'btn btn-primary'
        ]) !!}

    </div>

</div>

{!! Form::close() !!}

@endsection