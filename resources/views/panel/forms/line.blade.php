@extends('layouts.admin')

@section('content')

{!! Form::open([
    'method' => 'POST',
    'class' => 'form-horizontal',
    'role' => 'form',
    'files' => true
])!!}

{!! Form::group('text','line',$data['line'] ?? null,[
    'text' => 'Линия',
    'placeholder' => 'Номер на линията...',
]) !!}

{!! Form::group('select','vehicle',$vehicle ?? $data['vehicle_id'] ?? null,[
    'text' => 'Транспорт',
    'options' => $vehiclesArray
]) !!}




<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        {!! Form::submit((isset($data) ? 'Редактирай' : 'Добави'),[
            'class' => 'btn btn-default'
        ]) !!}

    </div>

</div>

{!! Form::close() !!}

@endsection