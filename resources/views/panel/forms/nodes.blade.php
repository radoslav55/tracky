@extends('layouts.admin')


@section('head')

@include('includes.maps')

<script>
    
    var map;
    var nodes;
    var deleteURL = '{!! url('/panel/nodes/delete') !!}';
    var currentNode = {!! $currentNode ?: 0 !!};

    function initialize() {

        map = new Map('mapContainer');
        
        map.run({!! $location !!},{!! $zoom !!},function(){
            nodes = new Nodes();
            nodes.makeEditable();
            loadNextNode();
        });

        
        /*nodes.clear();
        nodes.actualId = {!! Request::get('nodeId') !!};
        var ltlg;
        for(var i in nodesArray){
            if(nodesArray[i].id == nodes.actualId){
                ltlg = {
                    'lat' : parseFloat(nodesArray[i].lat),
                    'lng' : parseFloat(nodesArray[i].lng),
                }
                nodes.actualName = nodesArray[i].name;
            }
        }
        nodes.openInput(ltlg);
        */
        $('.fullscreen').click(function(){
            map.fullscreen();
        })

        $(document).on('keydown', function ( e ) {
            if(e.ctrlKey && (String.fromCharCode(e.which) == 'z' || String.fromCharCode(e.which) == 'Z')){
                path.pop();
            }
            if(e.ctrlKey && (String.fromCharCode(e.which) == 'x' || String.fromCharCode(e.which) == 'X')){
                path.clear();
            }

        });

        $('#map').height($('.main').height() - $('.main').find('.page-header').outerHeight() - 60);

       
    }

    function loadNextNode(){
        $.ajax({
            url: '/panel/nodes/next' ,
            type:"GET",
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
            success:function(data){
                data = JSON.parse(data);

                $('#nodeImg').css('background-image','url(' + data.url + ')');
                $('#nodeImg div').text(data.name);

                map.map.setCenter(new google.maps.LatLng({
                    'lat' : parseFloat(data.lat),
                    'lng' : parseFloat(data.lng)
                }));
                map.map.setZoom(18);
                nodes.markers = [];
                nodes.drawNode({
                    'lat' : parseFloat(data.lat),
                    'lng' : parseFloat(data.lng)
                },data.name,data.id);
            }
        });

    }

   
    google.maps.event.addDomListener(window, 'load', initialize);

</script>

@endsection

@section('content')

{!! Form::open([
    'method' => 'POST',
    'class' => 'form-horizontal',
    'role' => 'form',
    'files' => true
])!!}
   
<div class="col-md-12">
    <div id="map" style="background-color:#dcdcdc;width:100%;position:relative;">
        <div id="mapContainer" style="width:100%;height:100%"></div>
        <div class="map-alert">
            <div class="map-alert-middle">

            </div>
        </div>
        <div style="position:absolute;top:10px;left:10px;z-index:10">
            <div>
                <!--<input type="text" id="searchNode" class="form-control" placeholder="Търсене на спирка...">
                <ul class="search-node-results">
                    
                </ul>
                <button type="button" class="btn btn-default addStop">
                    Add stops
                </button>-->
                <div style="width:200px;height: 200px; background-size:100% 100%;" id="nodeImg">
                    <div style="position: absolute;color:black;font-size:18;font-weight: bold;background-color: rgba(255,255,255,0.8);">Central</div>
                </div>
            </div>
        </div>
        <div style="display:none">
            <div id="addInput">
                <div class="form-horizontal" id="addNodeForm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        {!! Form::label('name','Име',[
                            'class' => 'control-label col-md-3'
                        ])
                        !!}         
                        <div class="col-md-9">
                            {!! Form::text('name',null,[
                                'class' => 'form-control',
                                'placeholder' => 'Име на спирката...'
                            ]) !!}
                            <span class="error-span"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            {!! Form::button('Добави',[
                                'class' => 'btn btn-primary',
                                'id' => 'submitNode'
                            ]) !!}
                            {!! Form::button('Изтрий',[
                                'class' => 'btn btn-danger',
                                'id' => 'removeNode'
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


{!! Form::close() !!}

@endsection