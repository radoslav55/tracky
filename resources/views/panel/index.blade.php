@extends('layouts.admin')

@section('head')

    <script>
        $(function(){

            var animate;

            $('#synchDatabase').click(function(){

                var ask = confirm('Данните на сървъра ще бъдат презаписани. Сигурен ли сте?');

                if(ask){

                    animate = true;

                    $('body').waiting(`
                        <h2>Извършва се синхронизацията</h2>
                        <h4>Моля, изчакайте</h4>
                        <div class="progress" style="margin:0px auto;margin-bottom:10px;position:relative;width:60%">
                            <div class="progress-bar"  id="uploadDatabaseProgressBar" role="progressbar" aria-valuenow="70"
                                aria-valuemin="0" aria-valuemax="100" style="width:20%;position:absolute">
                            </div>
                            <div class="progress-bar" id="uploadDatabaseProgressBarSecond" role="progressbar" aria-valuenow="70"
                                aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                <span class="sr-only">70% Complete</span>
                            </div>
                        </div>
                    `);

                    $.get('/panel/mysqlUpload',function(data){

                        animate = false;
                        $('body').stopWaiting();

                        $('#synchDatabaseMessage').attr('class','alert alert-' + data.status);
                        $('#synchDatabaseMessage').html(data.body);
                    })

                    var left = 0;

                    function animation(){
                        $('#uploadDatabaseProgressBar').css('margin-left',left + '%');

                        if(left == 0.5){
                            $('#uploadDatabaseProgressBarSecond').css('width',0);
                        }
                        else if(left > 80 && left < 100){
                            $('#uploadDatabaseProgressBarSecond').css('width',(left - 80) + '%');
                        }
                        else if(left > 100){
                            left = 0;
                            
                        }

                        left += 0.5;
                        
                        if(animate){
                            requestAnimationFrame(animation);
                        }
                    }
                    animation();

                }

                
            })
        })

    </script>

@endsection


@section('content')

<!--<div class="alert alert-warning">
    <h2>Внимание! В момента панелът се дооправя</h2>
    <h5>Панелът е в състояние на постоянни редакции и може някоя функционалност да не работи ефективно.<br>
    Разработката <strong>ще се паузира в 00:00 вечерта на 30 март</strong>.</h5><br>

</div>-->
<!--<canvas id="graphic" width="600" height="300" style="border:1px solid #CCC"></canvas>-->
@if(Request::url() == 'http://localhost:8000/panel')
<div class="alert alert-warning">
    <h2>Внимание! Вие разработвате локално.</h2>
    <h5>Всички промени в базата данни ще бъдат само локални. Може да синхронизирате базата данни и допълнителните файлове веднага.</h5><br>
    <div style="position: relative;">
        <input type="button" id="synchDatabase" class="btn btn-primary" value="Синхронизирай базата данни">
        <span id="synchDatabaseMessage"></span>
        <!--<span class="glyphicon glyphicon-refresh glyphicon-animate" style="position:absolute;font-size:20px;padding:6px;"></span>-->
    </div>

</div>
@endif

<div class="row">

    <div class="col-md-6">
        <h3 align="center">Основни настройки</h3>
        <table class="table table-responsive table-striped">
            <thead>
                <th>
                    #
                </th>
                <th>
                    Стойност
                </th>
            </thead>
            @foreach($settings as $name=>$setting)
            <tr>
                <td>
                    {!! $name !!}
                </td>
                <td>
                    @if (filter_var($setting, FILTER_VALIDATE_URL))
                        <a href="{!! $setting !!}">{!! str_replace('http://','',$setting) !!}</a>
                    @else
                        {!! $setting !!}
                    @endif
                </td>
            </tr>
            @endforeach
            <tr>
                <td colspan="2" ><a href="{!! url('panel/settings')!!}" style="margin-top:10px">Виж всички настройки</a></td>
            </tr>
            <tr>
                <td colspan="2" ><a href="{!! url('panel/migrate')!!}" style="margin-top:10px">Направи миграция</a></td>
            </tr>
        </table>
    </div>

    <div class="col-md-6">
        <h3 align="center">Статистики</h3>
        <table class="table table-responsive table-striped">
            <thead>
                <th>
                    #
                </th>
                <th>
                    За днес
                </th>
                <th>
                    От началото
                </th>
            </thead>
            @foreach($logs as $log)
            <tr>
                <td>
                    {!! $log['title'] !!}
                </td>
                <td>
                    {!! $log['last_24'] !!}
                </td>
                <td>
                    {!! $log['all'] !!}
                </td>
            </tr>
            @endforeach
            <tr>
                <td colspan="3">
                    <a href="/panel/stats">Виж всички статискити </a>
                </td>
            </tr>
        </table>
    </div>

</div>

@endsection