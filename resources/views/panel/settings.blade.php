@extends('layouts.admin')

@section('head')

<script type="text/javascript">
    
    $(function(){

        $('#addHoliday').click(function(){
            $.get('/panel/settings/addholiday',{
                date : $('#holidayDate').val()
            },function(data){
                console.log(data);
                if(data.status == 'success'){
                    $('.holidaysRow .col-md-12').append(
                        '<h4 dayId="' + data.id + '">' + 
                            '<span class="label label-primary">' + 
                                data.date + 
                            '</span>' + 
                            '<div class="btn btn-danger btn-xs removeHoliday" style="margin-left:10px;" dayId="' + data.id + '">' + 
                                '<span class="glyphicon glyphicon-plus" ></span>' + 
                            '</div>' + 
                        '</h4>');
                }
                else{
                    alert(data.message);
                }
            })
        });

        $('body').on('click','.removeHoliday',function(){

            var id = $(this).attr('dayId');
            
            $.get('/panel/settings/deleteholiday',{
                'id' : id
            },function(data){
                if(data.status == 'success'){
                    $('h4[dayId=' + id + ']').remove();
                }
            })
        })
    })

</script>

@endsection


@section('content')

{!! Form::open([
    'method' => 'POST',
    'class' => 'form-horizontal',
    'role' => 'form',
])!!}

<table class="table table-responsive table-striped">
    <thead>
        <th>
            #
        </th>
        <th>
            Стойност
        </th>
    </thead>
    @foreach($settings as $name=>$setting)
    <tr>
        <td>
            {!! $name !!}
        </td>
        <td>
            <div class="form-group {!! $errors->has($setting['key']) ? 'has-error has-feedback' : null !!}">
                @if($setting['key'] == 'ignore_walking_percent')
                    {!! Form::input('range',$setting['key'],$setting['value'],[
                        'min' => '-30',
                        'max' => '30'
                    ]) !!}
                    <div class="row">
                        <div class="col-md-4" style="text-align: left;">
                            Ходенето е приоритет
                        </div>
                        <div class="col-md-4" style="text-align: center">
                            Без приоритет
                        </div>
                        <div class="col-md-4" style="text-align: right">
                            Пътуването е приоритет
                        </div>
                    </div>
                @else
                    {!! Form::input('text',$setting['key'],$setting['value'],[
                        'class' => 'form-control'
                    ]) !!}
                @endif
                {!! $errors->first($setting['key']) !!}
            </div>
        </td>
    </tr>
    @endforeach
    <tr>
        <td>
            Почивни дни
        </td>
        <td>
            <div class="form-group">
                <div class="row holidaysRow">
                    <div class="col-md-12">
                        @foreach($holidays as $holiday)
                        <h4 dayId="{!! $holiday->id !!}">
                            <span class="label label-primary">
                                {!! $holiday->day !!}
                            </span>
                            <div class="btn btn-danger btn-xs removeHoliday" style="margin-left:10px;" dayId="{!! $holiday->id !!}">
                                <span class="glyphicon glyphicon-plus" ></span>
                            </div>
                        </h4>
                        @endforeach
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <input type="date" id="holidayDate" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <input type="button" id="addHoliday" class="btn btn-primary" value="Добави">
                    </div>
                </div>
            </div>
            

        </td>
    </tr>
    <tr class="active">
        <td>
        </td>
        <td>
            <div class="form-group">
                {!! Form::submit('Запази',[
                    'class' => 'btn btn-default are-you-sure',
                    'question' => 'Сигурен ли сте?'
                ]) !!}
            </div>
        </td>
    </tr>
    <tr class="active">
        <td colspan="2" ><a href="{!! url('panel/migrate')!!}" style="margin-top:10px">Изтрий цялата база данни</a></td>
    </tr>
</table>
{!! Form::close() !!}
@endsection