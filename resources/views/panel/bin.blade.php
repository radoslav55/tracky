@extends('layouts.admin')

@section('head')

<script src="http://maps.googleapis.com/maps/api/js"></script>
<script src="/js/routes.js"></script>

@endsection

@section('content')

@if(count($data))

<div class="boxList">

@foreach($data as $item)

<div class="boxItem" style="margin-bottom:20px">
	<div class="boxItemName">{!! $item['name'] !!}</div>
	<div class="boxItemButtons">
		<a href="{!! url('/panel/' . $item['url'] . '/restore/' . $item['id']) !!}" class="btn btn-primary btn-xs" >Възстанови</a>
		<a href="{!! url('/panel/' . $item['url'] . '/destroy/' . $item['id']) !!}" class="btn btn-danger btn-xs are-you-sure" question="Сигурен ли сте? {!! $item['question'] !!} ще бъдат изтрити също">Изтрий</a>
	</div>

</div>

@endforeach

</div>

@else
<h4 align="center">Кошчето е празно</h4>

@endif

@endsection