<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Логин - Админ панел</title>
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="icon" href="/icons/panel-fav.png">
        <script src="/js/jquery.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function(){
                if(window.innerHeight > 500)
                    $('#header-main').height(window.innerHeight);
                else
                    $('#header-main').height(500);
            });
        </script>
        <style>
            html,body{
                width:100%;
                height:100%;
            }
            #header-main{
                position:absolute;
                display:table;
                height:100%;
                width:100%;
            }
            #header-main,.background{
                background: url('/images/panel-bg.jpg') center left no-repeat;
                background-attachment: fixed;
                background-size: cover;
            }
            .bg{
                position:absolute;
                width:100%;
                height:100%;
                background-color:rgba(2,196,95,0.4);
                z-index:2;
            }
            #header-main{
                z-index:100;
            }
            #intro{
                width:100px;
                display:table-cell;
                vertical-align:middle;
                text-align:center;                
            }
            
            #intro .container{
                position:relative;
                width:50%;
                max-width:750px;
                height:400px;
                padding:0;
            }
            
            #intro .container .background{
                position:absolute;
                height:100%;
                width:100%;
                -webkit-filter: blur(5px);
                -moz-filter: blur(5px);
                -o-filter: blur(5px);
                -ms-filter: blur(5px);
                filter: blur(5px);
            }
            #intro .container .form{
                color:white;
                padding:5px;
                padding-bottom:40px;
                padding-top:40px;
                position:absolute;
                height:100%;
                width:100%;
                background-color:rgba(60,60,60,0.65);
            }
            
            #intro .container .form div{
                padding:10px;
            }            
            
            #intro .container h2{
                margin-bottom:20px;
            }
            #intro .container input{
                width:40%;
                padding:4px;
                border:0;
                color:black;
                border-radius:5px;
            }
            #intro .container input:focus{
                outline:0;
            }
            #intro .container input[type="submit"]{
                color:white;
                border-radius:10px;
            }
            
            
            .alert{
                width:80%;
                margin:0px auto;
                margin-top:30px;
            }
            .alert-danger{
                background-color:rgba(150,10,10,0.5);
                color:white;
                border:0;
            }
            
            .shadow-top,.shadow-left{
                position:absolute;
                width:10px;
                height:2px;
                background-color:#00FF66;
            }
            .shadow-left{
                width:2px;
            }
            @media only screen and (max-width:1020px){
                #intro #container-background{
                    width:90%;
                }
                #intro .container{
                    width:90%;
                }
            }
            @media only screen and (max-width:680px){
                #intro .container input{
                    width:80%;
                }
            }
        </style>
    </head>
    <body>
        <div id="header-main">
            <div id="intro">
                <div class="container">
                    <!--<div class="background">
                        
                    </div>-->
                    <div class="form">
                        <h2 class="intro-heading">Добре дошли в Админ панела</h2>
                        {!! Form::open([
                            'method' => 'POST'
                        ])!!}
                        <div>
                            {!! Form::text('username',null,[
                                'placeholder' => 'Потребителско име...',
                            ])!!}
                        </div>
                        <div>
                            {!! Form::password('password',[
                                'placeholder' => 'Парола...',
                            ])!!}
                        </div>
                        <div>
                            <input type="submit" class="btn btn-primary" value="Влез">
                        </div>
                        @if(Session::has('message'))
                            <div class="alert alert-{!! Session::get('message.type') !!}">
                                @if(Session::has('message.head'))
                                <h4>{!! Session::get('message.head') !!}</h4>
                                @endif
                                <p>{!! Session::get('message.body') !!}</p>
                            </div>
                        @endif
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
