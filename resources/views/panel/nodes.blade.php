@extends('layouts.admin')

@section('head')

<script src="/js/admin/lists/nodes.js"></script>

@endsection

@section('content')

<div class="col-md-4 col-md-offset-8" style="margin-bottom:20px;">
    <input type="text" class="form-control" placeholder="Търси..." id="search-field">
</div>
    
<table class="table {!! $nodes->count() ? 'table-striped' : null !!}">
    <thead>
        <th>
            #
        </th>
        <th width="35%">
            Име
        </th>
        <th width="35%">
            <div class="incognitoSelect">
                <label class="clickableArea">
                    Град
                    @if(Request::input('town'))
                    - {!! $towns[Request::input('town')]->name !!}
                    @endif
                    <span class="glyphicon glyphicon-menu-down"></span>
                </label>
                <div class="options">
                    <a href="?"><div class="{!! Request::has('town') ? null : 'active' !!}">Всички</div></a>
                    @foreach($towns as $key=>$town)
                    <a href="?town={!! $key !!}"><div class="{!! $key == Request::input('town') ? 'active' : null !!}">{!! $town->name !!}</div></a>
                    @endforeach
                </div>
            </div>
        </th>
        <th width="1">

        </th>
        <th width="1">

        </th>
    </thead>

    <tbody class="main-list">
        @if($nodes->count() == 0)
            <tr>
                <td colspan="4">
                    <h4 align="center">Няма добавени спирки</h4>
                </td>
            </tr>
        @endif

        @foreach($nodes as $node)
        <tr>
            <td>
                {!! $node->id !!}
            </td>
            <td >
                {!! $node->name !!}
            </td>
            <td >
                {!! $node->town->name !!}
            </td>
            <td>
                <a href="{!! url('panel/nodes/manage?nodeId=' . $node->id) !!}" title="Редактиране" class="btn btn-success btn-xs">
                    <span class="glyphicon glyphicon-pencil"></span> Редактирай
                </a>
            </td>
            <td>
                <a href="{!! url('panel/nodes/delete/' . $node->id ) !!}" class="btn btn-danger btn-xs">
                    <span class="glyphicon glyphicon-remove"></span> Изтрий
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

{!! $nodes->render() !!}

@endsection