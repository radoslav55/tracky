@extends('layouts.admin')

@section('content')

@if($vehicles->count() == 0)
    <h4 align="center">Няма добавени транспортни средства</h4>
@else
    <table class="table table-responsive table-striped">
        <thead>
            <th>
                #
            </th>
            <th>
                Име
            </th>
            <th colspan="2">

            </th>
        </thead>
        @foreach($vehicles as $vehicle)
        <tr>
            <td>
                {!! $vehicle->id !!}
            </td>
            <td >
                <img height="20" src="{!! $iconPrefix . $vehicle->icon !!}">
                {!! $vehicle->name !!}
            </td>
            <td width="1">
                <a href="{!! url('panel/vehicles/edit/' . $vehicle->id) !!}" title="Редактиране" class="btn btn-success btn-xs">
                    <span class="glyphicon glyphicon-pencil"></span> Редактирай
                </a>
            </td>
            <td width="1">
                <a href="{!! url('panel/vehicles/delete/' . $vehicle->id ) !!}" class="btn btn-danger btn-xs">
                    <span class="glyphicon glyphicon-remove"></span> Изтрий
                </a>
            </td>
        </tr>
        @endforeach
    </table>

@endif

@endsection