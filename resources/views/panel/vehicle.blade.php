@extends('layouts.admin')

@section('content')
<div class="table-responsive">
    <table class="table table-responsive {!! $lines->count() ? 'table-striped' : null !!}">
        <thead>
            <th>
                #
            </th>
            <th>
                Линия
            </th>
            <th>
                Маршрути
            </th>
            <th colspan="3">

            </th>
        </thead>
        @if($lines->count() == 0)
            <tr>
                <td colspan="5">
                    <h4 align="center">Няма добавени линии</h4>
                </td>
            </tr>
        @endif

        @foreach($lines as $line)
        <tr>
            <td>
                {!! $line->id !!}
            </td>
            <td >
                <strong>{!! $line->line !!}</strong>
            </td>

            <td>
                <div style="padding-bottom:10px;font-weight: bold;">
                    Списък на маршрутите [{!! $line->routes()->count() !!}]
                </div>
                <table class="table" style="background-color: transparent;">

                    @foreach($line->routes as $route)
                        <tr>
                            <td>
                                <div style="padding:3px 0px">{!! $route->getFirstNode()->name !!} - {!! $route->getLastNode()->name !!}</div>
                            </td>
                            <td width="1">
                                <a href="{!! url('panel/schedule/' . $route->id) !!}" title="Редактиране" class="btn btn-primary btn-xs">
                                    <span class="glyphicon glyphicon-th"></span> <span class="glyphicon-label">Разписание</span>
                                </a>
                            </td>
                            <td width="1">
                                <a href="{!! url('panel/routes/edit/' . $route->id) !!}" title="Редактиране" class="btn btn-success btn-xs">
                                    <span class="glyphicon glyphicon-pencil"></span> <span class="glyphicon-label">Редактирай</span>
                                </a>
                            </td>
                            <td width="1">
                                <a href="{!! url('panel/routes/delete/' . $route->id) !!}" class="btn btn-danger btn-xs">
                                    <span class="glyphicon glyphicon-remove"></span> <span class="glyphicon-label">Изтрий</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="5">
                        <a href="{!! url('/panel/routes/create/' . $line->id) !!}" class="btn btn-primary btn-xs" style="margin-top:5px;">
                            @if($line->wizardStatus())
                            Продължи с помощника
                            @else
                            Добави нов маршрут
                            @endif
                        </a>
                        </td>
                    </tr>
                </table>
            </td>
            
            <td width="1">
                <a href="{!! url('panel/lines/edit/' . $line->id) !!}" title="Редактиране" class="btn btn-success btn-xs">
                    <span class="glyphicon glyphicon-pencil"></span> <span class="glyphicon-label">Редактирай</span>
                </a>
            </td>
            <td width="1">
                <a href="{!! url('panel/lines/delete/' . $line->id) !!}" class="btn btn-danger btn-xs">
                    <span class="glyphicon glyphicon-remove"></span> <span class="glyphicon-label">Изтрий</span>
                </a>
            </td>
        </tr>
        @endforeach
    </table>
</div>

{!! $lines->render() !!}

@endsection