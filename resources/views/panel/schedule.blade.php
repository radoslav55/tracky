@extends('layouts.admin')

@section('head')
<link rel="stylesheet" type="text/css" href="/css/admin-schedule.css">
<script>
	var nodes = {!! json_encode($nodes) !!};
	var routeId = {!! $routeId !!};

	$(function(){
		var nodesLength = Object.keys(nodes).length - 1;
		
		var nodesCanvasWidth = $('#nodes-canvas').width();
		var itemWidth = ((20 / nodesCanvasWidth) * 100) / nodesLength;

		var percentage = 100 / nodesLength - itemWidth;

		var percents = 0;

		var node;
		var nodeId = 0;

		var scheduleId;

		for(var i in nodes){
			$('#nodes-canvas').append('<div class="nodes-point" nodeId="' + i + '" style="left:' + percents + '%;"></div>');
			percents += percentage;
		}

		scheduleId = $('.node-modes-switch li a').first().attr('scheduleId');
		selectNode($('.nodes-point').first().attr('nodeId'));


		$('.nodes-point').on('click',function(){

			selectNode($(this).attr('nodeId'));
		})

		$('.previous-node').click(function(){

			if(nodes[nodeId - 1]){
				nodeId--;
				selectNode(nodeId);
			}
		})

		$('.next-node').click(function(){
			if(nodes[parseInt(nodeId) + 1]){
				nodeId++;
				selectNode(nodeId);
			}
		})

		$('.node-modes-switch li').first().attr('class','active');
		scheduleId = $('.node-modes-switch li a').first().attr('scheduleId');

		$('.node-modes-switch li a').click(function(){
			scheduleId = $(this).attr('scheduleId');
			getHours();
		})


		$('.add-times').click(function(){
			$('#main').waiting('Добавяне на часовете...');
			$.get('/panel/schedule/add',{
				'nodeId' : nodes[nodeId].id,
				'routeId' : routeId,
				'scheduleId' : scheduleId,
				'hours' : $(this).parent('div').find('textarea').val(),
			},function(data){
				if(data == 'ok'){
					getHours();
				}
				$('#main').stopWaiting();
			})
			$(this).parent('div').find('textarea').val('');
		})

		function selectNode(id){
			nodeId = id;
			node = nodes[nodeId];
			$('#nodes-redaction #node-title').html(node.name);
			$('.nodes-point.selected').removeClass('selected');
			$('.nodes-point[nodeId="' + id + '"]').addClass('selected');
			$('#nodes-redaction .arrow').css('left',parseFloat($('.nodes-point[nodeId="' + id + '"]').css('left')) + parseInt(10));
			
			getHours();
		}

		function getHours(){
			
			$('#main').html('');

			$.ajax({
				'url' : '/panel/schedule/gethours',
				'method' : 'get',
				'data' : {
					'nodeId' : nodes[nodeId].id,
					'routeId' : routeId,
					'scheduleId' : scheduleId,
				},
				'async' : false
			}).done(function(json){
				
				var data = JSON.parse(json);
				var iString = ''
				for(var i in data){
					iString = i.length == 1 ? '0' + i : i;
					$('#main').append('<div class="hours-column" id="hours-column-' + i + '"></div>');
					$('#main #hours-column-' + i).append('<h4 class="hours-column-header"><span class="label label-primary">' + iString + '</span></h4>');
					for(var j in data[i]){
						$('#main #hours-column-' + i).append('<h4 class="hours-column-el"><span class="label label-default" id="hours-column-el-' + j + '" hourId="' + j + '">' + data[i][j] + '</span></h4>');
					}
				}

				if(data.length == 0){
					$('#main').append('<h4 class="no-items-text">Няма добавени часове</h4>');
				}

				$('.hours-column').css('height',$('#main').height());
				$('#main').append('<div class="selector" style="position:absolute;background-color:#286090;border:1px solid #286090;opacity:0.4;left:0px;top:0px;z-index:20"></div>');
			})
			/*$.get('/panel/schedule/gethours',{
				'nodeId' : nodes[nodeId].id,
				'routeId' : routeId,
				'scheduleId' : scheduleId,
			},function(json){

				var data = JSON.parse(json);
				var iString = ''
				for(var i in data){
					iString = i.length == 1 ? '0' + i : i;
					$('#main').append('<div class="hours-column" id="hours-column-' + i + '"></div>');
					$('#main #hours-column-' + i).append('<h4 class="hours-column-header"><span class="label label-primary">' + iString + '</span></h4>');
					for(var j in data[i]){
						$('#main #hours-column-' + i).append('<h4 class="hours-column-el"><span class="label label-default" id="hours-column-el-' + j + '" hourId="' + j + '">' + data[i][j] + '</span></h4>');
					}
				}

				if(data.length == 0){
					$('#main').append('<h4 class="no-items-text">Няма добавени часове</h4>');
				}

				$('.hours-column').css('height',$('#main').height());
				$('#main').append('<div class="selector" style="position:absolute;background-color:#286090;border:1px solid #286090;opacity:0.4;left:0px;top:0px;z-index:20"></div>');
			
			})*/
		}

		$('#massHoursAction').change(function(){
			var action = $(this).val();
			var selectedItems = [];

			$('#main .hours-column-el span.label-info').each(function(){
				selectedItems.push($(this).attr('hourId'));
			});

			if(action == 'delete'){
				$('#main').waiting('Изтриване на избраните часове...');
				$.get('/panel/schedule/delete',{
					'items' : selectedItems
				},function(data){
					if(data == 'ok'){
						getHours();
					}
					$('#main').stopWaiting();
				})
			}
			$(this).val(0);
		})


		$('#main').mousedown(function(e){
			if($(event.target).parent('h4').attr('class') == 'hours-column-el'){
				$(event.target).parent('h4').find('span').toggleClass('label-default label-info');
			}
			if(e.which == 1){

				$('.hours-column-el').css( 'cursor', 'default' );


				var x = parseInt(e.clientX) - parseInt($('#main').offset().left) + parseInt($('#main').scrollLeft());
				var y = parseInt(e.clientY) - parseInt($('#main').offset().top) + parseInt($('#main').scrollTop());

				var width = 0;
				var height = 0;

				var mainWidth = $('#main').width();

				var top = 0;
				var left = 0;

				var scrollTo = 0;
				var maxScroll = $('#main')[0].scrollWidth;
				
				$('.selector').css('left',x + 'px');
				$('.selector').css('top',y + 'px');

				$('#main').disableSelection();
				
				$('body').mousemove(function(e){
					var relativeXPosition = parseInt(e.clientX) - parseInt($('#main').offset().left) + parseInt($('#main').scrollLeft()) - parseInt(x);
					var relativeYPosition = parseInt(e.clientY) - parseInt($('#main').offset().top) + parseInt($('#main').scrollTop()) - parseInt(y);


					if(relativeXPosition >= 0){
						width = relativeXPosition;
						scrollTo = parseInt(width + x) - parseInt(mainWidth);
						$('.selector').css('left',x + 'px');
					}
					else{
						left = parseInt(x) + parseInt(relativeXPosition);
						width = parseInt(relativeXPosition * -1);
						$('.selector').css('left',left + 'px');
						scrollTo = parseInt(left) - parseInt(width + x) - $('#main').width();
					}

					if(relativeYPosition > 0){
						height = relativeYPosition;
						$('.selector').css('top',y + 'px');
					}
					else{
						top = parseInt(y) + parseInt(relativeYPosition);
						height = relativeYPosition * -1;
						$('.selector').css('top',top + 'px');
					}

					//var width = parseInt(e.clientX) - 
					//var height = parseInt(e.clientY) - parseInt(y) - parseInt($('.main').offset().top) 

					$('.selector').css('width',width > maxScroll - x ? maxScroll - x : width);
					$('.selector').css('height',height);

					if(scrollTo > maxScroll){
						scrollTo = maxScroll;
					}

					if(e.clientX > parseInt($('#main').offset().left) + parseInt($('#main').width())){
						$('#main').scrollLeft(scrollTo);
					}
					else if(e.clientX < $('#main').offset().left){
						$('#main').scrollLeft(scrollTo + $('#main').width());
					}

					$('.hours-column-el span').each(function(){

						var xPos = parseInt($(this).offset().left) - parseInt($('#main').offset().left) + parseInt($('#main').scrollLeft());
						var yPos = parseInt($(this).offset().top) - parseInt($('#main').offset().top);
						var xPos2 = parseInt(xPos) + $(this).width();
						var yPos2 = parseInt(yPos) + $(this).height();

						var absoluteX = parseInt($('.selector').offset().left) - parseInt($('#main').offset().left);
						var absoluteY = parseInt($('.selector').offset().top) - parseInt($('#main').offset().top);
						var absoluteX2 = parseInt(absoluteX) + $('.selector').width() + parseInt($('#main').scrollLeft());
						var absoluteY2 = parseInt(absoluteY) + $('.selector').height();

						if(
							(xPos > absoluteX && xPos < absoluteX2 && yPos > absoluteY && yPos < absoluteY2) || 
							(xPos2 > absoluteX && xPos < absoluteX2 && yPos > absoluteY && yPos < absoluteY2) || 
							(xPos > absoluteX && xPos < absoluteX2 && yPos2 > absoluteY && yPos < absoluteY2) || 
							(xPos2 > absoluteX && xPos < absoluteX2 && yPos2 > absoluteY && yPos < absoluteY2)

						){
							$(this).removeClass('label-default').addClass('label-info');
						}
						else{
							$(this).removeClass('label-info').addClass('label-default');
						}
						
						
						
					})
					
				});

			}
				
		});

		$('body').mouseup(function(){

			$('body').off('mousemove');

			$('.hours-column-el').css( 'cursor', 'pointer' );

			$('.selector').css({
				'width' : 0,
				'height' : 0,
				'top' : 0,
				'left' : 0
			});

		//	$('body').enableSelection();
		})

		$.fn.disableSelection = function() {
	        return this
	        .attr('unselectable', 'on')
	        .css('user-select', 'none')
	        .css('-moz-user-select', 'none')
	        .css('-khtml-user-select', 'none')
	        .css('-webkit-user-select', 'none')
	        .on('selectstart', false)
	        .on('contextmenu', false)
	        .on('keydown', false)
	        .on('mousedown', false);
	    };
	 
	    $.fn.enableSelection = function() {
	        return this
	        .attr('unselectable', '')
	        .css('user-select', '')
	        .css('-moz-user-select', '')
	        .css('-khtml-user-select', '')
	        .css('-webkit-user-select', '')
	        .off('selectstart', false)
	        .off('contextmenu', false)
	        .off('keydown', false)
	        .off('mousedown', false);
	    };
	});
</script>

@endsection

@section('content')

<div id="nodes-canvas">
	<div class="nodes-line"></div>
</div>
<div id="nodes-redaction">
	<div class="arrow">
		<div class="arrow-background"></div>
	</div>
	<div class="node-header-bar">
		<button type="button" class="btn btn-primary previous-node">
			Предишна
		</button>
		<h3 id="node-title"></h3>
		<button type="button" class="btn btn-primary next-node">
			Следваща
		</button>
	</div>
	<div class="node-modes-switch" style="margin-bottom:10px;" role="group" aria-label="Justified button group">
		<ul class="nav nav-tabs nav-justified">
		@foreach($categories as $category)
			<li role="presentation">
				<a scheduleId="{!! $category->id !!}" data-toggle="tab" href="#main">
					{!! Schedule::getName($category->days) !!}
				</a>
			</li>
		@endforeach
		</ul>
	</div>
	<div class="node-body-div row">
		<div class="col-md-9 tab-pane active" >
			<div id="main">
			</div>
			<div class="main-buttons">
				
			</div>
			<select class="form-control" id="massHoursAction">
				<option selected disabled value="0">Масови действия</option>
				<option value="delete">Изтрий завинаги</option>
		    </select>
		</div>
		<div class="col-md-3">
			<textarea style="width:100%;height:200px;margin-top:10px;" class="add-weekdays-hours"></textarea>
			<button class="btn btn-primary add-times">
				Добави 
			</button>
			<button class="btn btn-default" data-toggle="modal" data-target="#modal-add-help">
				<span class="glyphicon glyphicon-question-sign"></span>
			</button>
		</div>
	</div>
	<div class="modal fade" id="modal-add-help" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Как се добавят нови часове?</h4>
				</div>
				<div class="modal-body">
					<b>1. Добавяне на единичен час</b>
					<p>
						За добавяне на нови часове се изписват часовете във формат <b>hh:mm</b>, като всеки нов час се изписва на <b>нов ред</b>
						<br>
						Пример:
						<br>
						<div style="width:100%;margin-top:10px;border:1px solid #666;padding:5px;">
							8:45<br>
							10:17<br>
							12:30<br>
						</div>
					</p>
					<b>2. През интервали</b>
					<p>
						За добавяне на нови часове през интервали, например от 10:00 до 18:00 през 10 минути, се изписва по следния начин<br>
						<div style="width:100%;margin-top:10px;margin-bottom:10px;border:1px solid #666;padding:5px;">
							10:00 - 18:00 | 0:10<br>
						</div>
						Всеки следващ интервал се добавя на нов ред<br>
					</p>
					<b>Всеки метод може да се комбинира със всеки</b><br>
					<b>Празните места не са от значение</b><br>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
 
</div> 

@endsection