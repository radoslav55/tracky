@extends('layouts.admin')

@section('content')

<h2 align="center" style="margin-top:10%;">
    Линията е успешно създадена. Сега идва ред на маршрутите
</h2>
<h4 align="center" style="width:60%;margin:0px auto;margin-top:20px;font-weight: italic;">
    <p>Този помощник е създаден за улесняване на задаването на "Главния" и "Обратния" маршрути.</p>
    <p>Предполага се, че всеки маршрут трябва да ги има.</p>
    <p>Разбира се, ако не се нуждаете от помощника, може още тук да го деактивирате</p>
</h4>
<div class="col-md-12 text-center"> 
    <a href="{!! url('/panel/routes/create/' . $line) !!}" class="btn btn-primary" style="margin-top:20px">
        Стартирай помощника!
    </a>
    <a href="{!! url('/panel/routes/cancelwizard/' . $line) !!}" class="btn btn-default" style="margin-top:20px">
        Излез от помощника
    </a>
</div>

@endsection