@extends('layouts.admin')

@section('content')

<h3 align="center" style="margin-top:5%;">
    Готово! Маршрутите са зададени. Остават разписанията.<br>
    Може да редактирате маршрутите по всяко време.
</h3>
<div class="row" style="margin-top:50px;text-align: center">
    <div class="col-md-4">
        <a href="{!! url('/panel/schedule/' . $routes[0]) !!}" class="btn btn-primary pull-right">
            Разписание за първи маршрут
        </a>
    </div>
    <div class="col-md-4">
        <a href="{!! url('/panel/lines/show/' . $vehicle) !!}" class="btn btn-default">
            Ще добавя разписанието после
        </a>
    </div>
    <div class="col-md-4">
        <a href="{!! url('/panel/schedule/' . $routes[1]) !!}" class="btn btn-primary pull-left">
            Разписание за последен маршрут
        </a>
    </div>
</div>

@endsection