<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs',function($table){
            $table->increments('id');
            $table->string('user_id',60)->nullable();
            $table->string('from',255)->nullable();
            $table->string('type',255)->nullable();
            $table->string('location',511)->nullable();
            $table->integer('timestamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('logs');
    }
}
