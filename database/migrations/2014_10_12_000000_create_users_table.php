<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->nullable();
            $table->string('password', 60)->nullable();
            $table->string('type',60)->default('guest')->nullable();
            $table->string('ip',255)->nullable();
            $table->string('cookie',255)->nullable();
            $table->integer('attempts');
            $table->integer('blockedTo');
            $table->dateTime('next_reset');
            $table->rememberToken();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
