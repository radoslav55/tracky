<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('routes',function($table){
            $table->foreign('line_id')->references('id')->on('lines');
        });

        Schema::table('edges',function($table){
            $table->foreign('route_id')->references('id')->on('routes');
            $table->foreign('from')->references('id')->on('nodes');
            $table->foreign('to')->references('id')->on('nodes');
        });

        Schema::table('nodes',function($table){
            $table->foreign('town_id')->references('id')->on('towns');
        });

        Schema::table('schedule_hours',function($table){
            $table->foreign('schedule_id')->references('id')->on('schedule');
            $table->foreign('route_id')->references('id')->on('routes');
            $table->foreign('node_id')->references('id')->on('nodes');
        });

        Schema::table('logs',function($table){
            //$table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('lines',function($table){
            $table->foreign('vehicle_id')->references('id')->on('vehicles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('routes',function($table){
            $table->dropForeign(['line_id']);
        });
    }
}
