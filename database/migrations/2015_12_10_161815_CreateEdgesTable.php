<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEdgesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edges',function(Blueprint $table){
            $table->increments('id');
            $table->integer('route_id')->default(0)->unsigned()->nullable();
            $table->integer('from')->unsigned();
            //$table->foreign('from')->references('id')->on('nodes');
            $table->integer('to')->unsigned();
            $table->integer('distance')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('edges');
    }
}
