<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShceduleHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_hours',function(Blueprint $table){
            $table->increments('id');
            $table->integer('schedule_id')->unsigned();
            $table->integer('route_id')->unsigned();
            $table->integer('node_id')->unsigned();
            $table->time('hour');
            $table->string('status')->length(4);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('schedule_hours');
    }
}
