<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nodes',function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            //$table->double('lat', 8, 5);
            //$table->double('lng', 8, 5);
            $table->string('lat')->length(8);
            $table->string('lng')->length(8);
            $table->integer('town_id')->length(8)->default(0)->unsigned();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nodes');
    }
}
